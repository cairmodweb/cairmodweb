# CairModWeb

## Context

Le projet CAIRMOD, vise à documenter les albums iconographiques qui représentent l'architecture moderne du Caire. Ce projet est conjointement dirigé par deux laboratoires du CNRS, le laboratoire InVisu et l'UMS Persée. 

Ces albums sont stockés dans Gallica qui est une bibliothèque numérique de la BNF. Une documentations précise est associé au jeu de données, elle est accéssible sur une plateforme en ligne (perséide Athar). 

Entre ces deux serveurs se trouve un gazetier. Il est géré à partir de l'outil OpenThéso qui est un gestionnaire de thésaurus multilingue. Chaque bâtiment possède une description détaillée. Elle est composée des coodonnées géographiques et des informations liées à lévolution des bâtiments.

Le projet CAIRMOD cherche à rassembler les images du Caire moderne (XIX-XXe siècle) et à les positionner. Il se situe dans la continuité du projet Athar entrepris entre 2014 et 2016. 

L'ensemble de ces données doivent alimenter un WebSIG. Ce webSIG doit s'adresser au grand public. Il lui permettra de consulter l'ensemble des données temporelles liées à l'évolution de la ville du Caire. 

Ce projet est réalisé par un groupe de deux élèves ingénieur en fillière TSI et de deux élèves en Master 2 TSI. Il est réalisé entre le 4 mars 2020 et le 24 avril 2020.

## Objectifs

Ce projet consiste à mettre en place un webSIG ergonomique qui intègre le module de requête et d’affichage des documents iconographiques du corpus.

Un soin particulier devra donc être apporté à l’ergonomie et à l’aspect visuel mais également à la pérennité et à l’interopérabilité du site dans une dimension de web sémantique.

Fonctionnalités principales de l'application : 

- Ergonomie de l'application (application intuitive dans le but d'être pris en main facilement et rapidement).
- Intérogation de la machine virtuelle.
- Affichage des données vecteurs et rasters. 
- Gestions des différentes couches importées dans le webSIG.
- Export des cartes aux formats PDF ou PNG.
- Export des données au format CSV.
- Comparaison de deux plans associés à deux dates différentes (possibilité d'utiliser un bare de scrole).
- Elaboration d'une interface de recherche élaboré (sur les différents champs, les différents dates, par zone géographique).



## Mapping du site

Lorsque l'application est déployée sur un serveur selon la procédure décrite dans le dossier [app](./app/README.md), les points d'entrée des différents composant diffèrent d'un déploiement local.

Les chemins suivants seront donc pour un déploiement local (le premier donnée) et un déploiement sur un serveur (le second), ici notre serveur scolaire [http://217.160.173.154](http://217.160.173.154).

### Mapping de la web-application : [http://localhost:4200](http://localhost:4200) ou [http://217.160.173.154/cairmodweb](http://217.160.173.154/cairmodweb)
* `/accueil` : Page d'accueil du site
* `/information` : Page de présentation projet et contact
* `/visualisation` : Page avec la carte, l'utilisation des query, gestion des couches
* `/doc` : documentation rédigée et mise en page de la web-application et l'ensemble de l'application

### Mapping de l'API de requêtes : [http://localhost:3000](http://localhost:3000) ou [http://217.160.173.154/api](http://217.160.173.154/api)
* `/api` : accès à l'api REST
* `/apispecs` : documentation OpenAPI de l'API de requêtes
* `/doc` : documentation rédigée et mise en page de l'API de requêtes



## L'équipe 

- Fanny Vignolles, Scrum Master,  @fvgls
- Julie Grosmaire, Développeuse, @jgrsmaire
- Antoine Laurendon, Développeur, @AntoineLaurendon
- Alexis Coatsaliou, Développeur, @Alexisctsl


## Liens utiles

* [Drive Google](https://drive.google.com/drive/folders/191DT_--llNyQLUeWSEo0HkVqjrnVRyps?usp=sharing) (lecture)
* Equipe Trello : https://trello.com/cairmodweb (lecture)
