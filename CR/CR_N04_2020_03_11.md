# Compte-Rendu de réunion CairmodWeb

**Date :** 06/03/2020
**Sprint :** #1

**Objet(s) :**
1. Redéfinition de l'utilisateur final
1. La visée du site 
1. Validation de la forme de l'interface
1. Clarification concernant la VM
1. Questions sur les fonctions de l'interface


**Visio :** Oui


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | R / OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | OK  |
| Julie Erismann | InVisu |  |
| Hélène Bégnis  | Persée |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Redéfinition de l'utilisateur final

#### Questions 

1.	Redéfinition de l’utilisateur final (grand public ou bien historien ?)

#### Réponses

1. Le grand public correspond à des personnes qui ne sont pas spécialistes en géomatique. Cela peut-être des scientifiques, historiens, cherchercheurs, etc...

### La visée du site 

#### Questions 

1.	Le site est à destination d'une présentation du travail de l'Institut ? Ou une utilisation pour n'importe qui souhaitant découvrir Le Caire ?
2.	Comment arrive-t-on sur le site ? En connaissant l'institut et en voulant découvrir le projet ? ou en cherchant des informations lambda sur Le Caire ?

#### Réponses

1. Le site sera utilisé dans la majorité des cas par des historiens qui n'ont pas de compétence dans l'utilisation de logiciel SIG.
2. Liens dans athar, dans l’IFAO et dans InVisu


### Validation de la forme de l'interface

Nous avons montré à Julie et Hélène un maquette de notre interface. Elles ont dans l'ensemble valider la maquette que nous avons présenter. Quelques remarques nous ont quand même été faites. 
* mettre des boutons dans la navbar pour naviguer entre les différentes pages, et un menu "burger".


Le logo de CairmodWeb a lui aussi été validé. Hélène nous a tout de même demandé de fournir le logo en niveau de gris, de founir les sources du logo (ex: logo réalisé) à partir du drapeau du caire et du logo de l'IFAO)

### Clarification concernant la VM

La VM récupère d'Openthéso l'ensemble des éléments en fonction du type de requête que l'utilisateur envoie. 

Persée est composé de perseeid, l'un des perséeid référence l'ensemble des monuments. Ce perseeid est lié à Athar. Il sera possible de récupérer une documentation avec l'ensemble des données liées à chacun des monuments. 

Pour accéder à cette documentation, la VM doit dans un premier temps récupérer l'id Openthéso du monument. La VM va dans un second temps récupérer la table de correspondance entre les ids d'Openthéso et les ids de Persée/Athar. 

Ci-dessous une image de l'architecture de notre système : 

![diagramme archi](img/diagramme_archi.jpg)

### Questions sur les fonctions de l'interface

#### Questions 

1. "Interface de recherche élaborée et possibilité de rechercher sur différents champs" : quel champ pourrait réellement intéresser un utilisateur lambda ? De plus, il y aura un blocage actuellement car ce résultat n'est pas prévu par la VM maintenue par Didier.
2.	"Fonds de carte Raster" : où seront-ils disponibles ? Via la VM ? en WMS ? Quand ?
3.	"Recherche par date" : avec la version actuelle de la VM, ce n'est pas possible en retour direct de la VM. Nous devons étudier un peu plus ce qu'il se passe dans le plugin QGIS mais le traitement en web va certainement prendre trop de temps et de ressources.

#### Réponses

1. Abandon de la recherche par valeur d’un champ particulier
2. Mettre des projets QGIS sur le QGIS serveur, il est possible de requêter le QGIS serveur pour obtenir les différents fonds raster en fonction du l’échelle de zoom. Possibilité de requêter une ou toutes les couches. Il est important que les noms des couches soient explicites. Dans les couches du projet QGIS : définir peut-être une autre sémiologie. Didier Richard a aussi mentionné la possibilité de mettre en place un MapServer, possiblement un GeoServer. Un tel serveur devra cependant permettre l'automatisation de l'ajout des fonds raster pour faciliter son utilisation.
3. Dans l’état actuelle de la VM ce n’est pas possible. L’ensemble des calculs pour récupérer les données par date se ferait coté client. Il n'est donc pas très pertinent de partir sur cette solution car le traitement peut s’avérer long et ce n’est pas en adéquation avec les concepts de Web.

## Remarques :

Réunion le vendredi 13 mars à 13h30 (dont la review du sprint 1).

## Tâches à faire

* Mettre à jour le backlog :
    * Revoir les userstories
    * définir de nouvelles userstories 