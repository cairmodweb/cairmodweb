# Compte-Rendu de réunion CairmodWeb

**Date :** 18/03/20
**Sprint :** _N02

**Objet(s) :**
1. Modification du rapport
2. Tâches individuelles
   - Julie
   - Alexis
   - Fanny 
   - Antoine
3. Autres points

**Visio :** Oui


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | R / OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Modification du rapport

En raison de certains changements majeurs et globaux réalisés sur le projet récemment, une modification du rapport est nécessaire concernant : 
- La sortie de l'API, au préalable prévue sur le même serveur Node.js que l'application Web, sur une VM distincte
- La future VM qui permettra d'accueillir les fonds raster à intégrer dans la partie "carto" du site
- Une mise à jour et une intégration au rapport du diagramme de déploiement et déploiement et du diagramme de classe
- La nouvelle organisation mise en place par le groupe (pour la communication, et le respect des méthodes agiles même à distance) en cette période de confinement en raison du Coronavirus. 
- Le nom du projet ainsi que le logo réalisé. 

### Tâches individuelles

#### Julie

Point à réaliser dans le rapport concernant les tests sur le serveur Express. La documentation pour réaliser des tests est disponible sur le dossier Google Drive. 
Julie n'ayant rien d'urgent à faire, Alexis lui lègue la tâche de la méthode d'ajout raster dans la carte, qui lui était initialement attribuée. 
Nous remarquons que ce n'est pas une seule personne qui s'occupe de réaliser une tâche et de réaliser les tests rattachés, mais bien deux personnes distinctes.

#### Fanny

Fanny travaille toujours sur les finitions concernant la mise en place de l'API. La documentation OpenAPI est terminée. 
Actuellement, l'API est mise en place avec plusieurs routes : 
- apispec : permet de visualiser la documentation de l'API
- search : permet de lancer une recherche avec les mots clés : 
   - keyword (mot clé unique)
   - directory

Dans le fichier JSON retourné par l'API, le persee id et la date ne sont pas encore ajoutés.

Décision de passer le serveur Express.js de l'application Angular sur le port 4200 (port de dev de Angular) et laisser l'API sur le port 3000.

Réponse au mail de Julie concernant le téléchargement des données. 

Demande concernant l'heure de sprint review prévue vendredi après-midi par visioconférence : 14h pour nous permettre de nous appeler à 13h30.


#### Alexis

Alexis a mis en place l'interface qui affiche la liste des couches retournées par les requêtes réalisées par les utilisateurs. Un problème est identifié : lorsque l'on quitte l'onglet "Carte", et en revant sur cette page par la suite, les couches sont dupliquées. Il faudra régler ce problème avant la merge request pour valider cette tâche. 
Cela fait ressortir une question. Lorsque l'utilisateur quitte l'onglet "Carte" pour se dirgier vers un autre onglet, les couches résultantes des requêtes s'effacent quand l'utilisateur revient sur l'onglet "Carte" ? Ou bien sont-elles conservées ? L'interrogation doit être transmise à Julie E.

De plus, Alexis s'est renseigné sur le serveur de données géographiques que nous devrons utiliser pour afficher des fonds raster historiques (Sprint 3). Ainsi, la solution que nous pourrions prévéilégier est un serveur MapServer, car très simple à mettre en place, et très robuste. Il faut également prendre en compte la simplicité de l'ajout de nouveaux fonds raster. Sur ce point c'est à l'équipe de réfléchir à une solution qui sera accessible pour les commanditaires. Toute une partie du rapport final sera dédiée à cette question. 

#### Antoine

Travail sur la userstory "Requête d'objets par unique mots-clé", et plus particulièrement sur la tâche "Interface de requête". Réalisation des formulaires pour permettre à l'utilisateur de réaliser des recherches en HTML et CSS. 

Travail à poursuivre, avec notamment la prise en compte de la modification réalisée par Fanny : l'API utilisera désormais le port 3000. 

### Autres points

- Lorsque nous arriverons au montage de la VM pour les fonds raster (au sprint 3), ne pas hésiter à prendre contact avec Didier qui nous propose de monter un serveur (48h de mise en place du serveur)
- Chaque membre de l'équipe devra relire le pré-rapport avant son rendu à Julie le vendredi 20/03 à la fin du sprint 3.


## Tâches à faire

* Penser à relire le rapport avant la fin de la semaine
* Relire tous les CR qui n'ont pas été lus avant la réunion du lendemain 
* réunion prévue le lendemain : jeudi 19 mars à 14h
* Tester la connexion à Renater pour la visio avec Julie prévue vendredi pour boucler le sprint 2 pour Antoine et Julie.
* Poser la question à Julie E. sur la nécessité de conserver les couches résultantes des requêtes lorsque l'utilisateur quitte l'onglet "Carte"