# Compte-Rendu de réunion CairmodWeb

**Date :** 20/03/2020 15h15-16h15

**Sprint :** #2

**Objets :**
1. Revue rapide du sprint 2 et questions à poser à Julie E
1. Fanny
1. Antoine 
1. Julie
1. Alexis

**Visio :** Oui


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | R/OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = Rédacteur)

## Objets
### Revue rapide du sprint 2 et questions à poser à Julie E

La branche CSS\_listing a été supprimée. La configuaration de la branche develop a été mise à jour (package_lock et node module ont été enlevés). Il faudra que Antoine et Fanny pensent bien à se mettre à jour sur leurs branches locales afin de créer le moins de conflits possible lors de leurs futures merge request.

Fanny a demandé a Didier si on doit enlever notre projet de Git. 
Sinon, elle a quand même tenue a le prévenir que nous avons reçu une alerte sur Git concernant le temps qu'il nous restait pour effectuer des pipelines de CI/CD. En effet, sur l'espace de travail TSI de Git, tous les groupes utilisent de la resource. Didier a répondu à cette question. Ainsi, Julie a mis en place ce que Didier nous a suggéré. Julie a modifié les paramètres du pipeline pour qu'il ne s'exécute qu'une fois tous les deux jours. Si nous voulons vraiment éxécuter le pipeline sans attendre les deux jours, il suffit de se rendre dans l'onglet de Git Pipeline/Schedule et d'appuyer dur le bouton play pour le lancer.


### Fanny

Fanny s'est occupée de finir les diagrammes de déploiement. Elle a aussi corrigé le pré-rapport que nous devions fournir à Julie E à la fin du sprint 2. 
Elle nous a informé qu'elle continuerait de travailler sur sa tâche en fin de journée. Elle a informé également Didier qu'il y avait un problème avec la requête par multiples mots-clés sur la VM. A la suite de cela, Didier a réglé le problème. 

### Antoine 

Antoine finalise son formulaire pour l'onglet requête dans le panel. On a decidé qu'il ne devait pour le moment pas s'attarder sur la partie des dates car la mise en place de cette fonctionnalité reste encore un peu floue. Il va commencer à regarder comment passer les mots-clés du formulaire dans les URL nécéssaires afin de requéter notre API surcouche de la VM de Didier. 

### Julie 

Julie a fini la partie concernant l'affichage des flux raster. Il est quand même nécessaire de poser quelques question au commanditaire.
- Est-ce qu'on laisse des sliders pour chaque couche raster? 
- Est-ce qu'on garde en mémoire le niveau d'oppacité de chaque couche raster ? 
- Est-ce que l'utilisateur peut changer l'oppacité d'une couche qui n'est pas visualisée ? 

### Alexis 

Mise en place de la fonctionnalité qui permet de visualiser ou non les couches vecteur. L'affichage se fait à partir d'une check box. 
Julie et Alexis ont également réglé les conflits entre leurs versions car ils travaillaient sur les mêmes fichiers. 


### Organisation au cours du projet

- Revue du sprint 2 avec la commanditaire
- Mise en place du Trello pour le sprint 3 
- Retrospective lundi matin à 10 heures
- Réunion avec Didier lundi après-midi
