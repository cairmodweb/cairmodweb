# Compte-Rendu de réunion CairmodWeb

**Date :** 21 avril (14-15H30)
**Sprint :** 6

**Objet(s) :**
1. Tâches de Julie
2. Tâche d'Antoine
3. Tâches de Fanny
4. Tâches d'Alexis
2. Test
6. Soutenance
1. Repartitiondes tâches

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | R/ |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Tâches de Julie

Julie a reglé le problème d'accent pour les requêtes par mots-clés, cela est maintenant possible. 
Julie a également fais des tests. Elle a trouvé une solution pour tester certaines fonctions ayant besoin d'une map. Elle teste ces fonctions à partir du composant Map.  

Julie a également présenté à Julie E la façon dont la BDD doit être mise à jour.


### Tâche d'Antoine

Antoine a modifé la légende pour l'export de carte. Il y avait un problème, la légende présentait toutes les couches créées par l'utilisateur même si certaines n'étaient pas afffichées sur la carte. Maintenant, nous ne mettons dans la legende que les couches visibles sur la carte. 

Antoine a également fait une merge request pour les tests qu'il a effectué. 

### Tâches de Fanny

Fanny s'occupe de la symbolique pour signaler l'ouverture et la fermeture des boutons. Fanny a décidé d'utiliser JQuery pour mettre en place dette fonctionnalité. Cependant un bug de visualisation survient quand l'utilisateur clique trop rapidement. 

### Tâches d'Alexis

Alexis a présenté à Julie E la façon de mettre à jour le mapserver avec de nouvelles images. Nous nous sommes rendus compte que la mise à jour ne s'éffectuait pas correctement pendant la présentation. Julie et Alexis ont réglé ce problème. Le mapserver est opérationnel et à jour avec l'ensemble des images que Julie a envoyé à Alexis. 
Alexis a averti Julie E que les accents n'étaient pas gérés par le mapserver ainsi il est necessaire de ne pas mettre d'accent dans les noms d'images que l'administrateur veut mettre dans le mapserver. 


### Tests et couverture

Nous devons bien tenir à jour le listing de nos tests.

Notre couverture est bof sur l'API malgré une très grande partie des fonctions qui sont testées. La partie dbtest est une grande lacune mais ne peut pas être testée.

Pour la partie application c'est également le cas et certaines parties sont encore à tester : comparaison / background / layers / export. 


### Soutenance

Le plan proposé par Fanny convient à l'équipe. Nous avons ensuite estimé le temps que prendrait chaque partie et nous nous sommes répartis les sections. 

Nous avons décidé de faire notre powerpoint avec un outil libre, nous utilisons donc latex. Fanny va faire le template de la présentation pour que tout le monde prépare sa partie pour vendredi. Nous projetons de nous entrainer vendredi matin.  


## Répartion des taches 
* Fanny : ajout de la symbolique pour signaler l'ouverture et la fermeture des boutons
* Julie : rédaction de la doc API
* Antoine : continuer des tests
* Alexis : Test + deploiement + doc déploiemenet
