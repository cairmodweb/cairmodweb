# Compte-Rendu de réunion CairmodWeb

**Date :** 16/03/2020 de 13h50 à 15h05
**Sprint :** 2

**Objet(s) :**
1. Rétrospective du sprint 1
2. Tests
3. API de requête
4. Organisation générale du travail

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | R/ OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Rétrospective du sprint 1
Nos sprints se terminent le vendredi à 12h. Nous avions donc décidé de réaliser la rétrospective de nos sprints le lundi matin suivant. Cependant, nous sommes actuellement obligés de réaliser nos réunions en visio et non en personne. Nous avons relevé la difficulté de la mise en place de rétrospectives puisque la rétrospective du sprint est censée conserver l'anonymat des participants afin d'assurer l'honnêteté et la pertinance des réponses fournies. Cela n'est cependant pas possible en visioconférence. Nous n'avons donc pas réalisé de réelle rétrospective du sprint 1 et avons donc décidé de réfléchir à la meilleure manière de gérer les prochaines rétrospectives. 

Fanny a cependant relevé notre manque de connaissance en machine virtuelle/server web ainsi qu'en mise en production comme un élément qui l'a rendue perplexe(?) (*puzzled*). 

### Tests
Actuellement, seuls les tests untiaires liés à Angular ont été étudiés et les tests `e2e` d'Angular sont en train d'être étudiés. Ils devront cependant être testés par plusieurs navigateurs et pas un seul. 

Nous avons parlé des tests unitaires et du tutoriel des tests unitaires qui a été ajouté sur le drive ainsi que les problèmes résiduels sur ces tests. Il faut regarder si il existe un équivalent à `protected` en typescript afin de pouvoir protéger nos fonctions. 

Nous avons évoqué la possibilité de devoir forcer les fonctions d'initialisation (comme `nginitafterview`) en fonction des besoins de tests. 

Nous avons décidé de mettre en place une solution de CI/CD avec une pipeline et avons décidé d'utiliser la solution proposée par Gitlab, notamment dans la continuité du projet de l'année dernière. Le responsable de tests sera chargé de cette tâche.

Nous avons aussi évoqué la nécessité de trouver une nouvelle librairie de tests pour pouvoir réaliser les tests liés à l'API de requête puisque actuellement, seuls des tests Angular sont réalisés. Une librairie de test de JavaScript/Nodejs devra donc être retrouvée. Le responsable de tests sera également chargé de cette tâche.

Nous nous sommes également mis d'accord sur la gestion des tests :
* Lorsque qu'une tâche est en QA et doit être vérifiée, le développeur doit préciser si des tests unitaires sont nécessaires ou non pour cette tâche dans le descriptif de la tâche Trello.
* Le développeur ouvre alors une merge Request avec ses modifications.
* Si des tests unitaires sont nécessaires, il doit rechercher si quelqu'un est disponible et assigner cette tâche au testeur. Si personne n'est disponible, le responsable des tests devra s'assurer que les tests sont réalisés ultérieurement.
* Le testeur doit créer une nouvelle carte sur Trello pour les tests unitaires à la tâche.
* Il doit ensuite modifier la merge request pour y rajouter les tests (et non la clore, il ne fait que pusher sur cette même branche pour mettre à jour la merge request). Les tests seront alors réalisés par le pipeline de CI/CD pour permettre la merge request. 

### API de requête
Nous avons parlé du fait et confirmé que nous n'avions pas d'accès à la machine virtuelle contenant l'API sur la machine virtuelle de Didier Richard. Pour faciliter la communication puisqu'il y a plusieurs API, nous avons jugé judicieux de trouver un nom à notre API de requête.

Nous avons parlé de la structure et de l'organisation de cette API de requête. Tout le code source de l'API sera placé dans le dossier /app/api. Elle sera mise en place grâce à un serveur ExpressJS avec des requêtes à paramètres.

Les routes de l'API seront les mêmes routes que celles de l'API de la machine virtuelle de Didier Richard. Pour répondre à nos nouvelles requêtes, certains paramètres seront cependant ajouter à la route /search :
* ?coordsrect=[xmin,ymin,xmax,ymax] : qui correspond aux coordonnées du rectangle lors de la selection par zone géographique
* ?start=&stop= : qui correspondent à la du début et la fin de l'intervalle de temps qui est requêté. Ces dates sont envoyées à notre API au format [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).

Chacune de ces routes doit renvoyer un `json`. Le `json` doit avoir la même forme qu'en sortie de l'API de la VM de Didier à l'exception de l'ajout de deux champs, tous les deux ajoutés à `properties` : le champs `perseeid` qui répertorie l'id de persée associé et le champs `date` qui contient la date associée à l'objet et qui est rempli seulement dans le cas où une requête sur les dates a été réalisée. 

Le `json` renvoyé aura donc la forme suivante :

```json
{
    "bbox":[x,y],
    "features":
    [
        {
            "geometry":{
                "coordinates":[X,Y],  
                "type":""
        },
            "properties":
            {
                "date":"",
                "perseeid": id,
                "altLabel_ala":"",
                "altLabel_ar":"",
                "altLabel_en":"",
                "altLabel_fr":"",
                "altLabel_iso":"",
                "altLabel_mul":"",
                "broadMatch":"",
                "created":"",
                "description_ala":"",
                "description_ar":"",
                "description_en":"",
                "description_fr":"",
                "description_iso":"",
                "description_mul":"",
                "editorialNote_ala":"",
                "editorialNote_ar":"",
                "editorialNote_en":"",
                "editorialNote_fr":"",
                "editorialNote_iso":"",
                "editorialNote_mul":"",
                "exactMatch":"",
                "historyNote_ala":"",
                "historyNote_ar":"",
                "historyNote_en":"",
                "historyNote_fr":"",
                "historyNote_iso":"",
                "historyNote_mul":"",
                "id":"41575",
                "isA":"3217",
                "label_ala":"",
                "label_ar":"",
                "label_en":"",
                "label_fr":"",
                "label_iso":"",
                "label_mul":"",
                "modified":"",
                "note_ala":"",
                "note_ar":"",
                "note_en":"",
                "note_fr":"",
                "note_iso":"",
                "note_mul":"",
                "related":"",
                "relatedMatch":"",
                "scopeNote_ala":"",
                "scopeNote_ar":"",
                "scopeNote_en":"",
                "scopeNote_fr":"",
                "scopeNote_iso":"",
                "scopeNote_mul":"",
                "uri":""
                },
            "type":"Feature"
            }
            ],
        "type":"FeatureCollection"}
```

### Organisation générale du travail
Nous avons décidé de réaliser une visio par jour et avons fixé la prochaine visio au 17/03/2020 à 14h.
En plus des compte-rendus de réunion avec les commanditaires ou l'équipe pédagogique, nous avons décidé de réaliser un compte-rendu de chacune de nos visios et de les relire le plus vite possible. 

Nous avons décidé de désormais privilégier Messenger à Slack dans le cadre de notre projet puisque cela nous assure souvent une réponse plus rapide. 


## Tâches à faire
Cette réunion a mis en avant la nécessité de réaliser les tâches suivantes :

* Fanny va ajouter des exemples du panel et de gestion des couches sur Gitlab.
* Relire en entier le rapport avant la fin de la semaine
* Envoyer un email à Didier Richard à propos des sujets suivants : l'accès à la machine virtuelle, le pré-rapport (date, si il faut lui envoyer ou non), et possiblement demander une confirmation par rapport à notre nouvelle architecture d'application
* Relire les compte-rendus de la semaine dernière
* Envoyer un message, possiblement sur WhatsApp à Julie à propos de la visioconférence prévue vendredi ainsi que la date du rendu du pré-rapport. Il faudra ensuite confirmer avec elle l'utilisation de la base de données dans notre architecture
