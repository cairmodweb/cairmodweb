# Compte-Rendu de réunion CairmodWeb

**Date :** 4 avril (14h-15h15)
**Sprint :** 5

**Objet(s) :**
1. Gestion du Git
2. Inventaire des tâches
    * Tuilage du flux WMS
    * Recherche par date
    * Export de cartes 
    * Balayage
3. Point sur les tests
4. Tâches personnelles pour la journée
5. A envisager dans les prochains jours

**Visio :** Non


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | R / OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Gestion du Git
Ce weed-end, Fanny a migré le Git du projet de l'espace de travail TSI2019 à l'espace Cairmodweb nouvellement créé, car le temps autorisé pour l'exécution des pipeline d'intégration continue sur la version gratuite de Gitlab était dépassé sur l'espace TSI2019. Pour être certains de ne pas dépasser le temps permis sur le nouvel espace de travail, nous maintenons un lancement du pipeline sur les branches develop et master tous les deux jours. 
Fanny a également ajouté notre commanditaire ainsi que l'équipe pédagogique de la promotion. 
Sur le git, plusieurs branches sont inutiles, et ont été mergées récemment. Elles sont donc supprimées. 
Fanny a créé un tag sur le Git pour définir la version actuelle comme étant la version 1.0.

### Inventaire des tâches
Nous réalisons un inventaire complet des tâches présentes dans les listes "to do" et "doing" du Trello. Fanny a migré l'ensemble de ces tâches dans un nouveau tableau Sprint 5.

#### Tuilage du flux WMS
La tâche tuilage du flux WMS a été passée en Q/A car le tuilage est bien fonctionnel sur QGis. Alexis n'a pas encore pu le tester sur l'application, mais logiquement cela devrait fonctionner aussi. 
Les rasters bruts sont lourds, et le tuilage réalisé par Alexis et Julie améliore le temps de traitement. POur rappel, chaque tuile a une taille de 2048x2048 pixels. Peut-être qu'en travaillant avec des tuiles encore plus petites le temps de chargement pourrait s'améliorer ? On peut aussi penser à compresser en JPG car les tuiles sont formatées en PNG actuellement. 
L'affichage des rasters a été paramétré en fonction de leur niveau de zoom. A l'arrivée, chaque raster pourra être affiché par l'utilisateur sur une plage de niveaux de zooms. 
Il reste à gérer l'affichage des rasters avec les checkboxes et l'automatisation des fonds rasters. Julie E. doit nous fournir un fichier CSV qui lie un raster à sa collection (fichier composé d'une colonne nom de collection et d'une colonne nom du fichier). On peut commencer d'écrit le script d'automatisation tant qu'on n'a pas de réponse de la partie de Julie E. 

#### Recherche par date
La merge request de Fanny concernant la requête par date a été validée ce week-end. La recherche par date fonctionne lorsqu'elle est combinée à un autre crière. Il reste à rendre possible la requête par date seule (sans la combiner à un autre critère), et à réaliser le script d'automatisation pour la mise à jour de la base de données en fonction du fichier CSV, et la déployer sur le serveur.


#### Export de cartes
La US export de carte est presque finie. Il reste à mettre en forme la légende sur le PDF généré, et rajouter la flèche nord sur la carte. Julie propose de laisser la main à quelqu'un d'autre sur ces tâches.

#### Balayage
Avec notre commanditaire, nous avons décidé de mettre en place la fonctionnalité balayage sur une nouvelle page de notre application. Julie a mis en place deux fenêtres carto qu'il est possible de balayer de gauche à droite. Nous avons échangé sur plusieurs possibilités pour paramétrer les données du balayage. Nous envisageons de mettre en place une fenêtre de paramétrage comme celle-ci : 

![Paramétrage du balayage](img/paramBalayage.JPG)

Julie a affiché un raster sur la fenêtre carto. Alexis va déposer d'autres rasters sur le MapServer pour que Julie puisse mettre en place les fonctionnalités de comparaison de deux rasters. 
Il ne reste plus que la fenêtre à créer, et gérer toutes les interactions sur l'affichage de la carte.
Julie propose une zone limite que l'utilisateur ne pourra pas dépasser sur la fenêtre carto du balayage. On ne sait pas encore vraiment si on l'empêche de dépasser la zone, ou si on affiche juste une alerte à l'écran. Mais ce n'est pas vraiment une priorité pour l'instant, on pourra voir pour faire ce genre de tâches pendant la semaine de vacances.


### Point sur les tests
Actuellement nous avons du retard sur la liste des tests à réaliser. Fanny a déjà réalisé beaucoup de tests cette semaine. Fanny doit prendre en priorité les tests de l'export de carte puisqu'il y a peu de tests qu'elle peut réaliser. Nous devons impérativement avancer sur ces tests cette semaine. 


### Tâches personnelles pour la journée
* Fanny : Requête par date : 
   * Recherche par date seulement (sans aucun autre critère)
   * Script d'automatiation de la mise à jour de la BDD
* Alexis : Déploiement de l'application sur la VM de Didier (Fanny reste disponible pour aider Alexis sur la tâche)
* Julie : Balayage, notamment avec la création d'un panel pour gérer l'affichage des fonds carto et fonds raster dans chacune des deux fenêtres
* Antoine : 
   * tests
   * affichage d'information sur les objets : rendre les couches non cliquables lorsqu'elles sont masquées

Le rapport reste une tâche prioritaire pour tous car nous devons le rendre vendredi au soir au plus tard. Parallèlement, il reste quelques diagrammes à mettre à jour pour les intégrer au rapport le plus tôt possible. 
Ce week-end, Fanny s'est chargé de créer le document Overleaf pour mettre en forme le rapport final. 


### A envisager dans les prochains jours

* Réaliser un fichier de configuration. Partout dans le code de l'application, de l'API et de la base de données pour a gestion des dates, on a entré l'adresse de l'api de dider en dur. Le fichier de config permet de définir ces éléments, que l'on peut appeler dans n'importe quel fichier de l'application.