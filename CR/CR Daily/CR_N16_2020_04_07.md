# Compte-Rendu de réunion CairmodWeb

**Date :** 7 avril (14h05-14h55)
**Sprint :** 5

**Objet(s) :**
1. Tâches de Julie
2. Tâche d'Antoine
3. Tâches d'Alexis et Fanny
4. Tests
5. Gitlab
6. Rapport

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | R/OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Tâches de Julie
Le balayage est presque fini, il reste des corrections de html ainsi qu'un problème de gestion des exceptions. 

Une démonstration du balayage actuel a été réalisée. Antoine a dit que la ligne blanche n'était pas une démarquation assez forte mais cela sera probablement non modifié. Nous avons décidé d'afficher le bouton retour à l'accueil seulement au premier affichage de la fenête et donc d'avoir l'affichage soit de retour à l'accueil soit de annuler. 

Il faudrait plus de données dans MapServer pour continuer et voir l'évolution du visuel. Alexis a sugéré de copier plusieurs fois la même couche dans le fichier du MapServer pour grandir le nombre de couches rapidement en attendant l'automatisation de la mise en place des cartes.

### Tâches d'Antoine
Antoine doit ouvrir une merge request pour ses modifications pour les pop-ups afin de les rendre inactives lorsque les marqueurs sont cachés.

### Tâches de Fanny et Alexis
Fanny et Alexis ont travaillé ensemble afin de déployer le site sur le serveur. Ils ont modifié la configuration de nginx et arrivent à accéder à localhost:4200 de l'extérieur mais il reste un problème. Les scripts buildés d'Angular ne se chargent pas côté client, ce qui entraîne une page blanche.

De plus, il n'est pas possible de build le projet directement sur le serveur. Il faut actuellement build sur un autre ordinateur puis copier les données buildées sur le serveur. Cela pourrait possiblement être réglé en mettant en place une marchine virtuelle, cela faciliterait notamment pour la maintenance future du site. La tâche s'avère cependant compliquée, il faudrait demander à Didier avant de commencer cette tâche. 

### Tests
Julie a réalisé quelques essais pour les tests liés à la BDD mais ils seront mis de côté en attendant. Ils ne sont pas prioritaires.

Un test a révélé un problème de fonctionnement du composant query. Nous nous sommes mis d'accord que peu importe la situation, si un mot-clef n'est pas validé, il n'est pas possible de valider le formulaire. 

Nous avons rappelé les règles de fonctionnement du fichier des tests :
* Il faut le garder à jour
* Fanny doit réaliser les tests de l'export. Elle a été rajoutée à tous ces tests

### Gitlab
Nous avons rappelé qu'une branche doit être supprimée quand elle est mergée.

Quand il s'agit d'une branche de tests, il faut bien préciser quels tests ont été réalisés. 

### Rapport
Le rapport est notre prioritée actuelle. Nous avons fixé jeudi 9h comme date où tout le monde devra avoir rédigé ses parties afin de laisser du temps pour la relecture et les corrections. 

Fanny a proposé de mettre en place un Trello pour les tâches liées au rapport pour rester à jour (TO-DO, DOING, QA, DONE). Il sera mis en place.

Les deux parties des besoins seront fusionnées en une. 

## Tâches à faire
* mise en place Trello rapport