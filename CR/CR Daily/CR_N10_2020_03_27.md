# Compte-Rendu de réunion CairmodWeb

**Date :** 27/03 de 14h40 à 15h
**Sprint :** 3

**Objet(s) :**
1. Tâches d'Antoine
2. Avancée d'Alexis et Julie
3. Tâches de Julie
4. Tâches de Fanny 

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI |  |
| Julie Grosmaire | ENSG - TSI |  |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | R / |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Tâches d'Antoine

Antoine s'occupe de l'affichage du pop-up. Il a un soucis avec le temps d'affichage des images qui est très long.

Il propose de charger toutes les images lors du chargement de la couche pour les avoir en câche. Nous rejetons cette solution car cela serait beaucoup trop demandeur en espace de stockage pour le navigateur. Julie propose d'essayer de réduire la résolution des images lors de la requête au serveur pour accélerer leur chargement.

### Avancée d'Alexis et Julie

Alexis et Julie ont bien avancé sur la mise en place du serveur. Un lien pour le getCapabilites est dispo sur une des cartes Trello.

Un soucis sur l'état actuel est la longueur de l'URL à requeter pour récupérer le flux. De plus, les navigateurs n'acceptent pas encore tous les tiff donc le raster est transformé en png. 

Un autre soucis est que Leaflet n'accepte notre flux qu'en `ImageOverlay`, il ne considère donc pas un flux mais vraiment une image.

Ils vont travailler à une autre solution.


### Tâches de Julie

Julie a fait une mise-à-jour des explications des tests sur le Drive.


### Tâches de Fanny

Fanny a terminé la requête géo, elle doit commenter son code et le nettoyer car elle a favorisé le fonctionnement pour la démonstration (et avoir donc la merge request avant la sprint review). 

Le problème d'overflow en x était résolu mais est réapparu, elle doit le corriger.



## Tâches à faire
