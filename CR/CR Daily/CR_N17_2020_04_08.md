# Compte-Rendu de réunion CairmodWeb

**Date :** 8 avril (14h05-15h30)
**Sprint :** 5

**Objet(s) :**
1. Tâches de Julie
2. Tâche d'Antoine
3. Tâches de Fanny
4. Tâches d'Alexis
6. Rapport

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | R/|
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Tâches de Julie

Julie a fini sa tâche sur le balayage, qui a déjà été mergée sur develop. Fanny a accepté la MR et a fait quelques remarques : Il faudrait mettre en place le bouton balayage sur la page d'accueil. Il faut peut-être changer le nom dans l'URL, pour le moment il s'apelle comparaison et le bouton s'appelle balayage. Il faudra en parler avec Julie E.. Pour la partie balayage, il faut également changer le style du message d'alerte pour dire que les couches choisies sont les mêmes. 

Julie a égalemnt fait ses parties du rapport (partie développement et analyse).

Julie a continué l'export de carte, elle a reussi à afficher les fonds raster sur l'export de carte.
Quequ'un d'autre doit regarder pour rajouter la rose des vents, modifier la symbologie de la légende et regarder pour l'opacité des couches. 

### Tâches d'Antoine
Antoine a fait du rapport sur ses parties et sur la bibliographie. Il est difficile pour lui de faire la bibliographie tout seul donc chaque personne du groupe doit l'enrichir. Il commence à faire le conclusion.

### Tâches de Fanny

Fanny a mis en place le déploiement du site sur le serveur. Pour cela elle a modifié la configuration des serveur Express et Node. Cela permet que le pare-feu de Nginx puisse prendre en compte les chemins de l'URL. 
Elle a créer une autre configuration suivant le type d'action (dev, production ou déploiement). Maintenenant pour builder une application il faut utiliser " npm run-script <action que vous voulez effectuer>". 
Dans la nouvelle configuration Fanny a également changer les URL suivant le type d'action. Les URL sont également factorisées pour rendre les changements moins contraignant. 

La configuration de Nginx a également été nétoyée de toutes les lignes non nécessaires. 

Fanny va également envoyer un mail à Julie E pour lui donner le lien du site et lui rappeler que nous avons besoin des fichiers CSV.

### Tâches d'Alexis

Alexis a aidé Fanny dans le déploiement du site. Il a paramétré le par-feu de Nginx pour les différents ports. Alexis a continué ses parties dans le rapport. Il a également fait la partie gestion de projet et il a commencé à faire la partie annexe. 
 

### Rapport

* Il faut faire attention aux références du rapport du drive car ce ne sont peut-être plus les mêmes dans le rapport latex. Il faut que tout le monde ait fini ses parties pour le lendemain afin que l'on puisse faire plusieur relectures pour le rendu de vendredi.


## Tâches à faire
* remplir les parties manquantes du rapport 
* envoyer un mail à Julie E. pour les fichier CSV et le lien du site