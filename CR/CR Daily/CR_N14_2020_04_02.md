# Compte-Rendu de réunion CairmodWeb

**Date :** 2 avril (14h-15h15)
**Sprint :** 4

**Objet(s) :**
1. Tâches de Julie
2. Tâches d'Alexis
3. Tâches d'Antoine
4. Tâches de Fanny
5. Discussions BDD dates

**Visio :** Non


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | R / OK |
| Fanny Vignolles | ENSG - TSI | OK |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets


### Tâches de Julie

En parallèle des tâches liées à la l'affichage de fonds raster avec Alexis, Julie a travailé sur la mise en place de l'export de cartes. 
Elle a fixé le bouton d'export de la carte en haut à gauche de la fenetre de visualisation cartographique. Il s'agit d'un widget Leaflet pour des raisons pratiques. Si cette idée est validée par Julie E., on abandonne l'idée de mettre le bouton dans le panel de résultats à droite. 

Julie nous montre son fonctionnement à l'écran. Au clic sur le widget, l'utilisateur choisit où il enregistre l'export au format PDF. Ce PDF est composé de la fenêtre cartographique affichée à l'écran, et des couches de résultats. Leur nom n'est pour l'instant pas très évocateur dans la légende de la carte sur l'export (Résultat 1, ..., Résultat n), mais il sera enrichit si on décide de l'intégrer dans le panel résultat une fonctionnalité qui permet de rappeller les infos de requête entrée par l'utilisateur pour arriver à ce résultat. 

On devrait ajouter des infos supplémentaires dans les sources (exemple : Cette carte a été générée par InVisu, projet CairmodWeb...). Les sources Leaflet et OSM sont intégrées en bas à droite de la vue cartographique, donc pas besoin d'y faire référence.
On doit poser la question à Julie E. si on laisse le choix à l'utilisateur d'importer la carte au format portrait ou paysage. On doit lui demain lors de la revue du sprint 4 un exemple de fichier généré par la fonction.

La question des marges sur le pdf créé se pose aussi. Si l'utilisateur souhaite imprimer sa carte, il a besoin que des marges soient rpésentes dans le pdf.

Fanny propose de laisser la possibilité à l'utilisteur de personnaliser les informations affichées sur le fichier PDF généré et aussi de rajouter un titre. Pour cela, il faudrait créer un formulaire au clic de l'utilisateur sur le widget d'export de la carte. Cette idée est approuvée par l'ensemble de l'équipe, et nous devons soumettre l'idée à notre commanditaire. 


### Tâches d'Alexis

Alexis travaille toujours sur le MapServer, avec l'aide de Julie. Hier ils avaient mis en place le tuilage des fonds raster. A l'heure actuelle on n'arrive pas à accéder aux tuiles. On dispose de leur emprise, mais elles ne s'affichent pas sur la carte Leaflet.
Ils communiquent par mail avec Didier pour tenter de régler le problème, mais sans succès. Didier souhaite que nous utilisions un MapProxy, mais Alexis et Julie ne comprennent pas comment cela pourrait régler le problème. Le MapProxy dit améliorer la rapidité de la réponse, mais ce n'est pour l'instant pas un élément bloquant de notre côté.
La tâche n'avance désormais plus. Alexis en parlera plus explicitement avec Didier demain matin lors de la visio. Alexis propose que Fanny ou Antoine se mettent sur la tâche pour apporter un regard nouveau sur la mise en place du MapServer, et identifier le blocage. 

En parallèle, Alexis a effectué des tests de fonctions ce matin.


### Tâches d'Antoine

Antoine a terminé son travail sur l'affichage d'informations liées à l'objet. Il va merger sa branche dans develop d'ici la revue de sprint demain avec Julie E. Les pop-up créées ont été soumis à notre commanditaire, qui nous soumettra comme prévu au départ toutes ses remarques pour pouvoir améliorer ce travail, surtout au niveau du rendu visuel de ces fenêtres d'info. 
Le temps d'affichage des images miniatures sur la pop-up est variable. Donc Antoine propose d'ajouter un spinner dans l'emprise où l'image doit s'afficher pour informer les utilisateurs que le chargement se fait. Lorsque l'image est chargée, on masque le spinner pour afficher l'image. 

Cet après-midi et demain matin, Antoine va réaliser des tests et avancer sur la rédaction du rapport.

### Tâches de Fanny

Ce matin, Fanny a réalisé 6 tests, mais est arrivée au bout de ce qu'elle prouvait faire sur cette tâche. Pour rappel, une seule et même personne ne peut pas réaliser le développement et les tests d'une méthode dans le code de l'application. 

Fanny a aussi travaillé sur la US de la requête par date comme hier, avec la mise en place d'une base de données MongoDB dans un Docker. A l'heure actuelle, lorsque l'on importe le CSV à l'initialisation, la base est dropée et on charge le CSV.

A l'heure actuelle Fanny se pose plusieurs questions concernant la mise en place plus approfondie de la base de données. Elles sont développées dans la partie **5. Duscussion BDD Dates **.

Fanny nous fait une démnstration rapide de son travail à l'écran. Il y a 3 fichiers pour initialiser la base de données : 
* un fichier pour créer un utilisateur et la collection
* un fichier pour importer un CSV
* un fichier pour créer les indexes.

Il reste donc à :
- Fixer les en-têtes du fichier CSV dans la définition de la base de données, pour s'affranchir d'une modfication des en-têtes du CSV non voulues par les utilisateurs. 
- Faire les squelettes de fonctions sur Express

### Discussions BDD dates

**Questions :**
1. Doit-on mettre en place une solution qui intègre la persistance des donnés ?
2. Est-ce que l'on impose un schéma à la base de données ?
3. Doit-on ignorer ou toujours tenir compte de l'entête du fichier CSV (nom des colonnes) lors de la mise en place de la base de données ?

**Discussions :**
1. A l'heure actuelle, le travail réalisé par Fanny n'intègre pas de persistance du volume Docker dans le volume local. A l'aide d'un `docker-compose up`, le volume est un volume docker et donc non persistant. Le seul avantage vu par l'équipe d'un volume persistant est peut-être un gain de temps. Cependant, la vérification du volume prend également du temps. Actuellement, la base de données se monte en 10 secondes car elle nécessite un serveur Web pour l'interface d'administration. Si on retire ce serveur Web, Fanny estime ce temps à 3 secondes. Donc cette opération est peu coûteuse en temps, et donc cela ne pose pas de problème de réimporter le fichier CSV à chaque démarrage, sans faire persister les données. Selon Fanny, pas besoin d'une persistance des données. Quand les administrateurs du site voudront mettre à jour la base de données, il n'auront qu'à modifier le fichier CSV, éteindre puis rallumer le Docker. Les commandes `docker-compose up` et `docker-compose down` doivent actuellement être lancées que dans le cas où le fichier CSV a été modifié, mais il faudra l'automatiser. 

2. Non, on n'ajoute pas de schéma car le typage de données se fait seul, et lorsque les champs sont nuls ils ne sont pas rajoutés. 

3. On peut fixer les en-têtes du fichier CSV dans la base de données. Ainsi, s'il est modifié par Julie E. ou un autre utilisateur, cela n'affectera pas le montage de la base de données, car le fichier CSV est réimporté à chaque montage de la base. 
Par la suite, le nom de chaque champ est indiqué en dur pour faire le lien entre l'API et la base de données, donc il ne doit pas varier. 
On décide donc de fixer les en-têtes dans les propriétés de la BDD.



## Tâches à réaliser
* Demander à Didier demain comment se déroulera la fin de projet (vacances toujours prévues ?, soutenance, etc...)
* Poser la question à Didier de comment tester les fonctions du MapService parce qu'on ne peut pas tester grand chose sans la map