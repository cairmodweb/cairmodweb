# Compte-Rendu de réunion CairmodWeb

**Date :** 17/03/20
**Sprint :** _N02

**Objet(s) :**
1. Revu des tâches de Fanny
1. Revu des tâches de Julie
1. Revu des tâches d'Alexis

**Visio :** Oui


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | /R OK |
| Julie Grosmaire | ENSG - TSI | OK  |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Revu des tâches de Fanny

Fanny a envoyé des mails a Didier pour accéder à l'openapi de la VM. Le fichier ainsi que la documentation ont été placés sur git dans le dosier [Doc_API](../../Doc_API). 
Fanny a également mis en place dans un dossier unique pour l'api, elle est train de refaire toutes les roots. 

### Revu des tâches de Julie

Julie a mis en place le pipeline CI/CD pour angular. La merge request pour cette étape a été faite par Fanny. 
Julie a également modifié le fichier décrivant l'étape des tests. Julie nous a décrit rapidement le fichier yml que met en place le pipeline. La partie before-script est indispensable car sans cette partie les tests ne peuvent s'éffectuer. 
Julie nous a également informés que les tests bout à bout ne peuvent pas être réalisés en local, car à priori il faut le fichier bin du navigateur pour lancer ces tests. Cependant, les tests bout à bout se réalisent bien dans le pipeline. Les tests ne pourront donc être effectués que lorsque la branche est pushée sur le git. 
Cela nous a fait remarquer une chose : nous ne pouvont pas travailler simultanément sur la même fonction avec deux branches différentes. Il est également nécessaire de ne pas toucher aux fonctions en lien avec la fonctions développées. Il est donc nécéssaire de s'informer au maximum pour éviter des problèmes lors des merges requests. 

Ce risque est a prendre en compte avec beaucoup de sérieux. 

### Revu des tâches d'Alexis

Alexis est en train de faire la user story sur la liste des couches dans le component. Voir avec julie pour modifier le mapService afin d'avoir tous les fichiers json dans un tableau. Cela nous permettra par la suite de facilement gérer les couches (affichage, drag n drop...)

## Tâches à faire

* Penser à relire le rapport avant la fin de la semaine
* Relire tous les CR qui n'ont pas été lu avant la réunion du lendemain 
* réunion prévu le lendemain : mercredi 18 mars à 14h