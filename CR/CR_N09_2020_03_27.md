# Compte-Rendu de réunion CairmodWeb

**Date :** 27/03/2020
**Sprint :** #3

**Objet(s) :**
1. Point sur la requête par date
2. Revue de sprint 3 avec demo du site
3. Réponses à diverses interrogations
4. Remarques de notre commanditaire sur le rapport
5. Tâches à effectuer

**Visio :** Oui

| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | R / OK |
| Fanny Vignolles | ENSG - TSI | OK |
| Julie Erismann | InVisu |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Point sur la requête par date

La requête par date est le 1er point abordé au cours de cette review. Pour rappel, la Userstory "Requête par date" a été sortie de notre planification après la réunion avec Julie Erismann et Hélène Bégnis (lors du sprint 1), sous réserve d'avoir de nouveaux éléments qui nous permettraient de la réaliser. 
Pour rappel, la date est contenue dans le champ "historyNote_fr" des entités retournées dans les JSON, à la suite d'une requête lancée sur la VM, mais n'est pas présente sur l'ensemble des données ou de manière non formatée. Cependant, aucun élément ne nous permet de requêter la VM en fonction de la date. Les deux solutions sont les suivantes : 

- Soit on privilégie la solution d'OpenTheso qui propose de taguer le champ HistoryNode avec la date
- Soit on propose à Julie de créer une table au sein d'une nouvelle base de données, qui nous permettra d'effectuer une première sélection sur la date des objets. 

Dans le premier cas, la requête par date est secondaire, donc pour être réalisée, une requête préalable par mot-clé ou par zone géographique est obligatoire. 

Dans le second cas, la requête par date est primaire, c'est-à-dire qu'elle ne nécessite pas d'être associée à une autre requête pour fonctionner. 

Nous devons trancher aujourd'hui sur une des deux solutions. La première dépend beaucoup d'OpenTheso, et en cette période de confinement, il est probable que le tag du champ HistoryNode arrive plus tard, après la fin de notre projet. Nous rappelons à Julie E. le fonctionnement des deux requêtes. Notre commanditaire opte pour la seconde solution, et s'engage à nous fournir un fichier CSV où est associé un identifiant OpenTheso à une date. On laisse le choix à Julie E. sur la forme du champ date, car on peut avoir des dates simples (ex : 1500) ou une plage (ex : 1500-1505).

Ce fichier nous permettra de commencer à réfléchir sur la requête par date dès le premier jour du sprint 4. 


### Revue de sprint 3 avec demo du site

Durant cette réunion, nous avons présenté toutes les tâches effectuées durant ce sprint, et fait une démonstration du site dans son état actuel à notre commanditaire. 
En raison du contexte défavorable de ce début de sprint (annonce du confinement à l'échelle nationale), l'organisation a été mouventée. Nous avons dû nous adapter rapidement à ces nouvelles mesures obligeant au travail à distance, et cela demande un certain temps d'adaptation et une oprganisation différente : cloisonnement des tâches de chaque coéquipier pour permettre l'indépendance du travail de chacun. C'est pouquoi nous n'avons pas pu achever certaines userstories et tâches programmées lors du sprint 2, mais nous avons pu attaquer certaines userstories prévues au sprint 3. A l'heure actuelle nous ne sommes donc pas en retard, ni en avance sur nos prévisions.

Rappel des fonctionnalités mises en place lors du sprint 1 :
* Mise en place de la structure du site, avec les différents onglets 
* Affichage de la carte 
* Déplacement sur la carte 
* Possibilité de zoom/dézoom sur la carte 
* Afficher des objets vecteurs sur la carte 
* Afficher les panels (le panel est retractable)

Rappel des fonctionnalités mises en place lors du sprint 2 : 
* Export d'une couche au format CSV
* Affichage de la liste des couches
* Afficher des flux raster sur la carte
* Gérer l'affichage et l'opacité des données raster
* Mise en place de l'API d'interrogration des données et de sa documentation OpenAPI
* Gérer la visibilité des couches vecteurs
* Création d'un outil d'intégration et de déploiment continus

Fonctionnalités mises en place lors du sprint 3 : 
* Requête d'objets par simple et multiples mots-clés
* Requête d'objets par sélection d'une zone géographique
* Combinaison de requêtes géographiques et par mots-clés
* Gestion de l'ordre des couches sur la carte
* Suppression de couches

L'ensemble de ces fonctionnalités a été validé par le PO. 

La validation des userstories suivantes est reportée au sprint 4 : 
* Ajouter un fond raster en tant que flux
* Affichage des informations associées à l'objet (initialement prévue au sprint 4, mais commencée au sprint 3)

Un vidéo de présentation du site sera transmise à Julie rapidement, pour la présenter aux équipes qui utiliseront le site Web, et ainsi obtenir des retours sur le travail déjà effectué.


### Réponses à diverses interrogations
Un certain nombre de questions avaient été évoquées lors de la review du sprint 2. Notre commanditaire les a transmises à l'équipe d'utilisateurs futurs du site. 

#### Questions
1. **Affichage des couches de résultat et de fond :** Devons-nous conserver une checkbox pour l'affichage / le masquage des couches ? Ou utiliser une icône "oeil" ?
2. **Nom des couches de résultat :** Est-ce qu'on garde les noms attribués au résultat : Résultat_1, Résultat_2, ..., Résultat_n ?
3. **Affichage des boutons suppression d'une couche et export en CSV :** Est-ce qu'on conserve l'affichage de ses options après un clic sur un menu "3 petits points" ?
4. **Export de la carte :** Où mettre le bouton d'export de carte ?
5. **Template du site** : Les couleurs attribuées pour l'instant conviennent-t-elles ?

#### Réponses de Julie E.
1. On conserve la checkbox.
2. On conserve ce système de nommage des couches de résultat.
3. On conserve ce système.
4. On ne rajoute pas un nouveau widget pour l'export de carte sur la carte elle même, mais en bas du panel "Résultats".
5. On conserve les couleurs actuelles, et on peut rajouter des éléments en noir.

### Remarques de notre commanditaire sur le rapport

Julie E. a relu le pré-rapport rendu à la fin du sprint 2. Elle n'a pas de remarques particulières à faire, si ce n'est de développer plus la partie contexte. Le rapport est en effet un document que des personnes qui découvrent le projet seront amenées à lire, donc il n'est pas évident de se plonger sur certains concepts comme OpenTheso, Athar, un gazetier, etc...

Julie E. nous soumets quelques conseils pour l'écriture de la seconde partie du rapport. Elle nous incite à justifier au mieux nos choix techniques, bien appuyer sur l'état de l'art réalisé, et pourquoi nous avons envisagé telle ou telle solution. 

### Tâches à effectuer
- Vidéo de présentation du site Web
- Récupérer le template pour l'export de carte envoyé par Julie
- Développer la partie 
- Faire un retour à Julie E. (sous forme d'une capture d'écran) des pop-up pour afficher des informations sur les bjets géographiques
