# Compte-Rendu de réunion CairmodWeb

**Date :** 11/03/2020
**Sprint :** #1

**Objet(s) :**
1. Revue de sprint 1 avec demo du site à la fin du sprint 1
1. Questions du flux raster
1. Questions sur la requête par date



**Visio :** Oui


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI | R /OK |
| Julie Grosmaire | ENSG - TSI | OK |
| Antoine Laurendon | ENSG - TSI | OK |
| Fanny Vignolles | ENSG - TSI | OK |
| Julie Erismann | InVisu |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Revue de sprint 1 avec demo du site à la fin du sprint 1

Durant cette réunion, nous avons présenté ce que nous avons mis en place durant ce sprint. Nous avons aussi réalisé quelques userstories qui étaient initialement prévues dans le sprint 2. 
Une grosse partie de ce sprint consistait à faire de l'analyse du projet. 
Les fonctionnalités à développer durant le sprint ont été présentées au PO (Julie E.) durant cette réunion. 
Les fonctionnalités sont les suivantes :  

* Mise en place de la structure du site, avec les différents onglets 
* Affichage de la carte 
* Déplacement sur la carte 
* Possibilité de zoom/dézoom sur la carte 
* Afficher des objets vecteurs sur la carte 
* Afficher les panels (le panel est retractable)

L'ensemble de ces fonctionnalités a été validé par le PO. 



### Questions du flux raster

Il a été prévu que nous devions mettre en place une VM afin de gérer les différents fonds de plan raster. Ces fonds seront accessibles à partir d'un flux WMS qui sera déservi par un serveur (approfondir pour savoir la techno que nous devont utiliser : QGIS serveur, MapServeur ou Géoserveur). 
Il a également été suggéré que les fonds raster soient regroupés par thématiques (aka périodes). Les différents fonds raster sont disponibles à plusieurs échelles.
Cela impliquerait que l'utilisateur choisisse sa thématique, puis les différents fonds raster de la thématique seraient visibles suivant le niveau de zoom de la carte. 



### Questions sur la requête par date

Julie a tenu à nous rappeler que la fonction permettant de requêter les objet par date était très importante. En effet, notre site devrait être utilisé majoritairement par des historiens, il est donc intéréssant pour l'utilisateur d'utiliser la date comme attribut de requête. 
Nous avons donc évoqué deux possibilités :
* celle d'utiliser la date comme deuxième critère de sélection pour travailler sur un jeu de données plus petit
* mettre en place une base de données dans une VM pour le faire. Julie founirait un fichier CSV donnant les dates associées au différents id des monuments. Nous ferions un premier filtre pour récupérer les identifiants Openthéso pour la date choisie. Les objets seront ensuite requétés avec les identifiants récupérés précédement. 


## Remarques :

Sûrement réunion le vendredi 20 Mars pour la revue de sprint 2 (horaire à confirmer).

## Tâches à faire

* Début du sprint 2, les première taches ont été réparties au seins de l'équipe : 
    * Julie : mise en place des tests
    * Fanny : faire les chemins sur l'API
    * Antoine : mise en place de l'interface de requête
    * Alexis : mise en place de l'interface de liste des couches