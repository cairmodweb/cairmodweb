# Compte-Rendu de réunion CairmodWeb

**Date :** _Date_ (heure début et fin)
**Sprint :** _Numero Sprint_

**Objet(s) :**
1. Objet 1
1. Objet 2

**Visio :** _Oui ou non_


| Presents | Groupe | Relecture\* |
| -------- | ------ | --- |
| Alexis Coatsaliou | ENSG - TSI |  |
| Julie Grosmaire | ENSG - TSI |  |
| Antoine Laurendon | ENSG - TSI |  |
| Fanny Vignolles | ENSG - TSI |  |
| Didier Richard | ENSG - DEI |  |
| Julie Erismann | InVisu |  |

\* (OK = relu / NOK = pas d'accord / R = rédacteur)

## Objets

### Objet 1

### Objet 2

## Tâches à faire