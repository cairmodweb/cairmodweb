sudo cp /home/cmweb/cairmodweb/app/cairmodweb-deploy/*.service /etc/systemd/system/
sudo systemctl reload
sudo systemctl stop cairmodweb-app.service
sudo systemctl stop cairmodweb-database.service
sudo systemctl stop cairmodweb-api.service
sudo systemctl stop cairmodweb-db-update.service
sudo systemctl start cairmodweb-database.service
sudo systemctl start cairmodweb-api.service
sudo systemctl start cairmodweb-db-update.service
sudo systemctl start cairmodweb-app.service
sudo systemctl enable cairmodweb-database.service
sudo systemctl enable cairmodweb-api.service
sudo systemctl enable cairmodweb-app.service
sudo systemctl enable cairmodweb-db-update.service