# Guide d'utilisation du site

Deux grandes fonctionnalités sont disponibles sur le site : visualiser et comparer, présentes dans deux pages du même nom. 

## 1 - Visualiser
Le site permet notamment de visualiser les données géolocalisées du thésaurus ainsi que des cartes anciennes. Cet onglet permet de visualiser les différentes données disponibles.

À l'ouverture de la page, se chargent toutes les données géolocalisées disponibles.

À la droite de l'écran, est disponible un panel avec plusieurs onglets.

### 1.1 - Requêter les données
L'onglet ouvert par défaut est l'onglet de requête. Cet onglet permet de récupérer les objets du thésaurus correspondant à des critères précis. Trois critères sont proposées :
- Les mots-clés : Tous les objets dont le nom ou la description correspondent à au moins un des mots-clés sont retournés. Il est possible de choisir autant de mots-clefs que souhaité. 
- La date : Pour la date, trois modes sont disponibles : entre deux dates, avant une date ou après une date. Cette requête retourne tous les objets ayant une information lié à l'intervalle de temps choisi. L'information peut corresponde à divers événéments : construction, destruction, existance d'une photographie prise à cette date, ...
- La zone géographique : Il est possible de dessiner directement sur la carte un rectangle. Toutes les données présentes dans ce rectangle sont alors retournées.  

Les différents critères de choix peuvent être combinés et la requête peut être lancée dès qu'un des criètes a été choisi.

### 1.2 - Exploiter ses résultats
Le deuxième onglet est l'onglet des résultats. Sont listés tous les résultats des requêtes réalisées ainsi que la couche contenant la totalité des données du gazetier chargée au démarage. Chacune de ses couches est listée avec sa légende associée. 

Il est possible d'inverser l'ordre des couches en les déplacement directement dans le panel afin d'avoir une meilleure visibilité.

Pour chaque couche, il est possible de la supprimer ainsi que d'exporter toutes les informations associées à cette couche en format csv. Ce csv contiendra toutes les informations présentes dans le gazetier, comme les coordonnées, le nom, tous les liens des images associées à l'objet. 

### 1.3 - Importer une carte
Le dernier onglet du panel est l'onglet des fonds de cartes. Dans cet onglet, sont proposées des cartes numérisées de la zone qui peuvent être affichées. Toutes ces cartes sont rangées par collection sémantique. Il est possible d'afficher une carte ainsi que l'intégralité de la collection. 

Pour chacune des couches, il est possible de changer la transparence. 

### 1.4 - Exporter la carte
Une fois toutes les manipulations souhaitées réalisées, il est possible d'exporter la carte présente à l'écran.

Sous les boutons du zoom, est présent un bouton d'export. Ce bouton permet d'exporter la présente carte en pdf. Il est possible de choisir un titre ainsi que l'orientation du pdf (portrait ou paysage).

Seront conservées toutes les requêtes visibles, le fond de carte OSM et de manière facultative les cartes (avec une opacité de 100%). L'emprise de la carte sera la même qu'à l'écran. L'export contiendra aussi le titre choisi, la légende des requêtes visibles et une rose des vents indiquant le nord. 

## 2 - Comparer
Le site propose une fonctionnalité de balayage. Cette fonctionnalité permet de comparer deux cartes différentes grâce un curseur.

À l'ouverture de la page, une fenêtre permettant de choisir les deux cartes à comparer s'affiche automatiquement. Il faut alors selectionner deux couches à comparer. Un curseur permet de se déplacer pour comparer verticalement les deux cartes.

**Attention :** Bien qu'un message d'alerte s'affiche, il est possible de comparer une carte à elle-même ainsi que deux cartes ayant des emprises différentes. Ces deux actions sont inutiles car aucune visualisation d'intérêt sera possible. Pour simplement visualiser les cartes, la fonctionalité 'visualiser' est plus appropriée.