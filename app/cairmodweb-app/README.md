# The web-app of the Cairmodweb project

## Composition

These web-application is developed with the [Angular-Cli Framework](https://angular.io/docs) and provided via an [ExpressJS server](https://expressjs.com).

## Prerequisites

* NodeJS:
    * version: recommended 12.16
    * documentation: https://nodejs.org/en/docs/
    * download: https://nodejs.org/en/download/
* Npm:
    * version: recommended 6.13
    * documentation: https://docs.npmjs.com
    * download: comes with NodeJS
* Angular-cli:
    * version: 9.0.5 or higher
    * documentation: https://angular.io/docs
    * download: `npm install -g @angular/cli@9.0.5`
* MkDocs:
    * version: version 1.0.4 or higher
    * documentation : https://www.mkdocs.org/
    * download : 'sudo apt-get install -y mkdocs'



## How to run the app locally

### Development mode: modification in real time

Start the web-application with modification in real time
```bash
npm install
npm run-script serve
```
Then, you can see your modification in real time at http://localhost:4200/.


### Production mode: build application

The build application differs when you want a local applciation or a deployment on a server.

**Precision:** The built app will be available in a new repository name _dist_, it is the Single Page Application built by Angular. These repository is like the public repository for expressJS server. In other words, the back-end has a **public** repository which gathers all _html_, _css_ and _js_ files that needed to be sent to the user browser. 

#### Local application
1. Build the application:
    ```bash
    npm install
    npm run-script build
    ```
1. Start the express server from the cairmodweb-app folder to display the app:
    ```bash
    npm start
    ```
    **NB :** Then, you can see the application at http://localhost:4200/.

#### Deployment on a server

Some modification has to be taken into account for a server deployment:
* the `base-href` depends on the server proxy configuration
* the environment variable of our application can change such as the address of the other components.
In this case, we need to consider the environment file for production [environment.prod.ts](./src/environments/environment.prod.ts) instead of the basic one [environment.ts](./src/environments/environment.ts). This change is provided in the [angular.json](./angular.json) file.

Consequently, to build and run the app on your server you will need to run the following commands:
```bash
npm install
npm run-script build-deploy
npm start
```

## Configure the environment variable of the web-app

The web-app has environment files that define some global parameters sush as:
* the API address
* the mapServer address

Those parameters has to be adapted to the context and to the deployment methods. You can modify them in the following files:
* For the deployment on a server: [environment.prod.ts](./src/environments/environment.prod.ts).
* For a local deployment: [environment.ts](./src/environments/environment.ts)


## Test the web-app

### Tests
You can run the tests of the webApp with:
```bash
npm install
npm test
```
Then, the results are shown in your terminal, but also in any browser at http://localhost:9876/ (or the port that is shown at the start of the test in your terminal, depends on your device). 

**Be careful:** The default browser for testing is Chrome, so if you test in a other browser with http://localhost:9876/, the number of tests doubles because they are run for Chrome (by default) but also for your browser.


### Code coverage

You can also get the code coverage result of the webApp with the following commands:
```bash
npm install
npm run-script test-coverage
```
Then, the results are shown in your terminal succinctly. 

Moreover, a new folder has been created in your tree view : [coverage](./coverage). It contains an HTML app to visualize your results in a more pleasant view. You can access it by opening in a browser the local file [coverage/cairmodweb-app/index.html](./coverage/cairmodweb-app/index.html).



