// Get dependencies
const express = require('express');
const http = require('http');

// Get our webapp routes
const webapp = require('./server/routes/app.js');

// Define the main express worker
const app = express();


// Set our doc route
app.use('/doc',express.static(__dirname + '/server/routes/app-doc/site'));

app.get('/doc', function (req, res){
    res.sendFile(__dirname + '/server/routes/app-doc/site/index.html');
});

// Set our test route
app.get('/test', (req, res) => {
  console.log("ExpressJS Server Test requested")
  res.send("ExpressJS Server works");
});

// Set our webapp route
app.use('/', webapp);


/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '4200';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));