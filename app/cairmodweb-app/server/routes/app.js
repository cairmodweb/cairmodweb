const express = require('express');
const path = require('path');
// Define router
const router = express.Router();

// Variables for allowed type for external files
const allowedExt = [ '.js', '.ico', '.css', '.png', '.jpg', '.woff2', '.woff', '.ttf', '.svg','.pdf'];


/* GET api listing. */
router.get('/state', (req, res) => {
    console.log("webapp state requested")
    res.send('webapp state works');
});

// Catch all other routes and return the index file
router.get('*', (req, res) => {
    // If a specific files is required such as scripts, css, images, etc.
    if (allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
        console.log("webapp file requested")
        res.sendFile(path.join(__dirname, `../../dist/cairmodweb-app/${req.url}`));
    } 
    // Else return the SPA Angular
    else {
        console.log("\nwebapp requested")
        res.sendFile(path.join(__dirname, '../../dist/cairmodweb-app/index.html'));
    }
});

// Point static path to dist
router.use('*', express.static(path.join(__dirname, '../../dist/cairmodweb-app')));



module.exports = router;