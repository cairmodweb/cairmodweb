# Modifications du site

## Prise en compte des modifications

Les modifications sur l'ensemble de l'application ne sont prises en compte sur le serveur qu'au déploiement de l'application sur celui-ci.

Les modifications peuvent être visualisée en temps réel en mode développement (cd partie [Développement](../developpement)).



## Configurer les différentes parties du projet

### Configurer la web-application

La configuration de la web-application peut être mise à jour dans les fichiers environnements présents dans le dossier `src/environments` de `cairmodweb-app`. 

Ils existent trois fichiers d'environnement :

- `environment.ts` : fichier d'environnement pris en compte lors du développement et de la compilation locale via `npm run-script build`.
- `environment.prod.ts` : fichier d'environnement pris en compte lors de la compilation pour le déploiement sur un server via `npm run-script build-deploy`.
- `environment.dev.ts` : fichier d'environnement non pris en compte à l'heure actuelle.

Ces fichiers définissent des paramètres globaux de la web-application tels que :

- Les urls externes : `urls`
    - l'adresse de l'API : `urls.api`
    - l'adresse du serveur cartographique pour les fonds raster : `urls.mapserver`
- Les urls internes à la web-application (urls chemin des différentes pages du site) : `internalUrls`
- Les paramètres du gazetier pris en compte dans l'application : `gazeteer`
    - le numéro du gazetier : `gazeteer.thid`
    - les coordonnées initiales du centre de la carte : `gazeteer.mapInitialCoordinates`
    - le zoom initial de la carte : `gazeteer.mapInitialZoom`
    - la date minimale pour les recherches par date : `gazeteer.minimalDate`


Il est donc important de mettre à jour **tous ces fichiers** pour un changement des paramètres du gazetier ou des urls internes à la web-application.

Lors d'un déploiement sur un nouveau serveur ou une modification dans les accès de l'API ou du mapServer, il faut mettre à jour les urls externes du fichier `environment.prod.ts`.


### Configurer l'API

La configuration de l'API peut être mise à jour dans le fichier `config.js` de `cairmodweb-api`. 

Il définit des paramètres globaux de l'API tels que :

- Les urls externes : `urls`
    - l'adresse de la VM CAIRMOD : `cairmodVM`
    - l'adresse de la base de données Mongo : `uriDB`
- Les paramètres de la base de données : `database`
    - le nom de la base de données : `dbName`
    - le nom de la collection : `monuments`
- Le paramètre de conversion de coordonnées entre degrés et kilomètres : `degkmconversion`

**Attention :** Les paramètres de la base de données sont présents dans le fichier de configuration mais ne sont pas encore pris en compte en tant que variable globale dans le code. Il faut donc mettre à jour ces paramètres dans le code en dur du dossier `routes`:

- Dans le fichier `api.js` : dans la fonction `getIdFromDBWithDate()`(lignes 546-548).
- Dans le fichier `dbtest.js` : à chaque appel de la fonction `database.connect()` (lignes 17-31-55) puis dans le `.then()`qui la suit.

### Configurer la base de données

La base de données est gérée par le biais d'un docker-compose dont l'ensemble des fichiers de paramètres se trouvent dans `cairmodweb-db`. Ainsi, les paramètres modifiables sont :

- Le nom de la base de données : 
    1. dans le fichier `docker-compose.yml`, modifier la variable `MONGO_INITDB_DATABASE`
    1. dans le fichier `00_init-mongo.js` du dossier `mongo-init`, modifier à la ligne 24 la valeur de la variable `db`.
    1. dans le fichier `01_import-csv-mongo.sh` du dossier `mongo-init`, modifier à la ligne 8 la variable juste après le flag `-d`.
    1. dans le fichier `02_create-index-mongo.js` du dossier `mongo-init`, modifier à la ligne 2 la variable prise par la fonction `db.getSiblingDB()`.
    1. **NB :** le nom de la base de données est à mettre à jour aussi dans l'API.
- Le nom de la collection :
    1. dans le fichier `00_init-mongo.js` du dossier `mongo-init`, modifier à la ligne 33 la variable prise par la fonction `db.createCollection()`.
    1. dans le fichier `01_import-csv-mongo.sh` du dossier `mongo-init`, modifier à la ligne 8 la variable juste après le flag `-c`.
    1. **NB :** le nom de la collection est à mettre à jour aussi dans l'API.






## Mettre à jour le contenu du serveur cartographique


### Fonctionnement général

Le serveur cartographique est hébergé sur le serveur courant et est automatiquement mis à jour lors d'une détection d'un changement dans le dossier `/home/cmweb/data_admin/`, inspecté toutes les 5 minutes et qui contient les fichiers suivants :

- un csv de référence listant les collections et leurs images associées : `20200408_coll_cartes_web.csv`. 
- un dossier contenant l'ensemble des images rasters du serveur cartographique : `img_mapserver`.

**Attention :** Les accents ne sont pas acceptés dans les noms de collection ou d'image.


### Fichier listant les images

Le fichier `20200408_coll_cartes_web.csv` doit répondre aux critères suivant pour être valide :

- Une ligne par image
- Le format d'une ligne est le suivant : `nom_collection,nom_image`


### Mise à jour sur le serveur

Le fichier csv peut être modifier directement ou un nouveau csv peut être copié pour le remplacer, notamment grâce à la commande suivant :
```bash
sudo scp <path to your file>file.csv cmweb@217.160.173.154:/home/cmweb/data_admin/20200408_coll_cartes_web.csv
```

Il faut aussi tenir à jour les images présentes dans le dossier `img_mapserver`.


Ajouter une image :
```bash
sudo scp <path to your files>*.tif cmweb@217.160.173.154:/home/cmweb/data_admin/img_mapserver/
```

Supprimer une image :
```bash
cd /home/cmweb/data_admin/img_mapserver
rm img_to_remove
```



## Mettre à jour la base de données des dates

### Fonctionnement général
La base de données est hébergée sur le serveur courant et est automatiquement mise à jour à partir d'un csv de référence. Ce csv est présent dans le dossier `/home/cmweb/data_admin/data_csv/` et porte le nom de `date.csv`. C'est ce csv qu'il faut modifier pour déclencher une mise à jour.

### Fichier de mise à jour
Le fichier .csv doit répondre aux critères suivants pour entraîner une mise à jour :

- Le délimiteur utilisé est la virgule.
- L'entête comprend les champs suivants dans cet ordre : `id_opentheso`, `nom`, `date`, `date_fourchette_debut`, `date_fourchette_fin`, `note_date`.
- Chaque ligne correspond à une date ou à un intervalle de date (date fourchette). Ainsi l'`id_opentheso` n'est pas forcément unique. Un objet peut être présent autant de fois qu'il présente une date d'intérêt.
- Si les champs de `date_fourchette_debut` et `date_fourchette_fin` fonctionnent ensemble. Si l'un est renseigné l'autre doit l'être aussi.
- Le champs `date` et l'ensemble `date_fourchette_debut`-`date_fourchette_fin` ne peuvent pas être simultanément remplis. Sinon, seule la première date sera prise en compte.

### Mettre à jour sur le serveur
Le fichier csv peut être mis à jour directement ou un nouveau csv peut être copié pour le remplacer, notamment grâce à la commande suivante.
```bash
scp <pathtofile>file.csv user@server-ip:/home/cmweb/data_admin/date_csv/date.csv
```

La mise à jour des données est automatiquement déclenchée au changement du csv et peut entraîner une coupure de service de l'ordre de la minute. 


## Modifier les contenus de la web-application

#### Généralités

Les différents contenus HTML sont modifiables dans les fichiers HTML de la web-application (dans `cairmodweb-app`). 

Il faut toutefois faire attention au fait que la web-application est développée via [Angular (cli)](https://angular.io/). Ainsi, la gestion des divers fichiers HTML est particulière. 


Les modifications faites sur ces fichiers peuvent être visualisées en temps réel à l'adresse [http://localhost:4200/](http://localhost:4200/) avec la commande : 
```bash
npm run-script serve
```
Elles seront prises en compte lors de la prochaine compilation de l'application. Il faut ensuite effectuer un nouveau déploiement du site (cf [partie Déploiement](../deploiement)).


#### Texte de la page _Accueil_

Le texte de la page _Accueil_ est modifiable dans le fichier `home.component.html` du dossier `src/app/components/home`.


#### Texte de la page _S'informer_

Le texte de la page S'informer est modifiable dans le fichier `info.component.html` du dossier `src/app/components/info`.


## Modifier le style de la web-application

### Charte graphique

Le fichier `styles.css` présent à la racine de `cairmodweb-app` est le fichier de style global de la web-application. Son contenu sera appliqué dans l'ensemble des component Angular de la web-application.
Notamment, la couleur rouge de _CairmodWeb_ est définie de manière globale dans ce fichier. Pour modifier le rouge de l'application dans l'ensemble du site, il suffit donc de modifier la valeur de `--cairmod-color` dans `styles.css`.

Toutefois, chaque component Angular détient aussi son propre CSS. Cela permet notamment de définir des règles CSS propre au component Angular et qui, de ce fait, ne s'appliqueront pas aux autres components (à l'exception de ces compoments enfants).
Pour modifier le style CSS particulier d'un component, il faut donc aller directement dans son fichier CSS propre.


### Logo

Le logo de l'application est le fichier `favicon.ico` de `cairmodweb-app`. 

**NB :** Le logo ne doit pas être trop lourd et doit toujours être nommé `favicon.ico`. En effet, l'appelation favicon est propre au fonctionnement web, elle représente la vignette sur l'onglet du navigateur. Si elle n'est plus présente à la racine de la web-application, certains navigateurs web peuvent présenter une erreur en console.



## Modifier la documentation

### Documentation utilisateur de la web-application

La documentation utilisateur est disponible en téléchargement sur la page _Accueil_ du site. Elle est rédigée en Markdown puis convertie en PDF par le biais du module NodeJs [markdown-pdf](https://www.npmjs.com/package/markdown-pdf) lors de la compilation de l'application.

Son contenu est modifiable depuis le fichier guide.md disponible dans le dossier `utils/UserGuide` de `cairmodweb-app`. La modification sera prise en compte lors de la prochaine compilation de l'application. Il faut ensuite effectuer un nouveau déploiement du site (cf [partie Déploiement](../deploiement)).

### Documentation administrateur du projet

La documentation que vous êtes en train de lire est générée en utilisant [MkDocs](https://github.com/mkdocs/mkdocs/). 

Elle est rédigée en Markdown. Vous pouvez la modifier dans le dossier `server/routes/app-doc` de `cairmodweb-app`. Sa modification sera effective à la prochaine compilation de l'application. Il faut ensuite effectuer un nouveau déploiement du site (cf [partie Déploiement](../deploiement)).

Vous pouvez visualiser en temps réel les changements réalisés sur la documentation avec la commande suivante lancée depuis la racine de `cairmodweb-app` :
```bash
cd ./server/routes/app-doc/
mkdocs serve
```
Le site est visible à l'adresse [http://localhost:8000](http://localhost:8000)