# Déploiement 

L'application étant constituée de trois briques d'architecture (cf page [Architecture](./architecture)), pour la déployer, il faut déployer les différentes briques selon les étapes de déploiement suivantes :

1. Télécharger le projet git sur le serveur
1. Installer les dépendances
1. Configurer le reverse proxy
1. Configurer le serveur cartographique
1. Compiler la web-application avec les paramètres de déploiement
1. Activer les services systemd de la base de données, l'api, l'application web

Il est important de réaliser toutes ces étapes dans l'ordre. Dans le cas d'un déploiement sur un serveur où l'application aurait déjà était dépoyée par le passé, la liste des étapes à rélaiser doit être adaptée en fonction des modifications réalisées. 


## 1 - Télécharger le projet git sur le serveur

La première étape consiste à extraire le projet git sur un serveur à partir d'une version :
```bash
cd ~
git clone -b <tag_release> https://gitlab.com/cairmodweb/cairmodweb.git
```
Un dossier nommé `cairmodweb` est créé à la racine utilisateur. Il contient tout ce qui est nécessaire pour déployer l'application.

Pour les étapes suivantes, il faut adapter le nom du serveur utilisateur et le chemin du projet. Nous considérerons ici que:

- l'utilisateur est `cmweb`.
- l'adresse du serveur de déploiement est `217.160.173.154`.
- le chemin du projet est `/home/cmweb/cairmodweb/`.
- votre répertoire de travail actuel est `/home/cmweb/`.


## 2 - Installer les dépendances

### 2.1 - Reverse proxy
- Nginx :
    - version : recommandée 1.14.2
    - documentation : [https://docs.nginx.com/nginx/](https://docs.nginx.com/nginx/)
    - installation : [https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/)

### 2.2 - Serveur cartographique
- MapServer :
    ```
    sudo apt-get install cgi-mapserver mapserver-bin mapserver-doc libmapscript-perl python-mapscript ruby-mapscript
    ```
- Spawn-fcgi :
    ```
    sudo apt-get install spawn-fcgi
    ```
- GDAL :
    - `sudo apt-get install gdal-bin`
    - `sudo apt-get install libgdal-dev`
    - `sudo apt-get install python-gdal`
- Pillow : `pip install pillow`

### 2.3 - Base de données
- Docker :
    - version : recommandée 19.03
    - documentation : [https://www.docker.com/](https://www.docker.com/)
- Docker-compose :
    - version : recommandée 1.25
    - documentation : [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
    - installation : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
- NodeJS :
    - version : recommandée 12.16
    - documentation : [https://nodejs.org/en/docs/](https://nodejs.org/en/docs/)
    - installation : [https://nodejs.org/en/installation/](https://nodejs.org/en/installation/)
- Npm :
    - version : recommandée 6.13
    - documentation : [https://docs.npmjs.com](https://docs.npmjs.com)
    - installation : comes with NodeJS

### 2.4 - API
- NodeJS
- Npm
- MkDocs :
    - version : 1.0.4 ou supérieure
    - documentation : [https://www.mkdocs.org/](https://www.mkdocs.org/)
    - installation : [https://github.com/mkdocs/mkdocs](https://github.com/mkdocs/mkdocs)

### 2.5 - WebApplication
- NodeJS
- Npm
- MkDocs
- Angular-cli :
    - version : 9.0.5 ou supérieure
    - documentation : [https://angular.io/docs](https://angular.io/docs)
    - installation : `npm install -g @angular/cli@9.0.5`


## 3 - Configurer le reverse proxy

Nginx est utilisé sur le serveur comme reverse proxy pour le port http (:80). Il gère la redirection des requêtes vers le serveur local approprié :

- `/cairmodweb/`: redirection vers le serveur de la web-application [http://localhost:4200/](http://localhost:4200/).
- `/api/`: redirection vers le serveur de l'API [http://localhost:3000/](http://localhost:3000/).
- `/mapsw/`: redirection vers le serveur cartographique on [http://localhost:9999/](http://localhost:9999/).


### 3.1 - Configurer Nginx

1) Placer la configuration Nginx dans le dossier nginx du système :
```bash
sudo cp /cairmodweb/app/cairmodweb-deploy/cairmodweb-nginx.conf /etc/nginx/sites-enabled/default
```

2) Ajouter la page pour l'erreur 404 :
```bash
sudo cp /cairmodweb/app/cairmodweb-deploy/custom-pages/custom_404.html /usr/share/nginx/html
```

3) Vérifier le fichier de configuration : 
```bash
sudo nginx -t
```
S'il est ok, alors continuer, sinon rapporter un bug dans le projet git.


4) Recharger la configuration Nginx :
```bash
sudo /etc/init.d/nginx reload
```

## 4 - Configurer le serveur cartographique

Notre serveur cartographique est un MapServer utilisant une configuration FastCGI. GDAL est utilisé pour assembler les images à fournir au MapServer. 


### 4.1 - Démarrez MapServer

Démarrez le service MapServer
```bash
sudo /etc/init.d/mapserv start
```

### 4.2 - Configurer FastCGI

1) Placer le fichier `mapserv` de `cairmodweb-mapserv` dans le dossier `/etc/init.d` du serveur :
```bash
sudo cp /cairmodweb/app/cairmodweb-mapserv/mapserv /etc/init.d/mapserv
```

2) Modifier les permissions sur le fichier `mapserv` :
```bash
sudo chmod +x /etc/init.d/mapserv
```

### 4.3 - Configurer l'automatisation d'ajout de fonds raster

1) Créer plusieurs dossiers avec les commandes suivantes :
```bash
 sudo mkdir /home/cmweb/data_admin
 sudo mkdir /home/cmweb/data_admin/img_mapserver
```

2) Placer le fichier `template_mapfile.map` de `cairmodweb-mapserv` dans le dossier du serveur `/home/cmweb/data_admin/` :
```bash
sudo cp /cairmodweb/app/cairmodweb-mapserv/template_mapfile.map /home/cmweb/data_admin/template_mapfile.map
```

3) Placer le fichier `automatisation_raster.py` de `cairmodweb-mapserv` dans le dossier du serveur `/home/cmweb/data_admin/` :
```bash
sudo cp /cairmodweb/app/cairmodweb-mapserv/automatisation_raster.py /home/cmweb/data_admin/automatisation_raster.py
```

4) Placer le fichier `modif-folder-img.sh` de `cairmodweb-mapserv` à la racine utilisateur du serveur :
```bash
sudo cp /cairmodweb/app/cairmodweb-mapserv/modif-folder-img.sh /home/cmweb/modif-folder-img.sh
```

5) Ecrire dans crontab pour mettre en place l'exécution automatique du script de mise à jour toutes les 5 minutes :
```bash
sudo crontab -e
```
L'éditeur du terminal s'ouvre, rajouter la ligne :
```bash
*/5 * * * * /home/cmweb/modif-folder-img.sh 1 >> /home/cmweb/log-cron/log.txt 2 >> / home / cmweb / log-cron / error.txt
```


## 5 - Compiler la web-application avec les paramètres de déploiement

La webApp doit être compilée pour être déployée. Le processus de génération n'est pas le même que celui d'un déploiement local. Les commandes sont les suivantes:
```
cd /cairmodweb/app/cairmodweb-app/
npm install
npm run-script build-deploy
```

Il est possible que votre application ne soit pas compilée en raison d'une mémoire insuffisante pour la pile de JavaScript. Dans ce cas, vous devez :

1) Compiler l'application sur une autre machine (par exemple votre ordinateur) mais toujours avec la commande précédente `build-deploy`.

2) Télécharger le dossier de build sur le serveur :
```bash
scp -r <my_project_folder>/cairdmoweb/app/cairmodweb-app/dist cmweb@217.160.173.154:/home/cmweb/cairmodweb/app/cairmodweb-app/
```

3) Télécharger le dossier de la documentation compilée sur le serveur :
```bash
scp -r <my_project_folder>/cairdmoweb/app/cairmodweb-app/server/routes/app-doc/site cmweb@217.160.173.154:/home/cmweb/cairmodweb/app/cairmodweb-app/server/routes/app-doc/
```



## 6 - Activer les services systemd de la base de données, l'api, l'application web

Les services peuvent être activés de manière automatique par le lancement d'un script shell ou manuellement par la réalisation de plusieurs commandes dans le terminal.


### 6.1 - Activation automatisée

Les services systemd peuvent être activés par le lancement du script `deploy.sh` de `cairmodweb-deploy` :
```bash
sudo sh /cairmodweb/app/cairmodweb-deploy/deploy.sh
```

Ce script permet de réaliser de manière automatique les étapes de mise à jour et activation des services.

### 6.2 - Activation manuelle


1) Placer les quatre services de `cairmodweb-deploy` dans le dossier `/sytem` :
```bash
sudo cp /cairmodweb/app/cairmodweb-deploy/*.service /etc/systemd/system/
```

2) Rafraichir les services du systemd :
```bash
sudo systemctl reload
```

3) Démarrer les services :
```bash
sudo systemctl start cairmodweb-database.service
sudo systemctl start cairmodweb-api.service
sudo systemctl start cairmodweb-app.service
sudo systemctl start cairmodweb-db-update.service
```

4) Activer les services pour qu'ils démarrent automatiquement à chaque redémarrage du serveur :
```bash
sudo systemctl enable cairmodweb-database.service
sudo systemctl enable cairmodweb-api.service
sudo systemctl enable cairmodweb-app.service
sudo systemctl enable cairmodweb-db-update.service
```

### 6.3 - Surveiller les services
L'état de chaque service systemd peut être surveillé avec la commande suivante :
```bash
sudo systemctl status <the_service>
```
