# Documentation du projet CairdmoWeb

Le projet CairmodWeb consiste à mettre en place un webSIG ergonomique qui intègre le module de requête et d’affichage des documents iconographiques d'un corpus sur le Caire moderne.

Ce projet a été réalisé par un groupe de deux élèves ingénieur en fillière TSI et de deux élèves en Master 2 TSI entre le 4 mars 2020 et le 24 avril 2020.

**Remarque :** 
La documentation du projet CairmodWeb est aussi disponible par le biais de divers README au sein du projet git.
