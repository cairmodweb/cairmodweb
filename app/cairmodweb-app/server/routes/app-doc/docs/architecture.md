# Architecture du projet CairmodWeb

## Généralités

L'architecture du projet CairmodWeb est constituée de quatre briques :

- Une web-application : `cairmodweb-app`
- Une API :  : `cairmodweb-api`
- Une base de données :  : `cairmodweb-db`
- Un serveur cartographique : `cairmodweb-mapserv`

Elles interagissent aussi bien ensemble qu'avec l'exterieur :

![Diagramme d'architecture](img/diag_archi.png)


## Précisions

### La web-application

La web-application est développée via [Angular (cli)](https://angular.io/) et diffusée via un serveur [ExpressJS](https://expressjs.com). Ainsi, elle possède une architecture particulière : (tous les items ne sont pas représentés, certains peuvent n'apparaître qu'après la compilation ou le lancement des tests)
```bash
cairmodweb-app
|   |-- code-coverage
|   |-- dist
|   |-- e2e
|   |-- server
|   |   |-- routes
|   |   |   |-- app-doc
|   |   |   |-- app.js
|   |-- src
|   |   |-- app
|   |   |   |-- components
|   |   |   |-- services
|   |   |   |-- app-routing-module.ts
|   |   |   |-- app.component.css
|   |   |   |-- app.component.html
|   |   |   |-- app.component.spec.ts
|   |   |   |-- app.component.ts
|   |   |   |-- app.module.ts
|   |   |-- assets
|   |   |-- environments
|   |   |-- favicon.ico
|   |   |-- index.html
|   |   |-- main.ts
|   |   |-- polyfills.ts
|   |   |-- popups.css
|   |   |-- styles.css
|   |   |-- text.ts
|   |-- utils
|   |   |-- UserGuide
|   |   |   |-- guide.md
|   |   |-- generateUserGuide.js
|   |-- angular.json
|   |-- karma.conf.js
|   |-- package-lock.json
|   |-- package.json
|   |-- server.js
|   |-- tsconfig.app.json
|   |-- tsconfig.json
|   |-- tsconfig.spec.json
|   |-- tslint.json
```

**Remarques :**

- Le dossier `code-coverage`contient les résultats de la couverture des tests. Il n'est visible et actualisé que lors du lancement de la commande :
    ```
    npm run-script test-coverage
    ``` 

- Le dossier `dist`contient l'application compilée. Il n'est visible et actualisé que lors du lancement d'une des deux commandes suivantes :
    - Compilation pour déploiement local
        ```
        npm run-script build
        ``` 
    - Compilation pour déploiement sur un serveur
        ```
        npm run-script build-deploy
        ``` 

- Le dossier `src/app/components` contient l'ensemble des components Angular de la web-application avec pour chaque component les fichiers suivants :
    - `monComponent.component.css` : le style propre au component.
    - `monComponent.component.html` : le composant HTML propre au component.
    - `monComponent.component.spec.ts` : le fichier de test du component.
    - `monComponent.component.ts` : le code TypeScript propre au component.

- Le dossier `src/app/services` contient l'ensemble des services Angular de la web-application avec pour chaque service les fichiers suivants :
    - `monService.service.spec.ts` : le fichier de test du service.
    - `monService.service.ts` : le code TypeScript propre au service.

- Le dossier `src/assets` contient les fichiers externes utiles (images, etc.).

- Le dossier `src/environments` contient les fichiers de configuration de la web-application.


### L'API

L'API est une API REST développée en JavaScript et diffusée via un serveur [ExpressJS](https://expressjs.com). Son architecture est la suivante :
```bash
cairmodweb-api
|   |-- bdd
|   |   |-- db.js
|   |-- node_modules
|   |-- public
|   |   |-- openapi.json
|   |-- routes
|   |   |-- api-doc
|   |   |-- api.js
|   |   |-- dbtest.js
|   |-- tests
|   |   |-- api.spec.js
|   |   |-- db.spec.js
|   |   |-- server.spec.js
|-- config.js
|-- package-lock.json
|-- package.json
|-- server.js
```

Une documentation spécifique à l'API est disponible sur la route `/doc` de celle-ci :
- [http://217.160.173.154/api/doc](http://217.160.173.154/api/doc) depuis le serveur de déploiement.
- [http://localhost:3000/doc](http://localhost:3000/doc) quand l'API est lancée localement.

### La base de données

La base de données est un docker-compose associé à un serveur un serveur [NodeJS](https://node.com) pour surveiller sa mise à jour. Son architecture est la suivante :
```bash
cairmodweb-db
|   |-- mongo-init
|   |   |-- 00_init-mongo.js
|   |   |-- 01_import-csv-mongo.sh
|   |   |-- 02_create-index-mongo.js
|   |   |-- init_csv.csv
|   |-- utils
|   |   |-- updateBDDfunction.js
|-- .gitignore
|-- date.csv
|-- docker-compose.yml
|-- package-lock.json
|-- package.json
|-- updateDatabase.js
```

### Le serveur cartographique

Le serveur cartographique est un [MapServer](https://mapserver.org).