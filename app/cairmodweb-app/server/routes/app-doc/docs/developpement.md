# Développement

Pour le mode développement, les briques web-application, API et base de données doivent être démarrées. 

## Prérequis

La web-application, l'API et la base de données nécessitent l'installation de dépendances comme expliqué dans la partie [Déploiement](../deploiement/#2-installer-les-dependances).

Un serveur cartographique doit aussi être accessible pour les fonctionnalités de la web-application telles que les fonds de carte raster et la page de Comparaison.

## Démarrer les différentes briques

Toutes les commandes suivantes sont à exécuter depuis la racine du dossier git `cairmodweb`.

### La base de données
L'image Docker de MongoDB est utilisée. Après s'être assuré que la base de données n'est pas déjà présente, la commande suivante permet de la mettre en place.
```bash
cd ./app/cairmodweb-db
docker-compose down
docker-compose up
```

Une fois la base de données mise en place avec docker-compose, il est possible de consulter manuellement les données qui y sont enregistrées. 

Il existe un interface administrateur où il est possible de consulter les données : [http://localhost:8081](http://localhost:8081).

Il est également possible d'y accéder par le conteneur docker. Les commandes suivantes sont à lancer :
```
docker container exec -it cairmodweb-db_mongo_1 bash
```
Puis dans le bash du conteneur : 
```
mongo -u root -p root
```
Il est alors possible de requêter la base de données. Les commandes possibles sont alors détaillées [ici](https://docs.mongodb.com/manual/reference/method/).



### L'API

L'API fonctionne par le biais d'un serveur NodeJS qui se démarre avec la commande suivante :
```bash
cd ./app/cairmodweb-api
npm install
npm start
```

Le service sera alors disponible à l'adresse [http://localhost:3000](http://localhost:3000). 

Une documentation spécifique à l'API est disponible sur la route `/doc` de celle-ci [http://localhost:3000/doc](http://localhost:3000/doc).


### La web-application

La web-application est une application Angular diffusée par le biais d'un serveur NodeJS qui se différemment en fonction du mode à tester.


#### Mode développement : visualiser les modifications en temps réel
Pour visualiser localement en temps-réel les modifications sur la web-application :
```bash
npm install
npm run-script serve
```
Ensuite, vous pouvez visualiser vos modifications en temps réel à l'adresse [http://localhost:4200/](http://localhost:4200/).


#### Mode production : visualiser l'application compilée

Visualiser la version compilée de l'application permet de tester de manière plus large qu'en mode développement. En effet, des bugs peuvent être introduits lors de la compilation de l'application, il est donc important de tester l'application localement en mode production avant de la compiler pour son déploiement (cf. partie [Déploiement](../deploiement/#5-compiler-la-web-application-avec-les-parametres-de-deploiement))[^1].
```bash
cd ./app/cairmodweb-app
npm install
npm run-script build
npm start
```
Ensuite, vous pouvez visualiser le site à l'adresse [http://localhost:4200/](http://localhost:4200/).

[^1]: Il est important de noter que le script de compilation n'est pas le même lors d'une compilation locale et d'une compulation pour déploiement sur un serveur.



## Tester l'application

Seules l'API et la web-application peuvent être testées de manière automatique.

### L'API

Pour tester l'API, il suffit de lancer la commande depuis `cairmodweb-api` :
```bash
npm install
npm test
```
Les résultats sont accessibles directement dans le terminal, avec un récapitulatif de la couverture de code.

### La web-application

Pour tester la web-app, il suffit de lancer la commande suivante depuis `cairmodweb-app` :
```bash
npm install
npm run-script test
```
Les résultats sont accessibles directement dans le terminal ou dans un navigateur web à l'adresse [http://localhost:9876/](http://localhost:9876/) si il est disponible.

**NB :** Le navigateur par défaut pour les tests est Chrome, ainsi si vous réalisez les tests depuis un navigateur différent à l'adresse [http://localhost:9876/](http://localhost:9876/), le nombre de test est doublé puisqu'ils sont réalisés à la fois dans Chrome (par défaut) et dans votre navigateur.

Vous pouvez aussi calculer la couverture de code en réalisant les tests avec la commande :
```bash
npm install
npm run-script test-coverage
```
Ensuite, les résultats des tests sont visuables comme expliqué précédemment. Les résultats de la couverture de code sont visibles dans le terminal mais aussi par le biais d'un navigateur web (avec un graphisme esthétique). En effet, le dossier `coverage` a été créé (ou modifié) dans `cairmodweb-app`. Il contient une application HTML pour visualiser les résultats en ouvrant le fichier `index.html` dans un navigateur.