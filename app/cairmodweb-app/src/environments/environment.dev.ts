// This file replaces `environment.ts` during build for dev build.
// `ng build --dev` replaces `environment.ts` with `environment.dev.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseurl: 'http://localhost:4200/',
  urls: {
    api: 'http://localhost:3000/',
    mapserver: 'http://217.160.173.154/mapws/background_map/?',
  },
  internalUrls: {
    root: {
      slash: '/',
      noSlash: ''
    },
    home: {
      slash: '/accueil',
      noSlash: 'accueil'
    },
    visu: {
      slash: '/visualisation',
      noSlash: 'visualisation'
    },
    info: {
      slash: '/information',
      noSlash: 'information'
    },
    comparison: {
      slash: '/comparaison',
      noSlash: 'comparaison'
    },
    all: {
      slash: '/**',
      noSlash: '**'
    }
  },
  gazeteer: {
    thid: 42,
    mapInitialCoordinates: [ 30.04372, 31.24554 ],
    mapInitialZoom: 12,
    minimalDate: 0
  }

};
