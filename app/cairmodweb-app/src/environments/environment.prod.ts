// This file replaces `environment.ts` during build for deployment on a server.
// `npm run-script buil-deploy` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  baseurl: 'http://localhost:4200/',
  urls: {
    api: 'http://217.160.173.154/api/',
    mapserver: 'http://217.160.173.154/mapws/background_map/?',
  },
  internalUrls: {
    root: {
      slash: '/',
      noSlash: ''
    },
    home: {
      slash: '/accueil',
      noSlash: 'accueil'
    },
    visu: {
      slash: '/visualisation',
      noSlash: 'visualisation'
    },
    info: {
      slash: '/information',
      noSlash: 'information'
    },
    comparison: {
      slash: '/comparaison',
      noSlash: 'comparaison'
    },
    all: {
      slash: '/**',
      noSlash: '**'
    }
  },
  gazeteer: {
    thid: 42,
    mapInitialCoordinates: [ 30.04372, 31.24554 ],
    mapInitialZoom: 12,
    minimalDate: 0
  }
};
