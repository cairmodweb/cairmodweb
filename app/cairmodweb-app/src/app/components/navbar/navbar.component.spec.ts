import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let router : Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      imports: [RouterTestingModule]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should detect the url for accueil', (done) => {    
    spyOnProperty(router, 'url', 'get').and.returnValue('/u/accueil');
    expect(component.hasRoute("information")).toBeFalse();
    expect(component.hasRoute("accueil")).toBeTruthy();
    done();     
  });

  it('should detect the url for information', (done) => {    
    spyOnProperty(router, 'url', 'get').and.returnValue('/u/information');
    expect(component.hasRoute("accueil")).toBeFalse();
    expect(component.hasRoute("information")).toBeTruthy();
    done();     
  });

  it('should be disable for accueil et enable for another', (done) => {    
    spyOnProperty(router, 'url', 'get').and.returnValue('/u/accueil');
    component.disableNavBarItems();
    expect(document.getElementById(component.navbar_items['accueil']).classList).toContain("disabled");
    expect(document.getElementById(component.navbar_items['information']).classList.contains("disabled")).toBeFalsy();
    done();      
  });

  it('should be disable for information et enable for another', (done) => {    
    spyOnProperty(router, 'url', 'get').and.returnValue('/u/information');
    component.disableNavBarItems();
    expect(document.getElementById(component.navbar_items['information']).classList).toContain("disabled");
    expect(document.getElementById(component.navbar_items['accueil']).classList.contains("disabled")).toBeFalsy();
    done();      
  });
});
