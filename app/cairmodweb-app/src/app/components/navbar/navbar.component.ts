import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'cairmodweb-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public navbar_items = {"visualisation":"map-nav-item", 
                          "information" : "info-nav-item", 
                          "accueil" : "home-nav-item",
                          "comparaison": "comp-nav-item"}

  constructor(private router: Router,) { 

    

  }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      this.disableNavBarItems();
  });
    
  }
  /**
   * allow to make disable a tab
   */
  public disableNavBarItems(){
    for (var key in this.navbar_items){
      let elem = document.getElementById(this.navbar_items[key]);
      if (this.hasRoute(key)){
        elem.classList.add("disabled");
      }
      else {
        elem.classList.remove("disabled");

      }
      
    }
  }
  /**
   * allow to know if a tab exist
   * @param route {string}
   */
  public hasRoute(route: string) {
    return this.router.url.includes(route);
  }

}
