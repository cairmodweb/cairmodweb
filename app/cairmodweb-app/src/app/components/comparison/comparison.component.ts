import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import './leaflet-side-by-side.js';
import { Router } from '@angular/router';
import { MapService } from 'src/app/services/map/map.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'cairmodweb-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.css']
})
export class ComparisonComponent implements OnInit {
  public pickMapsWindowOpened = true;
  public map;
  public controlSideBySide;
  public OSMurl = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
  public mapServerURL = environment.urls.mapserver;
  public leftLayerName;
  public rightLayerName;
  public overlapsedMaps = true; // Are the zone overlaping ?

  // Size of the buffer multiplier for zoom limit
  private bufferMultiplier = 2;

  //Definition of the length of the minimum overlapse zone
  private zoneminlat = 0.001;
  private zoneminlng = 0.001;



  constructor(private router: Router, public mapService: MapService) { }

  ngOnInit(): void {
    this.map = L.map('map-comparison').setView([30.0, 31.245587], 13);
    L.tileLayer(this.OSMurl, {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

    this.mapService.addScaleBar(this.map);
    this.mapService.addMousePosition(this.map);
    this.initialiseMapsDialog();
  }

  /**
   * Init MapsDialog Box
   */
  initialiseMapsDialog() {
    var customControl = L.Control.extend({
      options: {
        position: 'topright'
      },

      onAdd: (map) => {
        var container = L.DomUtil.create('input', 'leaflet-bar leaflet-control leaflet-control-custom');
        container.type = "button";

        container.style.backgroundColor = 'white';     
        container.style.backgroundImage = "url(assets/changeMap.png)";
        container.style.backgroundSize = "45px 45px";
        container.style.width = '50px';
        container.style.height = '50px';

        container.onclick = () => {
          this.open();
        }

        return container;
      }
    });
    this.map.addControl(new customControl());
  }


  /**
   * Close map window
   * @param status 
   */
  close(status) {
    if (status == "cancel") {//If the user cancels the modifications
      this.pickMapsWindowOpened = false; // Close window;
    }
    else {
      this.resetMap();

      if (status == "no") {// If the user wants to return to home
        this.router.navigate(['/accueil'])
      }
      else {// If the user wants new comparison
        this.compare();
      }
    }
  }

  /**
   * Compares two maps;
   */
  compare() {
    this.pickMapsWindowOpened = false; // Close window;
    this.setMaxBounds();

    var leftlayer = L.tileLayer.wms(this.mapServerURL, {
      layers: this.leftLayerName,
      crs: L.CRS.EPSG4326,
      format: 'image/png',
      transparent: true,
      version: '1.1.0',
    }
    ).addTo(this.map);


    var rightlayer = L.tileLayer.wms(this.mapServerURL, {
      layers: this.rightLayerName,
      crs: L.CRS.EPSG4326,
      format: 'image/png',
      transparent: true,
      version: '1.1.0',
    }
    ).addTo(this.map);

    this.controlSideBySide = L.control.sideBySide(leftlayer, rightlayer).addTo(this.map);

  }

  /**
   * Opening map window
   */
  public open() {
    this.pickMapsWindowOpened = true;
  }

  /**
   * Update layerName when checked
   * @param side 
   * @param layerName 
   */
  changeMap(side, layerName) {
    if (side == 'left') {
      if (layerName == this.leftLayerName) {
        this.overlapsedMaps = true;
        this.leftLayerName = null;
      }
      else {
        this.leftLayerName = layerName;
        if (this.rightLayerName) {
          this.checksBbox();
        }
      }
    }
    else {
      if (layerName == this.rightLayerName) {
        this.overlapsedMaps = true;
        this.rightLayerName = null;
      }
      else {
        this.rightLayerName = layerName;
        if (this.leftLayerName) {
          this.checksBbox();
        }
      }
    }
  }

  /**
   * Resets map to init stage
   */
  resetMap() {
    if (this.controlSideBySide) {
      this.controlSideBySide.remove();
    }
    this.map.eachLayer((layer) => {
      if (layer._url != this.OSMurl) {
        layer.remove();
      }
    });
  }

  /**
   * Checks both maps are covering similar zones.
   */
  checksBbox() {
    let leftlatMax = this.mapService.backgroundInfo[this.leftLayerName].northEast.lat - this.zoneminlat;
    let leftlngMax = this.mapService.backgroundInfo[this.leftLayerName].northEast.lng - this.zoneminlng;
    let leftlatMin = this.mapService.backgroundInfo[this.leftLayerName].southWest.lat + this.zoneminlat;
    let leftlngMin = this.mapService.backgroundInfo[this.leftLayerName].southWest.lng + this.zoneminlng;

    let rightlatMax = this.mapService.backgroundInfo[this.rightLayerName].northEast.lat - this.zoneminlat;
    let rightlngMax = this.mapService.backgroundInfo[this.rightLayerName].northEast.lng - this.zoneminlng;
    let rightlatMin = this.mapService.backgroundInfo[this.rightLayerName].southWest.lat + this.zoneminlat;
    let rightlngMin = this.mapService.backgroundInfo[this.rightLayerName].southWest.lng + this.zoneminlng;

    //Check if the zone are overlaping
    let isSuperposed = (this.between(rightlatMax, rightlatMin, leftlatMax) && (this.between(leftlngMax, leftlngMin, rightlngMax) || this.between(leftlngMax, leftlngMin, rightlngMin) ||
      this.between(rightlngMax, rightlngMin, leftlngMax) || this.between(rightlngMax, rightlngMin, leftlngMin))) ||
      (this.between(rightlatMax, rightlatMin, leftlatMin) && (this.between(leftlngMax, leftlngMin, rightlngMax) || this.between(leftlngMax, leftlngMin, rightlngMin) ||
        this.between(rightlngMax, rightlngMin, leftlngMax) || this.between(rightlngMax, rightlngMin, leftlngMin))) ||
      (this.between(leftlatMax, leftlatMin, rightlatMax) && (this.between(leftlngMax, leftlngMin, rightlngMax) || this.between(leftlngMax, leftlngMin, rightlngMin) ||
        this.between(rightlngMax, rightlngMin, leftlngMax) || this.between(rightlngMax, rightlngMin, leftlngMin))) ||
      (this.between(leftlatMax, leftlatMin, rightlatMin) && (this.between(leftlngMax, leftlngMin, rightlngMax) || this.between(leftlngMax, leftlngMin, rightlngMin) ||
        this.between(rightlngMax, rightlngMin, leftlngMax) || this.between(rightlngMax, rightlngMin, leftlngMin)));


    if (isSuperposed) // If there is overlap
    {
      this.overlapsedMaps = true;
    }
    else {
      this.overlapsedMaps = false;
    }
  }

  /**
   * Sets maximum map Bounds
   */
  setMaxBounds() {
    let leftlatMax = this.mapService.backgroundInfo[this.leftLayerName].northEast.lat;
    let leftlngMax = this.mapService.backgroundInfo[this.leftLayerName].northEast.lng;
    let leftlatMin = this.mapService.backgroundInfo[this.leftLayerName].southWest.lat;
    let leftlngMin = this.mapService.backgroundInfo[this.leftLayerName].southWest.lng;

    let rightlatMax = this.mapService.backgroundInfo[this.rightLayerName].northEast.lat;
    let rightlngMax = this.mapService.backgroundInfo[this.rightLayerName].northEast.lng;
    let rightlatMin = this.mapService.backgroundInfo[this.rightLayerName].southWest.lat;
    let rightlngMin = this.mapService.backgroundInfo[this.rightLayerName].southWest.lng;

    // Get min + max values
    let latMax = this.max(rightlatMax, leftlatMax);
    let latMin = this.min(rightlatMin, leftlatMin);
    let lngMin = this.min(rightlngMin, leftlngMin);
    let lngMax = this.max(rightlngMax, leftlngMax);


    let trueSouthWest = L.latLng(lngMin, latMin),
      trueNorthEast = L.latLng(lngMax, latMax),
      trueBounds = L.latLngBounds(trueSouthWest, trueNorthEast);

    //Compute buffer
    let bufferlat = (latMax - latMin) * this.bufferMultiplier;
    let bufferlng = (lngMax - lngMin) * this.bufferMultiplier;

    let southWest = L.latLng(lngMin - bufferlng, latMin - bufferlat),
      northEast = L.latLng(lngMax + bufferlng, latMax + bufferlat),
      maxbounds = L.latLngBounds(southWest, northEast);

    this.map.fitBounds(trueBounds);
    this.map.setMaxBounds(maxbounds);

  }

  /**
   * Returns if c is within the range [b;a]
   * @param a, max value of the range
   * @param b, min value of the range
   * @param c, value to test
   */
  between(a, b, c) {
    return (a >= c && c >= b)

  }


  /**
   * Returns the maximum of two values
   * @param a 
   * @param b 
   */
  max(a, b) {
    return a >= b ? a : b;
  }

  /**
   * Returns the minimum of two values
   * @param a 
   * @param b 
   */
  min(a, b) {
    return a >= b ? b : a;
  }

}
