import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonComponent } from './comparison.component';
import { RouterTestingModule } from '@angular/router/testing';

import * as L from 'leaflet';

describe('ComparisonComponent', () => {
  let component: ComparisonComponent;
  let fixture: ComponentFixture<ComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComparisonComponent],
      imports: [RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  function countMapLayers() {
    let i = 0;
    component.map.eachLayer(() => i++)
    return i;
  }

  function initialiseLeftRightLayers(firstCoordSW = [30, 31], firstCoordNE = [31, 32], secondCoordSW = [29, 31], secondCoordNE = [32, 34]) {
    component.leftLayerName = "firstLayer";
    component.rightLayerName = 'secondLayer';
    component.mapService.backgroundInfo[component.leftLayerName] = {
      "groupname": "test",
      "southWest": L.latLng(firstCoordSW),
      "northEast": L.latLng(firstCoordNE)
    };
    component.mapService.backgroundInfo[component.rightLayerName] = {
      "groupname": "test",
      "southWest": L.latLng(secondCoordSW),
      "northEast": L.latLng(secondCoordNE)
    };
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the pickMapsWindowOpened boolean', () => {
    component.pickMapsWindowOpened = false;
    component.open();
    expect(component.pickMapsWindowOpened).toBe(true);
  });

  it('should set the min', () => {
    expect(component.min(1, 3)).toEqual(1);
    expect(component.min(4, 3)).toEqual(3);
    expect(component.min(2, 2)).toEqual(2);
  });

  it('should set the max', () => {
    expect(component.max(1, 3)).toEqual(3);
    expect(component.max(81, -50)).toEqual(81);
    expect(component.max(0, 0)).toEqual(0);
  });

  it('should test value in between values', () => {
    expect(component.between(5, 1, 3)).toBeTruthy();
    expect(component.between(1, 5, 3)).toBeFalsy();
    expect(component.between(6, 3, 1)).toBeFalsy();
    expect(component.between(3, 6, 2)).toBeFalsy();
    expect(component.between(6, 3, 3)).toBeTruthy();
    expect(component.between(6, 3, 3)).toBeTruthy();
    expect(component.between(3, 6, 6)).toBeFalsy();
  });

  it('should close the window', () => {
    component.close('cancel')
    expect(component.pickMapsWindowOpened).toBeFalsy();
  });

  it('should open the window', () => {
    component.open()
    expect(component.pickMapsWindowOpened).toBeTruthy();
  });


  it('should launch comparison of the two maps', () => {
    let initialNumberOfLayers = countMapLayers();

    initialiseLeftRightLayers();
    component.pickMapsWindowOpened = true;

    component.compare()
    // Test pickMapsWindowOpened value
    expect(component.pickMapsWindowOpened).toBeFalsy();
    // Test if controlSideBySide added
    expect(component.controlSideBySide.getPosition()).toBeDefined();
    // Test if both layers has been added
    expect(countMapLayers()).toEqual(initialNumberOfLayers + 2);
  });


  it('should change the map', () => {
    initialiseLeftRightLayers();

    component.overlapsedMaps = false

    // First if true, second if true
    component.changeMap('left', 'firstLayer');
    expect(component.overlapsedMaps).toBeTruthy();
    expect(component.leftLayerName).toBeNull();

    // First if true, second it false
    component.changeMap('left', 'secondLayer');
    expect(component.leftLayerName).toEqual('secondLayer');

    // First if false, second if true
    component.changeMap('right', 'secondLayer');
    expect(component.overlapsedMaps).toBeTruthy();
    expect(component.rightLayerName).toBeNull();

    // First if false, second if false
    component.changeMap('right', 'firstLayer');
    expect(component.rightLayerName).toEqual('firstLayer');
  })

  it('should reset the map', () => {
    initialiseLeftRightLayers();
    var leftlayer = L.tileLayer.wms(component.mapServerURL, {
      layers: component.leftLayerName,
      crs: L.CRS.EPSG4326,
      format: 'image/png',
      transparent: true,
      version: '1.1.0',
    }
    ).addTo(component.map);

    var rightlayer = L.tileLayer.wms(component.mapServerURL, {
      layers: component.rightLayerName,
      crs: L.CRS.EPSG4326,
      format: 'image/png',
      transparent: true,
      version: '1.1.0',
    }
    ).addTo(component.map);

    component.controlSideBySide = L.control.sideBySide(leftlayer, rightlayer).addTo(component.map);

    component.resetMap();
    // // Test if controlSideBySide removed
    // expect(component.map.sideBySide).toBeUndefined();
    // Test if layers has been removed (except OSM)
    expect(countMapLayers()).toEqual(1);
  })

  it('should verify if both layers bbox are superposed', () => {
    
    /* Test: layers are superposed */
    // When left include right
    initialiseLeftRightLayers([29,29], [32,32], [29.5,30], [31,31.5]);
    component.overlapsedMaps = false;
    component.checksBbox();
    expect(component.overlapsedMaps).toBeTruthy();

    // When right include left
    initialiseLeftRightLayers([29.5,30], [31,31.5], [29,29], [32,32]);
    component.overlapsedMaps = false;
    component.checksBbox();
    expect(component.overlapsedMaps).toBeTruthy();

    // When left and right have a same zone in their area
    initialiseLeftRightLayers([29,29], [32,32], [29.5,30.5], [33.5,31.5]);
    component.overlapsedMaps = false;
    component.checksBbox();
    expect(component.overlapsedMaps).toBeTruthy();

    /* Test: layers are NOT superposed */
    initialiseLeftRightLayers([29,29], [32,32], [33,33], [33.5,33.5]);
    component.overlapsedMaps = true;
    component.checksBbox();
    expect(component.overlapsedMaps).toBeFalsy();

  })

  it('should set maxBounds of the map', () => {
    spyOn(component.map, "fitBounds");
    spyOn(component.map, "setMaxBounds");

    // first test
    initialiseLeftRightLayers([29,28], [32,33], [29.5,34], [31,35]);
    fixture.detectChanges();

    // latMax = 32
    // latMin = 29
    // lngMin = 28
    // lngMax = 35

    var trueSW = L.latLng(28, 29);
    var trueNE = L.latLng(35, 32);
    var trueBounds = L.latLngBounds(trueSW, trueNE);

    var bufferlat = (32 - 29) * 2;
    var bufferlng = (35 - 28) * 2;
    var southWest = L.latLng(28 - bufferlng, 29 - bufferlat);
    var northEast = L.latLng(35 + bufferlng, 32 + bufferlat);
    var maxbounds = L.latLngBounds(southWest, northEast);

    component.setMaxBounds();
    expect(component.map.fitBounds).toHaveBeenCalledWith(trueBounds);
    expect(component.map.setMaxBounds).toHaveBeenCalledWith(maxbounds);


    // second test
    initialiseLeftRightLayers([30,29], [33,34], [31,28], [34,35]);
    fixture.detectChanges();

    // latMax = 34
    // latMin = 30
    // lngMin = 28
    // lngMax = 35

    trueSW = L.latLng(28, 30);
    trueNE = L.latLng(35, 34);
    trueBounds = L.latLngBounds(trueSW, trueNE);

    bufferlat = (34 - 30) * 2;
    bufferlng = (35 - 28) * 2;
    southWest = L.latLng(28 - bufferlng, 30 - bufferlat);
    northEast = L.latLng(35 + bufferlng, 34 + bufferlat);
    maxbounds = L.latLngBounds(southWest, northEast);

    component.setMaxBounds();
    expect(component.map.fitBounds).toHaveBeenCalledWith(trueBounds);
    expect(component.map.setMaxBounds).toHaveBeenCalledWith(maxbounds);

  })

  


});
