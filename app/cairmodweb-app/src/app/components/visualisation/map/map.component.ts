import { Component, AfterViewInit, OnDestroy, AfterContentChecked } from '@angular/core';

import "./mousePosition/mousePosition";
import { MapService } from "src/app/services/map/map.service";
import { ExportService } from 'src/app/services/export/export.service';
import { QueryService } from 'src/app/services/query/query.service';


@Component({
  selector: 'cairmodweb-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit, AfterContentChecked, OnDestroy {

  constructor(
    public mapService: MapService,
    private queryService: QueryService,
    public exportService: ExportService
  ) { }

  ngAfterViewInit(): void {
    // Display all datas only if it is the first time on the page
    if (this.mapService.queryLayers.length == 0) {
      this.queryService.getAllDataFromGazetier();
    }

    // Map initialisaton
    this.mapService.initMap();


    // Init for export
    this.exportService.initialiseDownloadButton();
    this.exportService.initialiseNorth(); //Affichage Nord
    this.mapService.map._controlCorners.bottomright.firstChild.hidden = true;//Affichage Nord
  }

  ngAfterContentChecked(): void {
    this.mapService.getQueryLayers();
  }

  ngOnDestroy(): void {
    this.mapService.resetQueryLayers();
  }



}
