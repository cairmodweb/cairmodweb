import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapComponent } from './map.component';
import * as L from 'leaflet';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add layer',async (done)=>{
    await component.mapService.requestLayer(component.mapService.requestURL);
    expect(component.mapService.collectionLoaded).toBe(true);
    let layerName1 = component.mapService.backgroundNames[0];
    let layerName2 = component.mapService.backgroundNames[1];
    component.mapService.addBackgroundToMap(layerName1);
    let currentBackgrounds = [];

    component.mapService.map.eachLayer((layer) => {
      if (layer instanceof L.TileLayer){
        currentBackgrounds.push(layer.name);        
      }
    });
    expect(currentBackgrounds.includes(layerName1)).toBe(true);
    expect(currentBackgrounds.includes(layerName2)).toBe(false);
    done();
    })

    it('should init rectangle selection',async(done)=>{
      var bounds = [[30.767202, -31], [37.766560, -30.3]];     
      var rectangle = L.rectangle(bounds);
      var allBounds = [];
      component.mapService.drawnItems = rectangle;
      component.mapService.initRectangleSelectionLayer();
      component.mapService.map.eachLayer((layer) => {
        if(layer._bounds){
          if(layer._bounds._southWest){
            let sw = layer._bounds._southWest;
            let ne = layer._bounds._northEast;
            allBounds.push([sw.lat,sw.lng,ne.lat,ne.lng])
          }
        }  
      });
      expect(allBounds).toEqual([[30.767202, -31,37.766560, -30.3]]);
      done();
    }) 
    
    it('should remove rectangle selection',async(done)=>{
      var bounds = [[30.767202, -31], [37.766560, -30.3]];     
      var rectangle = L.rectangle(bounds);
      component.mapService.drawnItems = rectangle;
      component.mapService.initRectangleSelectionLayer();
      let isPresentBeforeBeingRemoved = false;
      let isPresent = false;

      component.mapService.map.eachLayer((layer) => {
        if(layer._bounds){
          if(layer._bounds._southWest){
            let sw = layer._bounds._southWest;
            let ne = layer._bounds._northEast;
            if(sw.lat == 30.767202 && sw.lng ==  -31 && ne.lat == 37.766560 &&  ne.lng == -30.3){
              isPresentBeforeBeingRemoved = true;
            }
          }
        }  
      });
      expect(isPresentBeforeBeingRemoved).toBe(true);

      component.mapService.removeRectangleSelection();
      setTimeout(function() {
        component.mapService.map.eachLayer((layer) => {
          if(layer._bounds){
            if(layer._bounds._southWest){
              let sw = layer._bounds._southWest;
              let ne = layer._bounds._northEast;
              if([sw.lat,sw.lng,ne.lat,ne.lng] == [30.767202, -31,37.766560, -30.3]){
                isPresent = true;
              }
            }
          }
          });
        expect(isPresent).toBe(false);
        done();
    }, 1000);

    })

    it('should oder layers',()=>{
      var bounds1 = [[30.4,30.5], [37.766560, 30.8]];     
      var rectangle1 = L.rectangle(bounds1);
      var bounds2 = [[30.4,30.5], [37.766560, 30.8]];     
      var rectangle2 = L.rectangle(bounds2);
      var bounds3 = [[30.4,30.5], [37.8, 35.8]];     
      var rectangle3 = L.rectangle(bounds3);
      component.mapService.queryLayers = [rectangle1,rectangle2,rectangle3];
      component.mapService.orderQueryLayersInMap();
      expect(component.mapService.queryLayers[0].zOrder).toEqual(0);
      expect(component.mapService.queryLayers[1].zOrder).toEqual(1);
      expect(component.mapService.queryLayers[2].zOrder).toEqual(2);
      component.mapService.queryLayers = [rectangle1,rectangle3,rectangle2];
      expect(component.mapService.queryLayers[0].zOrder).toEqual(0);
      expect(component.mapService.queryLayers[1].zOrder).toEqual(2);
      expect(component.mapService.queryLayers[2].zOrder).toEqual(1);


    })

    it('should initialise north arrow',()=>{
      var imageNorthArrow = fixture.debugElement.nativeElement.querySelector('.leaflet-bottom.leaflet-right').querySelector('img');
      
      expect(imageNorthArrow).not.toEqual(null);
      expect(imageNorthArrow.style.height).toBe("40px");
      expect(imageNorthArrow.style.width).toBe("40px");
      expect(imageNorthArrow.src).toContain("assets/northArrow.jpg");
    })
	
    it('should initialise download button',()=>{
      var downloadButton = fixture.debugElement.nativeElement.querySelector('.leaflet-top.leaflet-left').querySelector('input');
      
      expect(downloadButton).not.toEqual(null);
      expect(downloadButton.type).toBe("button");
      expect(downloadButton.style.height).toBe("30px");
      expect(downloadButton.style.width).toBe("30px");
      expect(downloadButton.style.backgroundSize).toBe("27px 27px");
      expect(downloadButton.style.backgroundColor).toBe("white");
      expect(downloadButton.style.backgroundImage).toContain("assets/exportLogo.jpg");
      
      spyOn(component.exportService,'open');
      
      downloadButton.click();
      
      expect(component.exportService.open).toHaveBeenCalled();
    })

    it('should add the scale bar control to Leaflet Map', ()=>{
      var scaleControl = fixture.debugElement.nativeElement.querySelector('.leaflet-control-scale.leaflet-control');
      expect(scaleControl).not.toEqual(null);
    })

    it('should add the mouse position control to the Leaflet Map', ()=>{
      var mousePositionControl = fixture.debugElement.nativeElement.querySelector('.leaflet-control-mouseposition.leaflet-control');
      expect(mousePositionControl).not.toEqual(null);
    })


    it('should initialise OSM background', ()=>{
      var leafletTileContainer = fixture.debugElement.nativeElement.querySelector('.leaflet-tile-container');
      expect(leafletTileContainer).not.toBe(null);
      expect(leafletTileContainer.querySelector('img')).not.toBe(null);
    })

    it('should add json layer', () => {
      let jsonobj = {
        "bbox": [
          31.23625,
          30.03211,
          31.26015,
          30.08097
        ],
        "features": [
          {
            "geometry": {
              "coordinates": [
                31.2423,
                30.05122
              ],
              "type": "Point"
            },
            "properties": {
              "label_fr": "rue el Maghraby",
              "note_fr": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/1135.4470989761103,486.25796592113096,2484.921501706485,1644.6962457337881/2485,1645/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/4129.478260869566,861.9049632605021,1331.3979933110368,893.438127090301/1332,894/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/2272.528428093646,3542.219344531405,1366.434782608696,875.9197324414708/1367,876/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/4199.551839464884,2964.112321120034,2487.6120401337785,1839.4314381270897/2488,1840/0/native.jpg",
              "perseeid": "1000"
            },
            "type": "Feature"
          }
        ],
        "type": "FeatureCollection"
      }
      component.mapService.addLayer(jsonobj, "test");
      let currentLayers = [];
  
      component.mapService.map.eachLayer((layer) => {
        currentLayers.push(layer.name);
      })
      expect(currentLayers.includes("test")).toBe(true);
    })
  
    it('should remove json layer', () => {
      let jsonobj = {
        "bbox": [
          31.23625,
          30.03211,
          31.26015,
          30.08097
        ],
        "features": [
          {
            "geometry": {
              "coordinates": [
                31.2423,
                30.05122
              ],
              "type": "Point"
            },
            "properties": {
              "label_fr": "rue el Maghraby",
              "note_fr": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/1135.4470989761103,486.25796592113096,2484.921501706485,1644.6962457337881/2485,1645/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/4129.478260869566,861.9049632605021,1331.3979933110368,893.438127090301/1332,894/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/2272.528428093646,3542.219344531405,1366.434782608696,875.9197324414708/1367,876/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f53/4199.551839464884,2964.112321120034,2487.6120401337785,1839.4314381270897/2488,1840/0/native.jpg",
              "perseeid": "1000"
            },
            "type": "Feature"
          }
        ],
        "type": "FeatureCollection"
      }
      component.mapService.addLayer(jsonobj, "test");
      let currentLayers = [];
  
      component.mapService.map.eachLayer((layer) => {
        console.log(layer.name);
        if (layer.name == 'test') {
          component.mapService.removeLayer(layer);
        }
      })
      component.mapService.map.eachLayer((layer) => {
        currentLayers.push(layer.name);
      })
      expect(currentLayers.includes("test")).toBe(false);
    })
    
    

});
