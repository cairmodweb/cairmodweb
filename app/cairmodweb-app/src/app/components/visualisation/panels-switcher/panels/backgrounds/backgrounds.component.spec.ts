import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackgroundsComponent } from './backgrounds.component';

describe('BackgroundComponent', () => {
  let component: BackgroundsComponent;
  let fixture: ComponentFixture<BackgroundsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackgroundsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should click collection',()=>{
    component.faIconRotationCollapsed[1] = 0;
    let event = {target: {id:1}};
    component.clickCollection(event);
    expect(component.faIconRotationCollapsed[1]).toEqual(180);
    component.clickCollection(event);
    expect(component.faIconRotationCollapsed[1]).toEqual(0);
  })

  it ('should verify visibility',()=> {
    let collection = {name: "namegroup",allVisible: false};
    (component.mapService as any).backgroundList = [{
      name: "layerName",
      collection: "namegroup",
      visible: true,
      opacity: 100
    }];
    component.verifyVisibilityCollection(collection);
    expect(collection.allVisible).toBeTrue();
    (component.mapService as any).backgroundList = [{
      name: "layerName",
      collection: "namegroup",
      visible: false,
      opacity: 100
    },
    {
      name: "layerName2",
      collection: "namegroup",
      visible: true,
      opacity: 100
    }];
    component.verifyVisibilityCollection(collection);
    expect(collection.allVisible).toBeFalse();
  });
  
  

});
