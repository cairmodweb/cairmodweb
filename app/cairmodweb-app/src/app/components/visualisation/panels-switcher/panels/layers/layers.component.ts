import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MapService } from 'src/app/services/map/map.service';
import { ExportService } from 'src/app/services/export/export.service';
import { faEllipsisH, faSquare } from '@fortawesome/free-solid-svg-icons'


declare var $: any;

@Component({
  selector: 'cairmodweb-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.css', '../../panels-switcher.component.css']
})
export class LayersComponent implements AfterViewInit {

  public menuIcon = faEllipsisH;
  public legendIcon = faSquare;

  constructor(
    public mapService: MapService,
    public exportService: ExportService
  ) { }


  ngAfterViewInit(): void {

    /**
     * Gestion du Drag'n'Drop des couches.
     */
    let self = this;
    $(function () {
      $("#layer-list").sortable({
        revert: true,
        scroll: true,
        axis: 'y',

        start: function (event, ui) {
          ui.item.data('start_pos', ui.item.index());
        },
        stop: function (event, ui) {
          // Get the start poisition
          var start_pos = ui.item.data('start_pos');

          // If the item position has really changed 
          if (start_pos != ui.item.index()) {
            // Get the item
            let item = self.mapService.queryLayers[start_pos];
            console.log('item ' + start_pos + ' got moved to ' + ui.item.index());

            // Update the queryLayers array with the new position of the item
            self.mapService.queryLayers.splice(start_pos, 1);
            self.mapService.queryLayers.splice(ui.item.index(), 0, item);

            // Update the zOrder of the layers in the map
            self.mapService.orderQueryLayersInMap();

          } 
          // Else the item is back to its original position
          else {
            console.log('item ' + start_pos + ' returned to the same position')
          }


        },
        update: function (event, ui) {
          $.post('/reorder', $("#layer-list").sortable('serialize'))
            .done(function () {
              alert('Updated')
            });
        }
      });

    });
  }

  /**
   * Modifies the visibility of the layer according to the checkbox.
   * @param event 
   * @param layer selected layer
   */
  onChange(event, layer) {
    if (layer.visible == true) {
      layer.visible = false;
      // Get the layer objects (considered as sublayers by Leaflet)
      var keys = Object.keys(layer._layers)
      // Update with an invisible style
      keys.forEach(function (key) {
        layer._layers[key].setStyle({
          fillColor: "#ffffff",
          color: "#ffffff",
          opacity: 0,
          fillOpacity: 0,
		  interactive: false
        });
		// Delete related popup
		layer._layers[key]._popup._source.unbindPopup();
		
      })
    } else {
      layer.visible = true;
      // Get the layer objects (considered as sublayers by Leaflet)
      var keys = Object.keys(layer._layers)

	    for(var i=0; i<keys.length; i++){
		  // Reset feature's original style
		  layer._layers[keys[i]].setStyle(layer.style);
		 
		  // Reset feature's original popup 
		  var popup = this.mapService.createPopup(layer._layers[keys[i]].feature);
		  layer._layers[keys[i]].bindPopup(popup);
	  } 
    }
  }




}
