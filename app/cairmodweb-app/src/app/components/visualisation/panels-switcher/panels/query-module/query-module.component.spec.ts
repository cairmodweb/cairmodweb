import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from "@angular/forms";
import { QueryModuleComponent } from './query-module.component';
import * as L from 'leaflet';

describe('QueryModuleComponent', () => {
  let component: QueryModuleComponent;
  let fixture: ComponentFixture<QueryModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryModuleComponent ],
      providers: [
        FormBuilder
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  function updateKeyword(keyword: string){
    component.requestForm.controls['keyword'].setValue(keyword);
  }

  function updateSelectionType(selectiontype: string){
    component.requestForm.controls['date_selection_type'].setValue(selectiontype);
  }

  function updateStartDate(startDate: number){
    component.requestForm.controls['start_date'].setValue(startDate);
  }

  function updateStopDate(stopDate: number){
    component.requestForm.controls['stop_date'].setValue(stopDate);
  }

  it('should return if date is valid',()=>{
    //Test between two dates
    updateSelectionType("between");
    expect(component.isDateValid()).toBe(false);
    updateStartDate(1760);updateStopDate(1780);
    expect(component.isDateValid()).toBe(true);
    updateStartDate(1799);
    expect(component.isDateValid()).toBe(false);

    component.resetDate();
    updateSelectionType("after");
    expect(component.isDateValid()).toBe(false);
    updateStartDate(-2);
    expect(component.isDateValid()).toBe(false);
    updateStartDate(1960);
    expect(component.isDateValid()).toBe(true);

    component.resetDate();
    updateSelectionType("before");
    expect(component.isDateValid()).toBe(false);
    updateStopDate(1980);
    expect(component.isDateValid()).toBe(true);
  })


  it('should return if keyword is empty',() => {
    updateKeyword('');
    expect(component.isKeywordEmpty()).toBe(true);
    updateKeyword('pouet');
    expect(component.isKeywordEmpty()).toBe(false);
  })

  it('should return if keyword is valid',() => {
    // Regex not respected
    updateKeyword('');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('123ajfne');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('afgnjkz123');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('/vfv');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('"');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('_');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword("'");
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('(');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword(')');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('?');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('!');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('$');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('%');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword(':');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword(';');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword(',');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('.');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('|');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('"');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('[');
    expect(component.isKeywordValid()).toBe(false);
    updateKeyword('÷');
    expect(component.isKeywordValid()).toBe(false);
    // Regex respected
    updateKeyword('pouet');
    expect(component.isKeywordValid()).toBe(true);
    updateKeyword('ageBopvj');
    expect(component.isKeywordValid()).toBe(true);
    updateKeyword('VZHFBIVGJRZ');
    expect(component.isKeywordValid()).toBe(true);
    updateKeyword('mots-cles');
    expect(component.isKeywordValid()).toBe(true);
    updateKeyword('äâàáéèêëîïìíûùüúÿçñôöœ');
    expect(component.isKeywordValid()).toBe(true);
  })

  it('should enable/disable btnAddKeywordDisable and errorInvalidKeyword',() => {
    updateKeyword('');
    component.inputKeyword();
    expect(component.isValidKeyword).toBe(false);
    expect(component.btnAddKeywordDisable).toBe(true);
    expect(component.errorInvalidKeyword).toBe(false);

    updateKeyword('123');
    component.inputKeyword();
    expect(component.isValidKeyword).toBe(false);
    expect(component.btnAddKeywordDisable).toBe(true);
    expect(component.errorInvalidKeyword).toBe(true);

    updateKeyword('pouet');
    component.inputKeyword();
    expect(component.isValidKeyword).toBe(true);
    expect(component.btnAddKeywordDisable).toBe(false);
    expect(component.errorInvalidKeyword).toBe(false);
  })


  it('should verify if keyword has been added',() => {
    updateKeyword('pouet');
    component.addKeyword();
    expect(component.keywords).toEqual(['pouet']);
    expect(component.btnAddKeywordDisable).toBe(true);
    expect(component.requestForm.get('keyword').value).toEqual('');


    updateKeyword('booh');
    component.addKeyword();
    expect(component.keywords).toEqual(['pouet', 'booh']);
    expect(component.btnAddKeywordDisable).toBe(true);
    expect(component.requestForm.get('keyword').value).toEqual('');
  })

  it ('should delete correct keyword',()=> {
    component.keywords = ["test","test2","palais","maison"];
    component.deleteKeyword(2);
    expect(component.keywords).toEqual(["test","test2","maison"]);
    expect(component.btnSubmitDisable).toEqual(false);
    component.keywords = ["azerty"];
    component.deleteKeyword(0);
    expect(component.keywords).toEqual([]);
    expect(component.btnSubmitDisable).toEqual(true);
    
  })

  it('should remove rectangle selection', async ()=> {
    var bounds = [[37.767202, -122.456709], [37.766560, -122.455316]];     
    var rectangle = L.rectangle(bounds)
    component.mapService.drawnItems.addLayer(rectangle);
    await component.removeRectangleSelection();
    expect(component.mapService.drawnItems.getLayers().filter( function(l) {return l instanceof L.Rectangle} ).length).toEqual(0);
    expect(component.btnSubmitDisable).toEqual(true);
    expect(component.bbox).toEqual([]);
  })

  it('should reset Date',() => {
    updateStartDate(1760);updateStopDate(1780);
    component.resetDate();
    expect(component.requestForm.get('start_date').value).toEqual(null);
    expect(component.requestForm.get('stop_date').value).toEqual(null);
    component.resetDate();
    expect(component.requestForm.get('start_date').value).toEqual(null);
    expect(component.requestForm.get('stop_date').value).toEqual(null);
  })

  it('should determine if the form is valid',() => {
    //Only Keyword
    updateKeyword('');
    component.keywords = ["test","test2","palais","maison"];
    component.resetDate();
    expect(component.isFormValid()).toEqual(true);
    //Only keyword + Started to type another (not validated)
    updateKeyword('jj');
    expect(component.isFormValid()).toEqual(false);
    // With keyword + geoselection
    updateKeyword('');
    component.bbox = [45,45,45,45];
    expect(component.isFormValid()).toEqual(true);
    // With keyword + geoselection + date
    updateStartDate(1760);updateStopDate(1780);
    updateSelectionType("between");
    expect(component.isFormValid()).toEqual(true);
    // With keyword + geoselection + date + typing another (not validated))
    updateKeyword('jj');
    expect(component.isFormValid()).toEqual(false);
    //With only date
    updateKeyword('');
    component.bbox = [];
    component.keywords = [];
    expect(component.isFormValid()).toEqual(true);
    //With only geoselection
    component.resetDate();
    component.bbox = [45,45,45,45];
    expect(component.isFormValid()).toEqual(true);

  })

  it('should run query',async (done)=>{
    component.keywords = ['palais'];
    component.bbox = [5,6,7,8];
    expect(component.keywords).toEqual(['palais']);

    const promise1 = new Promise((resolve) => {
      setTimeout(function() {
        resolve({features : [{
          id:'2'

        }]});
      }, 300);
    });
    const promise2 = new Promise((resolve) => {
      setTimeout(function() {
        resolve({features : []});
      }, 300);
    });

    let spyOnRequestResponse = spyOn(component.queryService,'getRequestResponse');
    spyOn(component.mapService,'removeRectangleSelection');
    spyOn(component,'resetDate');
    //Objects
    spyOnRequestResponse.and.returnValue(promise1);
    
    component.makeQuery();
  
    setTimeout(()=>{
      expect(component.keywords).toEqual([]);
      expect(component.bbox).toEqual([]);
      expect(component.errorRequest).toBe('');
      expect(component.mapService.removeRectangleSelection).toHaveBeenCalled();
      expect(component.resetDate).toHaveBeenCalled();
      expect(component.btnSubmitDisable).toBe(true);

      //No objects
      spyOnRequestResponse.and.returnValue(promise2);
      component.keywords = ['palais'];

      component.makeQuery();
      setTimeout(()=>{
        expect(component.keywords).toEqual([]);
        expect(component.errorRequest).toBe("La requête n'a pas de résultats.");
        done();
      },1000);
      
    },1000)
    

  })

  it('should make rectangle selection',async(done)=>{
    let promise = new Promise(function(resolve, reject) {
      setTimeout(function() {
        resolve({ coordsRec: [45,46,48,49] });
      }, 300);
    });
    spyOn(component.mapService, 'enableRectangleSelection').and.returnValue(promise);
    component.makeRectangleSelection();
    setTimeout(()=>{
      expect(component.bbox).toEqual([45,46,48,49]);
      expect(component.btnSubmitDisable).toEqual(false);
      done();}
      ,1000);

  })


  it('should be called when click on button', () => {
    component.queryService.initDataloaded = true;
    fixture.detectChanges();

    let buttonCollapseZoneGeo = fixture.debugElement.nativeElement.querySelector('#buttonCollapseZoneGeo');
    
    expect(component.faIconRotationCollapsed['buttonCollapseZoneGeo']).toBe(180);
    buttonCollapseZoneGeo.click();
    expect(component.faIconRotationCollapsed['buttonCollapseZoneGeo']).toBe(0);

    let buttonCollapseKeyword = fixture.debugElement.nativeElement.querySelector('#buttonCollapseKeyword');
    expect(component.faIconRotationCollapsed['buttonCollapseKeyword']).toBe(0);
    buttonCollapseKeyword.click();
    expect(component.faIconRotationCollapsed['buttonCollapseKeyword']).toBe(180);
  });


});
