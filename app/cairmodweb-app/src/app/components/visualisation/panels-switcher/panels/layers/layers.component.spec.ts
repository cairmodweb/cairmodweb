import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import * as L from 'leaflet';

import { LayersComponent } from './layers.component';

describe('LayersComponent', () => {
  let component: LayersComponent;
  let fixture: ComponentFixture<LayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change',()=>{
    var geojsonMarkerOptions = {
      radius: 5,
      fillColor: '#ff7800',
      color: "#000",
      weight: 1,
      opacity: 1,
      fillOpacity: 0.8,
      interactive: true
    };

    let marker1 = L.circleMarker([30.2, 31.2],geojsonMarkerOptions).bindPopup('mark1'),
    marker2    = L.circleMarker([30.3, 31.3],geojsonMarkerOptions).bindPopup('mark2'),
    marker3    = L.circleMarker([30.4, 31.4],geojsonMarkerOptions).bindPopup('mark3'),
    marker4   = L.circleMarker([30.5, 31.5],geojsonMarkerOptions).bindPopup('mark4');
    let layer = L.layerGroup([marker1,marker2,marker3,marker4]);
    layer.visible = true;
    let event;

    //Visible to not visible
    component.onChange(event,layer);
    
    expect(layer.visible).toBe(false);
    let noPopUp = true;
    var keys = Object.keys(layer._layers)
    keys.forEach(function (key) {
      if (layer._layers[key]._popup){
        noPopUp = false;
      }
    })
    expect(noPopUp).toBe(true);

    //Not visible to visible
    noPopUp = false
    spyOn(component.mapService,"createPopup");
    component.onChange(event,layer);
    expect(layer.visible).toBe(true);
    var keys = Object.keys(layer._layers)
    keys.forEach(function (key) {
      if (layer._layers[key]._popup){
        noPopUp = false && noPopUp;
      }
    })
    expect(noPopUp).toBe(false);


  })
});
