import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/services/map/map.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { faPlus, faTimes, faVectorSquare, faEraser, faAngleDown } from '@fortawesome/free-solid-svg-icons'
import { QueryService } from "src/app/services/query/query.service";

declare var $: any;

@Component({
  selector: 'cairmodweb-query-module',
  templateUrl: './query-module.component.html',
  styleUrls: ['./query-module.component.css']
})
export class QueryModuleComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    public queryService: QueryService,
    public mapService: MapService
  ) { };

  // Icon importations for HTML
  public plusIcon = faPlus;
  public crossIcon = faTimes;
  public squareIcon = faVectorSquare;
  public eraserIcon = faEraser;
  public angleIcon = faAngleDown;

  // For the triangles 
  public collapsedDegrees = 180;
  public faIconRotationCollapsed = {
    buttonCollapseKeyword: 0,
    buttonCollapseDate: this.collapsedDegrees,
    buttonCollapseZoneGeo: this.collapsedDegrees,
  }

  // Variables for form management 
  public requestForm: FormGroup;
  public btnAddKeywordDisable: boolean = true;
  public btnSubmitDisable: boolean = true;

  public errorInvalidKeyword: boolean = false;
  public loading: boolean = false;
  public errorRequest: string = '';

  public keywords: string[] = [];
  public regex = new RegExp("^[A-Za-zÀ-ÖØ-öø-ÿ-\--œ]*$");
  public isValidKeyword: boolean = true;

  public bbox: number[] = [];

  public date_selection_type: string = "disable";
  public dateExtremum = this.queryService.dateExtremum;



  ngOnInit() {
    // Create a FormBuilder
    this.requestForm = this.formBuilder.group({
      keyword: [''],
      date_selection_type: this.date_selection_type,
      start_date: null,
      stop_date: null
    });
    // Listen To value change
    this.onValueChanges();
  }



  /* --- KEYWORD VALIDATION FUNCTIONS --- */

  /**
   * Boolean for the emptiness of the form field 'keyword'
   */
  isKeywordEmpty(): boolean {
    return this.requestForm.get('keyword').value == ""
  }

  /**
   * Test if keyword format is valid
   */
  isKeywordValid(): boolean {
    this.errorRequest = '';
    return this.requestForm.get('keyword').value.length > 0
      && this.regex.test(this.requestForm.get('keyword').value)
  }


  /**
   * Listen to the input of the HTML form field 'keyword'
   * Enable/Disable the button to add keyword
   * Print/Hide the message for invalid Keyword format
   */
  inputKeyword(): void {
    this.isValidKeyword = this.isKeywordValid();
    this.btnAddKeywordDisable = !this.isValidKeyword;

    this.errorInvalidKeyword = !this.isValidKeyword;

    if (this.isKeywordEmpty()) {
      this.errorInvalidKeyword = false
    }
  }


  /**
   * Add keyword to the list, if valid
   */
  addKeyword(): void {
    this.keywords.push(this.requestForm.get('keyword').value);
    this.btnAddKeywordDisable = true;

    // Clear input
    this.requestForm.get('keyword').setValue("");
  }

  /**
   * Delete the i element of the keywords list
   * @param i Index
   */
  deleteKeyword(i: number): void {
    this.keywords.splice(i, 1);
    this.btnSubmitDisable = !this.isFormValid();
  }


  /* --- ZONE GEO VALIDATION FUNCTIONS */

  /**
   * Call mapService functions to make the rectangle selection
   */
  makeRectangleSelection() {
    this.mapService.enableRectangleSelection()
      .then(res => {
        this.bbox = Object.values(res)[0];
        this.btnSubmitDisable = !this.isFormValid();
      });
  }

  /**
   * Call the mapService to remove the rectangle selection
   */
  removeRectangleSelection() {
    this.mapService.removeRectangleSelection()
      .then(() => {
        this.bbox = [];
        this.btnSubmitDisable = !this.isFormValid();
      })
  }


  /* --- DATE VALIDATION FUNCTIONS --- */

  /**
   * Boolean for the validity of the date 
   * Valid : - if mode before and stop_date
   *         - if mode after and start_date
   *         - if mode between and start_date and stop_date
   */
  isDateValid(): boolean {
    //Initialisation
    let dateValid = false;
    let start_date = this.requestForm.get('start_date').value;
    let stop_date = this.requestForm.get('stop_date').value;

    // Test in fonction of date type
    switch (this.requestForm.get('date_selection_type').value) {
      case "before":
        if (stop_date) {
          dateValid = (stop_date != null) && stop_date > this.dateExtremum[0];
        }
        break;
      case "after":
        if (start_date) {
          dateValid = (start_date != null) && start_date > this.dateExtremum[0];
        }
        break;
      case "between":
        if (start_date && stop_date) {
          dateValid = (start_date != null) && (stop_date != null)
            && start_date < stop_date
            && start_date > this.dateExtremum[0];
        }
        break;
      default:  // even 'disable'
        ;
    }
    return dateValid;
  }

  /**
   * Reset the date values in the form
   */
  resetDate() {
    this.requestForm.get('start_date').setValue(null);
    this.requestForm.get('stop_date').setValue(null);
  }



  /* --- FORM VALIDATION FUNCTIONS --- */

  /**
   * Test if form is valid 
   * valid : if input field empty && keyword list not empty
   *      or if zone geo ok
   * Enable/Disable the submit button
   */
  isFormValid(): boolean {
    let keywordPart = (this.requestForm.get('keyword').value == ''
      && this.keywords.length > 0);

    let geoPart = (this.requestForm.get('keyword').value == '' && this.bbox.length > 0);

    let datePart = (this.requestForm.get('keyword').value == '' && this.isDateValid());

    return keywordPart || geoPart || datePart;
  }


  /**
   * Listen to changes on the form field 'keyword'
   */
  onValueChanges(): void {
    this.requestForm.valueChanges
      .subscribe(val => {
        this.btnSubmitDisable = !this.isFormValid();
        this.date_selection_type = this.requestForm.get('date_selection_type').value;
      });
  }


  /**
   * Process request at the submit of the form
   */
  makeQuery() {
    // Display loading
    this.loading = true;
    this.btnSubmitDisable = true;

    // Fill form un queryService
    this.queryService.setFormDataKeywords(this.keywords);
    this.queryService.setFormDataDates(this.requestForm.get('start_date').value, this.requestForm.get('stop_date').value);

    // Make the request
    this.queryService.getRequestResponse()
      .then((queryResult) => {
        // Manage the details
        this.loading = false; // Loading spinner
        this.keywords = []; // Keywords list
        this.bbox = []; // Bbox
        this.mapService.removeRectangleSelection(); // Clean rectangle selection
        this.requestForm.get('date_selection_type').setValue('disable'); // date type
        this.resetDate(); // Date values
        this.btnSubmitDisable = !this.isFormValid(); // Disable submit btn

        
        // Manage if the result is empty
        if (queryResult.features && queryResult.features.length == 0) {
          this.errorRequest = "La requête n'a pas de résultats."
        }
        else {
          this.errorRequest = '';
        }
      })
      .catch(() => {
        this.loading = false;
        this.btnSubmitDisable = !this.isFormValid(); // Disable submit btn
        this.errorRequest = "Le serveur est momentanément indisponible, veuillez réessayez ultérieurement."
      });
  }

  /* --- OTHER FUNCTIONS --- */
  /**
  * Change the rotation of the requestType icon
  * Indicate if it is opened or not
  * @param event 
  */
  clickRequestType(event) {
    let id = event.target.id;
    this.faIconRotationCollapsed[id] = this.faIconRotationCollapsed[id] == 0 ? this.collapsedDegrees : 0;
  }
}