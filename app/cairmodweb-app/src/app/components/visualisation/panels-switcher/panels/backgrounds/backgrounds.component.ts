import { Component, OnInit } from '@angular/core';
import { MapService } from 'src/app/services/map/map.service';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import { EventService } from 'src/app/services/event/event.service';

declare var $: any;

@Component({
  selector: 'cairmodweb-backgrounds',
  templateUrl: './backgrounds.component.html',
  styleUrls: ['./backgrounds.component.css']
})
export class BackgroundsComponent implements OnInit {


  public mapNames = [];

  constructor(
    public mapService: MapService,
    public eventService: EventService
  ) { }

  // Icon importations for HTML
  public angleIcon = faAngleDown;

  // For the triangles 
  public collapsedDegrees = 180;
  public faIconRotationCollapsed = {};

  ngOnInit(): void {
    this.mapNames = this.mapService.getBackgroundNames();

    // Initialize IconRotation when collectionList is initialised
    this.eventService.getCollectionListChanged()
      .subscribe(colList => {
        this.mapService.collectionsList.forEach(col => {
          this.faIconRotationCollapsed['buttonCollapse' + col.name] = this.collapsedDegrees;
        })
      })
  }

  /**
   * Change the rotation of the collection icon
   * Indicate if it is opened or not
   * @param event 
   */
  clickCollection(event) {
    let id = event.target.id;
    this.faIconRotationCollapsed[id] = this.faIconRotationCollapsed[id] == 0 ? this.collapsedDegrees : 0;
  }

  /**
   * Change the opacity of the layer on click
   * @param {JSON} background The layer
   * @param {Event} evt Event of the slider
   */
  public changeOpacity(background, evt): void {
    this.mapService.changeOpacityRaster(background.name, evt.target.value);
    background.opacity = evt.target.value;
  }

  /**
   * Change the background on click
   * @param {JSON} background The layer
   */
  public changeBackground(background): void {
    // Reset opacity
    background.opacity = 100;

    // Add or remove the background
    if (background.visible) {
      this.mapService.addBackgroundToMap(background.name);
    }
    else {
      this.mapService.removeBackground(background.name);
    }

    // Update the visibility global of the background collection
    let collection = null;
    this.mapService.collectionsList.forEach(coll => {
      if (background.collection == coll.name) {
        collection = coll;
      }
    })
    this.verifyVisibilityCollection(collection);
  }

  /**
   * Change on click the visibility of all backgrounds in the collection
   * @param {Event} event Event of the checkbox
   * @param {string} collection The collection
   */
  public changeBackGroundCollection(event, collection): void {
    // Search for the backgrounds in the collection
    this.mapService.backgroundList.forEach(background => {
      // Update background visibility
      if (background.collection == collection.name) {
        background.visible = event.target.checked
      }

      // Update opacity and add or remove from map
      if (background.visible) {
        background.opacity = 100;
        this.mapService.addBackgroundToMap(background.name);
      } else {
        this.mapService.removeBackground(background.name);
      }
    })

    // Update the visibility global of the background collection
    this.verifyVisibilityCollection(collection);

  }

  /**
   * Verify the allVisible attribute of our collection 
   * To be true
   * @param {JSON} collection The collection
   */
  public verifyVisibilityCollection(collection) {
    let allVisible = true;

    // Check visibility of all backgrounds in the collection
    this.mapService.backgroundList.forEach(background => {
      if (background.collection == collection.name) {
        allVisible = allVisible && background.visible;
      }
    })

    // Update the allvisibility attribute
    collection.allVisible = allVisible;
  }




}
