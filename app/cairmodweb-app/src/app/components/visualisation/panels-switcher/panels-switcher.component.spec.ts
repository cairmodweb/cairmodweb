import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelsSwitcherComponent } from './panels-switcher.component';
import {By} from "@angular/platform-browser";

describe('PanelsComponent', () => {
  let component: PanelsSwitcherComponent;
  let fixture: ComponentFixture<PanelsSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelsSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelsSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should close the panel', () => {  
	var panelSwitcherWrapperBefore = fixture.debugElement.query(By.css('#panel-switcher-wrapper')).nativeElement.classList.contains('active');
	var panelSwitcherOptsBefore = fixture.debugElement.query(By.css('.panel-switcher-opts')).nativeElement.classList.contains('active');

	component.closePanel();
	
	var panelSwitcherWrapperAfter = fixture.debugElement.query(By.css('#panel-switcher-wrapper')).nativeElement.classList.contains('active');
	var panelSwitcherOptsAfter = fixture.debugElement.query(By.css('.panel-switcher-opts')).nativeElement.classList.contains('active');
	
	expect(panelSwitcherWrapperBefore).not.toEqual(panelSwitcherWrapperAfter);
	expect(panelSwitcherOptsBefore).not.toEqual(panelSwitcherOptsAfter);
  })
  
  it('should select and open the panel to show', () => {  
	
	// Case when open the panel
	var panelSwitcherWrapperBefore = fixture.debugElement.query(By.css('#panel-switcher-wrapper')).nativeElement.classList.contains('active');
	var panelSwitcherOptsBefore = fixture.debugElement.query(By.css('.panel-switcher-opts')).nativeElement.classList.contains('active');

	component.setPanelToShow("Edition");
	
	var panelSwitcherWrapperAfter = fixture.debugElement.query(By.css('#panel-switcher-wrapper')).nativeElement.classList.contains('active');
	var panelSwitcherOptsAfter = fixture.debugElement.query(By.css('.panel-switcher-opts')).nativeElement.classList.contains('active');
	
	expect(panelSwitcherWrapperBefore).not.toEqual(panelSwitcherWrapperAfter);
	expect(panelSwitcherOptsBefore).not.toEqual(panelSwitcherOptsAfter);
	
	
	// Case when close the panel
	component.setPanelToShow("Edition");
	
	var panelSwitcherWrapperAfter2ndCall = fixture.debugElement.query(By.css('#panel-switcher-wrapper')).nativeElement.classList.contains('active');
	var panelSwitcherOptsAfter2ndCall = fixture.debugElement.query(By.css('.panel-switcher-opts')).nativeElement.classList.contains('active');
	
	expect(panelSwitcherWrapperAfter).not.toEqual(panelSwitcherWrapperAfter2ndCall);
	expect(panelSwitcherOptsAfter).not.toEqual(panelSwitcherOptsAfter2ndCall);
  })

  
  


});
