import { Component, OnInit, Input } from '@angular/core';


import { faBookReader } from '@fortawesome/free-solid-svg-icons';

declare var $: any;

@Component({
  selector: 'cairmodweb-panels-switcher',
  templateUrl: './panels-switcher.component.html',
  styleUrls: ['./panels-switcher.component.css']
})
export class PanelsSwitcherComponent implements OnInit {

  panelIcon = faBookReader;

  public panelToShow = 'Edition';

  // As soon we close the panel, we want the startup dialog to hide. But for
  // some reason (side effect of Angular's rerender on Bootstrap's popover?),
  // the DOM element for the dialog is created again when one clicks on the
  // close panel button. Thus we use a local variable that we check every time
  // the popover is "inserted", to force it to hide after a first click to close
  // (see below).
  private showStartDialog: Boolean = true;


  constructor() { }

  ngOnInit(): void {
    let self = this;
    $(".btn-panel-switcher").click(function (e) {
      e.preventDefault();
    });
    $('#panel-switcher-wrapper').on('inserted.bs.popover', function () {
      if (!self.showStartDialog) {
        $('#panel-switcher-wrapper').popover('hide');
      }
    })
  }


  ngAfterViewInit() {
    // Initialize tootltips of all components, in particular in the right panel
    $(document).ready(function () {
      $('app-map-panel-switcher [data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
      });
    });

    // By default, panel open on edition panel
    this.setPanelToShow(this.panelToShow);
  }


  /**
   * Select and open the panel choosen by the user
   * @param {string} newPanelToShow  
   * @param force 
   */
  setPanelToShow(newPanelToShow, force?: boolean) {
    let oldPanel = this.panelToShow;
    let newPanel = newPanelToShow;
    this.panelToShow = newPanelToShow;  // update panelToShow

    // Manage the opening/closure of the panel on the right component
    if (!$(".panel-switcher-opts").hasClass("active")) {
      // If the panel is closed, we open it
      $("#panel-switcher-wrapper").toggleClass("active");
      $(".panel-switcher-opts").toggleClass("active");
    } 
    else {
      // If the panel is already opened
      if (!force && (oldPanel == newPanel)) {
        // If click on the option already visible: closure of the panel 
        this.closePanel();
      }
    }
  }


  /**
   * Close the panel
   */
  closePanel() {
    $(".panel-switcher-opts").toggleClass("active");
    $("#panel-switcher-wrapper").toggleClass("active");
  }
}

