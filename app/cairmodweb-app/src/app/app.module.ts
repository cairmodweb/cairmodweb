import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { VisualisationComponent } from './components/visualisation/visualisation.component';
import { InfoComponent } from './components/info/info.component';
import { MapComponent } from './components/visualisation/map/map.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PanelsSwitcherComponent } from './components/visualisation/panels-switcher/panels-switcher.component';
import { QueryModuleComponent } from './components/visualisation/panels-switcher/panels/query-module/query-module.component';
import { LayersComponent } from './components/visualisation/panels-switcher/panels/layers/layers.component';
import { BackgroundsComponent } from './components/visualisation/panels-switcher/panels/backgrounds/backgrounds.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { ComparisonComponent } from './components/comparison/comparison.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VisualisationComponent,
    InfoComponent,
    MapComponent,
    NavbarComponent,
    PanelsSwitcherComponent,
    QueryModuleComponent,
    LayersComponent,
    BackgroundsComponent,
    ComparisonComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, 
    DialogModule, 
    ButtonsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
