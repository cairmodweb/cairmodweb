import { Injectable } from '@angular/core';
import { MapService } from '../map/map.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  constructor(
    public mapService: MapService,
  ) { }

  // The form
  protected formDatas = {
    keywords: [],
    theme: "",
    start_date: 0,
    stop_date: 0
  }

  // API url
  public urlAPI = environment.urls.api;
  public gazeteerNumber =  environment.gazeteer.thid;
  public initDataloaded = false;

  // The extremum for dates
  public dateExtremum = [ environment.gazeteer.minimalDate, new Date().getFullYear() ]

  // The response of the query 
  public queryResponse;
  // Follow number of request
  public nbResult = 0;

  /* --- GETTERS && SETTERS --- */

  /**
  * Get variable "keywords" of "formDatas".
  */
  getFormDataKeywords() {
    return this.formDatas['keywords'];
  }
  /**
  * Set variable "keywords" of "formDatas".
  */
  setFormDataKeywords(value: string[]) {
    this.formDatas['keywords'] = value;
  }

  /**
   * Get all keywords parameters 
   */
  getKeywordParam(): string {
    let string = "";

    for (let i = 0; i < this.formDatas["keywords"].length; i++) {
      string += this.formDatas["keywords"][i] + ",";
    }
    // Removing last ',' before returning
    return string.substring(0, string.length - 1);
  }

  /**
   * Get geo parameters
   */
  getGeoParam(): string {
    let geoParam = "";

    if (this.mapService.getDrawnItemsBbox()) {
      geoParam += "[" + this.mapService.getDrawnItemsBbox() + "]";
    }
    return geoParam;
  }

  /**
   * Get variable "start_date" and "stop_date" of "formDatas".
   * @returns {Number[]} [ start_date, stop_date ]
   */
  getFormDataDates() {
    let start = undefined;
    let stop = undefined;

    if (this.formDatas['start_date'] != null || this.formDatas['stop_date'] != null) {
      start = this.formDatas['start_date'] != null ? this.formDatas['start_date'] : this.dateExtremum[0];
      stop = this.formDatas['stop_date'] != null ? this.formDatas['stop_date'] : this.dateExtremum[1];
    }
    return [start, stop];
  }


  /**
   * Set variable "start_date" and "stop_date" of "formDatas".
   * @param start_date 
   * @param stop_date 
   */
  setFormDataDates(start_date: number, stop_date: number) {
    this.formDatas['start_date'] = start_date;
    this.formDatas['stop_date'] = stop_date;
  }


  /**
   * Get variable "theme" of "formDatas".
   * @returns {String} theme
   */
  getFormDataTheme(): string {
    let theme = undefined;

    if (this.formDatas['theme'] != "") {
      theme = this.formDatas['theme']
    }

    return theme;
  } 

  /**
   * Set variable "theme" of "formDatas".
   * @param theme 
   */
  setFormDataTheme(theme: string = "") {
    this.formDatas['theme'] = theme;
  }


  /* --- REQUEST FUNCTIONS --- */

  /**
  * Create string of the request for the API
  */
  createRequestString(): string {
    // Define general parameters
    let host = this.urlAPI;
    let searchRequest = "api/search";
    let gazeteerNumber = this.gazeteerNumber;

    // Get query params
    let directoryParam = "directory=" + this.getFormDataTheme();
    let keywordsParam = "keyword=" + this.getKeywordParam();
    let geoParam = "coordsRec=" + this.getGeoParam();
    let dates = this.getFormDataDates();
    let dateParam = "start=" + dates[0] + "&stop=" + dates[1];

    // Build the full request
    let fullRequest = host + searchRequest + "/" + gazeteerNumber + "?"
      + directoryParam + "&" + keywordsParam + "&" + geoParam + "&" + dateParam;
    console.log(fullRequest);

    return fullRequest;
  }

  /**
  * Make the actual request, save the result, and add Layers to Map
  */
  getRequestResponse() {
    return fetch(this.createRequestString(), { credentials: 'omit' })
      .then(resp => resp.json())
      .then(data => {
        this.queryResponse = data;

        // If items in result
        if (this.queryResponse.features.length != 0) {
          
          // Create layerName
          let layerName = "";
          
          if (this.getFormDataTheme() == "root") {
            layerName = "Gazetier_entier";
          }
          else {
            // Update number of result
            this.nbResult++;

            layerName = "Resultat_" + this.nbResult;
          }
          // Add layer result
          this.mapService.addLayer(this.queryResponse, layerName);
        }
        // If no items in result
        else {
          console.log("no result");
        }

        return this.queryResponse;
      })
  }


  /**
   * Get all datas from the gazetier (when theme = root)
   */
  getAllDataFromGazetier() {
    // Fix directory/theme for the request
    this.setFormDataTheme("root");

    this.getRequestResponse()
      .then((queryResult) => {
        // Only information for initialisation
        if (queryResult.features && queryResult.features.length == 0) {
          console.log("no results are received from all results");
        }
        else {
          console.log("all are received results");
        }

        // Reset Form
        this.setFormDataTheme("");
        this.initDataloaded = true;
      })
      .catch(() => {
        console.log("error in getAllDatas");
        // Reset Form
        this.setFormDataTheme("");
        this.initDataloaded = true;
      });
  }
}

