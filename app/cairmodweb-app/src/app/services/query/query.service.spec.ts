import { TestBed } from '@angular/core/testing';
import { QueryService } from './query.service';
import * as L from 'leaflet';
import fetchMock from 'fetch-mock';

describe('QueryService', () => {
  let service: QueryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QueryService);
  });

  afterEach(()=>{
    fetchMock.reset();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return Geoparam', () => {
    expect(service.getGeoParam()).toBe("");
    var bounds = [[37.76656, -122.456709], [37.767202, -122.455316]];     
    var rectangle = L.rectangle(bounds);
    service.mapService.drawnItems.addLayer(rectangle);
    expect(service.getGeoParam()).toBe("[-122.456709,37.76656,-122.455316,37.767202]");
  });

  it('should get form date',()=>{
    //No date
    service.setFormDataDates(null,null);
    expect(service.getFormDataDates()[0]).toBe(undefined);
    expect(service.getFormDataDates()[1]).toBe(undefined);
    //After
    service.setFormDataDates(1880,null);
    expect(service.getFormDataDates()[0]).toBe(1880);
    expect(service.getFormDataDates()[1]).toBe(new Date().getFullYear());
    //Before
    service.setFormDataDates(null,2000);
    expect(service.getFormDataDates()[0]).toBe(0);
    expect(service.getFormDataDates()[1]).toBe(2000);
    //Between
    service.setFormDataDates(1880,1980);
    expect(service.getFormDataDates()[0]).toBe(1880);
    expect(service.getFormDataDates()[1]).toBe(1980);
  })


  it('should return form keywords',()=> {
    service.setFormDataKeywords(["test","toto"]);
    expect(service.getFormDataKeywords()[0]).toBe("test");
    expect(service.getFormDataKeywords()[1]).toBe("toto");
  })

  it('should return keywords',()=>{
    service.setFormDataKeywords(["test","toto"]);
    expect(service.getKeywordParam()).toBe("test,toto");
  })

  it('should return API request',()=>{
    //Date only
    service.setFormDataDates(1880,1980);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=&coordsRec=&start=1880&stop=1980");
    
    
    //Keyword + Date
    service.setFormDataKeywords(["test","toto"]);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=test,toto&coordsRec=&start=1880&stop=1980");

    //Keyword only
    service.setFormDataDates(null,null);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=test,toto&coordsRec=&start=undefined&stop=undefined");
  

    //Geoselection only
    service.setFormDataDates(null,null);
    service.setFormDataKeywords([]);
    var bounds = [[37.76656, -122.456709], [37.767202, -122.455316]];     
    var rectangle = L.rectangle(bounds);
    service.mapService.drawnItems.addLayer(rectangle);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=&coordsRec=[-122.456709,37.76656,-122.455316,37.767202]&start=undefined&stop=undefined");

    //Geoselection + Date
    service.setFormDataDates(1880,1980);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=&coordsRec=[-122.456709,37.76656,-122.455316,37.767202]&start=1880&stop=1980");

    //Geoselection + keywords
    service.setFormDataDates(null,null);
    service.setFormDataKeywords(["test","toto"]);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=test,toto&coordsRec=[-122.456709,37.76656,-122.455316,37.767202]&start=undefined&stop=undefined");

    //Geoselection + keyword + date
    service.setFormDataDates(1880,1980);
    expect(service.createRequestString()).toBe(service.urlAPI+"api/search/"+service.gazeteerNumber+"?directory=undefined&keyword=test,toto&coordsRec=[-122.456709,37.76656,-122.455316,37.767202]&start=1880&stop=1980");

  })

  it('getAllDataFromGazetier',async(done)=>{
    let rootUrl = service.urlAPI+'api/search/'+service.gazeteerNumber+'?directory=root&keyword=&coordsRec=&start=0&stop=0';
    expect(rootUrl).toEqual('http://localhost:3000/api/search/42?directory=root&keyword=&coordsRec=&start=0&stop=0');
    fetchMock.mock(rootUrl, {
      status: 200,
      body: {
        features: [
          { id: 1 },
        ],
      },
    });

    service.getAllDataFromGazetier();
    expect(service.getFormDataTheme()).toEqual('root');

    setTimeout(()=>{
      expect(service.getFormDataTheme()).toEqual(undefined);
      done();
    },1000)


  })
  
  it('getRequestResponse',async(done)=>{
    
    service.setFormDataKeywords(["test","toto"]);
    spyOn(service.mapService, 'addLayer');
    
    fetchMock.mock(service.createRequestString(), {
      status: 200,
      body: {
        features: [
          { id: 1 },
        ],
      },
    });
    
    service.getRequestResponse().then(()=>{
      expect(service.nbResult).toBe(1);

      service.setFormDataDates(1880,1980);

      fetchMock.restore();
      fetchMock.mock(service.createRequestString(), {
        status: 200,
        body: {
          features: [
            { id: 1 },
          ],
        },
      });
      service.getRequestResponse().then(()=>{
        expect(service.nbResult).toBe(2);
        fetchMock.restore();
        done();
      });
      
    })
    
  })

  it('should return form theme', ()=>{
    expect(service.getFormDataTheme()).toBe(undefined);
    service.setFormDataTheme("test");
    expect(service.getFormDataTheme()).toBe("test");
  })


});
