import { TestBed } from '@angular/core/testing';

import { MapService } from './map.service';
import * as L from 'leaflet';

describe('MapService', () => {
  let service: MapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapService);
  });


  function updateQueryLayers(layers: Array<Object>) {
    layers.forEach(layer => {
      service.queryLayers.push(layer);
    })
  }
  function updateBackgroundNames(names: Array<String>) {
    names.forEach(name => {
      service.backgroundNames.push(name);
    })
  }

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should reset queryLayers', () => {
    updateQueryLayers([{ key1: 'value1' }, { key2: 'value2' }]);
    service.resetQueryLayers();
    expect(service.queryLayers.length).toEqual(0);
    expect(service.queryLayers).toEqual([]);
  })


  it('should return backgroundNames', () => {
    updateBackgroundNames(["name1", "name2"]);
    expect(service.getBackgroundNames()).toEqual(["name1", "name2"]);
  })

  it('should generate a random color',() =>{
    let randomColor = service.getRandomColor();
    expect(randomColor.length).toBeLessThan(8);
    expect(randomColor[0]).toBe("#");
    const digits_only = string => [...string].every(c => '0123456789ABCDEFabcdef'.includes(c));
    expect(digits_only(randomColor.substring(1))).toBe(true);
  })

  it('should return drawnItems bbox',()=>{
    var bounds = [[37.76656, -122.456709], [37.767202, -122.455316]];     
    var rectangle = L.rectangle(bounds);
    service.drawnItems.addLayer(rectangle);
    let BBox  = service.getDrawnItemsBbox();
    expect(BBox[0]).toBe(-122.456709);
    expect(BBox[3]).toBe(37.767202);
    expect(BBox[2]).toBe(-122.455316);
    expect(BBox[1]).toBe(37.766560);

  })

  it('should replace character', () => {
    expect(service.changeCaract("Test")).toBe("Test");
    expect(service.changeCaract("Test_2")).toBe("Test 2");
    expect(service.changeCaract("Test2_0_test")).toBe("Test2 0 test");
  })

  it('should getQueryLayers', () => {
    expect(service.getQueryLayers()).toEqual([]);

    var layer1 = { zOrder: 2 };
    var layer2 = { zOrder: 1 };
    var layer3 = { zOrder: 0 };
    
    service.queryLayerGroup.addLayer(layer1);
    service.queryLayerGroup.addLayer(layer2);
    service.queryLayerGroup.addLayer(layer3);

    spyOn(service, "orderQueryLayersInMap");

    var queryLayers = service.getQueryLayers();
    expect(service.orderQueryLayersInMap).toHaveBeenCalled();
    
    expect(queryLayers[0]['zOrder']).toEqual(service.queryLayerGroup.getLayers()[2]['zOrder']);
    expect(queryLayers[1]['zOrder']).toEqual(service.queryLayerGroup.getLayers()[1]['zOrder']);
    expect(queryLayers[2]['zOrder']).toEqual(service.queryLayerGroup.getLayers()[0]['zOrder']);

    expect(queryLayers[0]['zOrder']).toBe(0);
    expect(queryLayers[1]['zOrder']).toBe(1);
    expect(queryLayers[2]['zOrder']).toBe(2);   
  })

  it('should add pop-up',()=>{
    let marker1 = L.marker([30.2, 31.2]);
    spyOn(service,'createHTMLPopup').and.returnValue('pop-up content');
    let popUp = service.createPopup(marker1);
    let content = popUp._content;
    expect(content).toEqual('pop-up content');
  })
  
});
