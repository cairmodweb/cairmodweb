import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { xml2json } from 'xml-js';
//import 'leaflet-draw';
import '../../../../node_modules/leaflet-draw/dist/leaflet.draw-src.js';
import { environment } from 'src/environments/environment';
import { EventService } from 'src/app/services/event/event.service';



@Injectable({
  providedIn: 'root'
})
export class MapService {

  /* Initialisation of variables */
  // Map variables
  public map;
  private tiles;
  private scalebar;
  private mousePosition;

  // Query Layer variabels
  public queryLayers = [];
  public queryLayerGroup = new L.LayerGroup();

  // Background variables
  public backgroundNames = [];
  public backgroundList = [];
  public collectionsList = [];
  public collectionLoaded = false;
  public groupNames = [];
  public backgroundInfo = {};
  public backgroundLayerGroup = new L.LayerGroup();

  // Rectangle selection variables
  public drawnItems = new L.FeatureGroup();


  // Temporal variables
  requestURL = environment.urls.mapserver + "SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities";
  mapInitialCoordinates = environment.gazeteer.mapInitialCoordinates; // Caire Coordinates
  mapInitialZoom =  environment.gazeteer.mapInitialZoom;


  constructor(
    private eventService: EventService
  ) {
    // Chargement of the mapServer data
    this.requestLayer(this.requestURL).then(() => { 
      // Alert of the chargment
      this.eventService.emitCollectionListChanged(this.collectionsList) 
      this.collectionLoaded = true;
    });
  }

  /**
   * Initialize the Leaflet map
   */
  public initMap(): void {
    // Initialisation center and location of the map
    this.map = L.map('map', {
      center: this.mapInitialCoordinates,
      zoom: this.mapInitialZoom,
      renderer: L.canvas()
    });


    // Init OSM background
    this.initOSMBackground();

    // Init layerGroup for rasters
    this.backgroundLayerGroup.addTo(this.map);
    this.queryLayerGroup.addTo(this.map);

    // Initialise the FeatureGroup to store editable layers 
    this.initRectangleSelectionLayer();

    // Init map controls
    this.addMousePosition(this.map);
    this.addScaleBar(this.map);
  }



  /* --- FUNCTION FOR LAYERS MANAGEMENT --- */
  /**
   * Sets initialisation parameters
   * @param {String} requestURL Url of the capacities
   */
  public async requestLayer(requestURL) {
    // Get result
      let result = await fetch(requestURL)
      .then(response => response.text())
      .then(str => this.getLayers(str));

      // Push into variables
      this.backgroundNames = result[0];
      this.backgroundInfo = result[1];
      this.groupNames = result[2];
      this.collectionsList = result[3];
      this.backgroundList = result[4];  
  }


  /**
   * Returns the name of every layer in GetCapabilities, several informations of layer and names of every collections
   * @param str Stringformat of the GetCapacities XML
   */
  public getLayers(str): any {
    let namesList = [];
    let layersDescription = {};
    let namesGroupList = [];
    let backgroundList = [];

    let data = JSON.parse(xml2json(str));
    data["elements"].forEach(function (el) {
      if (el.type == "element") {
        el["elements"].forEach(function (el0) {
          if (el0.name == "Capability") {
            el0["elements"].forEach(function (el1) {
              if (el1.name == "Layer") {
                el1["elements"].forEach(function (el2) {
                  if (el2.name == "Layer") {
                    var namegroup = "";
                    el2["elements"].forEach(function (el3) {

                      if (el3.name == "Name") {
                        namegroup = el3["elements"][0].text;
                      } if (el3.name == "Layer") {
                        var layerName;
                        var southWest;
                        var northEast;
                        el3["elements"].forEach(function (el4) {
                          if (el4.name == "Name") {
                            layerName = el4["elements"][0].text;
                          }
                          if (el4.name == "BoundingBox") {
                            southWest = L.latLng(el4["attributes"]["miny"], el4["attributes"]["minx"]);
                            northEast = L.latLng(el4["attributes"]["maxy"], el4["attributes"]["maxx"]);

                          }

                        })
                        layersDescription[layerName] = {
                          "groupname": namegroup,
                          "southWest": southWest,
                          "northEast": northEast
                        }

                        // Collect background info in a objet
                        let background = {
                          name: layerName,
                          collection: namegroup,
                          visible: false,
                          opacity: 100
                        }
                        backgroundList.push(background);

                        // Add layerName to the namesList
                        namesList.push(layerName);

                        // Verify is the colleciton group is already in the list
                        let testValue = namesGroupList.includes(namegroup);
                        if (testValue == false) {
                          namesGroupList.push(namegroup);
                        }
                      }
                    })
                  }
                })
              }
            })
          }
        });
      }
    });

    // Create our collectionsList
    let collectionsList = [];
    namesGroupList.forEach(name => {
      collectionsList.push({
        name: name,
        allVisible: false
      })
    })


    return [ namesList, layersDescription, namesGroupList, collectionsList, backgroundList ];

  }

  /**
 * Removes Layer from interface (both queryLayers object and map)
 * @param layer Layer to remove
 */
  public removeLayer(layer): void {
    this.map.removeLayer(layer);
    this.queryLayerGroup.removeLayer(layer);
    let name = layer.name;
    this.queryLayers = this.queryLayers.filter((value, index, arr) => {
      return value.name != name;
    });
  }

  /**
  * Create the HTML popUp of the given feature
  * @param feature
  * @returns The html text for the popup
  */
  public createHTMLPopup(feature): string {
    // First character of Monument's name in Upper
    let label_frUpper = feature.properties.label_fr.replace(/^\w/, c => c.toUpperCase());

    // Keep only the 2 first image links
    let everyImgLinks = feature.properties.note_fr.split(" | ");
    let imgLinks = everyImgLinks.slice(0, 1);

    // Set the HTML images content
    let imgsHTMLContent = "";

    // Loops on every image to display
    imgLinks.forEach((imgLink) => {
      // Get natural size of img in URL
      if (imgLink.includes(",")) {
        let imgSize = imgLink.split("/")[9].split(",");

        // Set height and width of each img container - set its style
        let containerTbnlImageWidth = 170;
        let containerTbnlImageHeight = ((parseInt(imgSize[1]) * containerTbnlImageWidth) / parseInt(imgSize[0]));
        let styleContainerTbnlImage = `height: ${containerTbnlImageHeight}px;
                                        width: ${containerTbnlImageWidth}px;
										display: flex;`;

        //--- Set HTML pop-up content ---//
        imgsHTMLContent += `
                          <div class='containerTbnlImage' style='${styleContainerTbnlImage}'>` +
          /*<div class="spinner-border text-primary my-spinner" role="status">
            <span class="sr-only">Loading...</span>
          </div>*/
          `<img class='tbnlImage' src='${imgLink}'/>
                          </div>       
                          `;
      }
    });

    // Creating HTML environement of each img to display
    let everyImg = "<div id='containerDocLinks' class='text-center pr-4 pl-4'>";
    if (imgLinks.length > 0) {
      let imgIntro = "<div id='headerImgs' class='pt-3 pb-2 mt-3 pl-2'><p class='mt-0 mb-0' pl-2>Tous les documents associés au monument :</p></div>";
      let imgNb = 0;

      // Loop on each img
      everyImgLinks.forEach((imgLink) => {
        imgNb++;
        everyImg += `<a href="${imgLink}" class="btn btn-secondary text-white mr-1 mb-1" target="_blank" role="button">${imgNb}</a>`;
      });
      everyImg += "</div>";
      imgsHTMLContent = "<div id='containerImages' nbImg='" + imgLinks.length + "'>" + imgsHTMLContent + imgIntro + "</div>";
    }

    let atharLink = `https://athar.persee.fr/authority/${feature.properties.perseeid}`;

    // Join pop-up elements
    let popUpHTMLContent = `
                          <div id="popUp">
                            <div id="popUpHeader">
                              <h5>${label_frUpper}</h5>
                            </div>
                            <div id="popUpContent"> 
                              ${imgsHTMLContent} ${everyImg}
                              <div id='containerPersee' class='mt-3 w-100 text-center'><a id='perseeLink' href='${atharLink}' class="btn btn-secondary" target='_blank' role="button"><img src="https://athar.persee.fr/static/athar.png"/>Ouvrir la fiche Athar</a></div>
                            </div>
                          </div>
                          `;

    // @ToDo--------------------
    // Listen loading image event 

    return popUpHTMLContent;
  }


  /**
   * Creating the popup to display
   * @param feature Feature parameter of Leaflet layer
   */
  public createPopup(feature) {
    let popup = L.popup()
      .setContent(this.createHTMLPopup(feature));
    return popup;
  }


  /**
   * Add a geojson to the map
   * @param data Geojson to add as a layer
   * @param path Path to the geojson
   */
  public addLayer(data, path) {

    // Define style of the geojsonMarkers
    let color = this.getRandomColor();
    var geojsonMarkerOptions = {
      radius: 5,
      fillColor: color, //"#ff7800",
      color: "#000",
      weight: 1,
      opacity: 1,
      fillOpacity: 0.8,
      interactive: true
    };

    // Get the layer
    let layer = L.geoJSON(data, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, geojsonMarkerOptions);
      },
      onEachFeature: function (feature, layer) {
        return layer.bindPopup(this.createPopup(feature));
      }.bind(this)
    });


    // Saving important info in the layer
    layer.name = path;
    layer.visible = true;
    layer.style = geojsonMarkerOptions;
    layer.zOrder = this.queryLayers.length ? this.queryLayers.length : 0;

    layer.onclick = function () { console.log("layer clicked"); };

    // Add the layer to the map and ordering layer zindex
    this.queryLayerGroup.addLayer(layer);
    this.queryLayers.unshift(layer);  // Put the layer at the begining of the list
    this.orderQueryLayersInMap();
  }



  /**
   * Reset the queryLayers
   */
  public resetQueryLayers(): void {
    this.queryLayers.length = 0;
  }


  /**
   * Getter for queryLayers attribute
   * Update it from the Leaflet methods with the real list of layers
   * in queryLayerGroup
   * Order them in fonction of their zOrder (0 => the higher)
   * @returns The list queryLayers of layers resulting from queries
   */
  public getQueryLayers(): Array<JSON> {
    this.queryLayers = this.queryLayerGroup.getLayers()
      .sort((a, b) => {
        return a.zOrder - b.zOrder
      });
    this.orderQueryLayersInMap();
    return this.queryLayers;
  }

  /**
   * Order the queryLayers with their zOrder into the map
   * The lower the zOrder is, the higher the layer is displayed in the map
   */
  public orderQueryLayersInMap(): void {
    let nbLayer = this.queryLayers.length;
    for (let i = 1; i <= nbLayer; i++) {
      let layer = this.queryLayers[nbLayer - i];
      layer.bringToFront();
      layer.zOrder = nbLayer - i;
    }
  }

  /**
   * Get a random color in HEX notation
   */
  public getRandomColor(): String {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
  }



  /* --- FUNCTION FOR MAP CONTROLS --- */

  /**
   * Add the scaleBar to the Leaflet Map
   * @param map the map to add scaleBar to
   */
  addScaleBar(map): void {
    this.scalebar = L.control.scale({
      imperial: false,
      position: 'bottomright'
    })
    this.scalebar.addTo(map);
  }

  /**
   * Add the mousePosition Control to the Leaflet Map
   * @param map the map to add MousePosition
   */
  addMousePosition(map): void {
    this.mousePosition = L.control.mousePosition();
    this.mousePosition.addTo(map);
  }





  /* --- FUNCTION FOR BACKGROUND MANAGEMENT --- */
  /**
   * Initialize the OSM Background
   */
  initOSMBackground() {
    this.tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    this.tiles.addTo(this.map);
  }

  /**
   * Adds specitic wms layer to map
   * @param wmslayerName name of the wms layer to display on the map
   */
  public async addBackgroundToMap(wmslayerName) {
    let backgrounds = this.backgroundNames;
    let wmsLayer = L.tileLayer.wms(environment.urls.mapserver, {
      layers: wmslayerName,
      crs: L.CRS.EPSG4326,
      format: 'image/png',
      transparent: true,
      version: '1.1.0',
    }
    ).addTo(this.map);

    wmsLayer.name = wmslayerName;
    this.backgroundLayerGroup.addLayer(wmsLayer);

  }


  /**
   * Removes specitic wms layer from map
   * @param name names of the wms layer to remove from the map
   */
  removeBackground(name: any) {
    let _this = this;
    this.map.eachLayer(function (layer) {
      if (layer.name == name) {
        _this.backgroundLayerGroup.removeLayer(layer);
      }
    });
  }



  /**
   * Change the opacity of the Leaflet layer
   * @param name name of the layer 
   * @param opacity value of the new opacity
   */
  changeOpacityRaster(name: string, opacity: number) {
    this.map.eachLayer(function (layer) {
      if (layer.name == name) {
        layer.setOpacity(opacity / 100);
      }
    });

  }

  /**
   * Getter of BackgroundNames
   * @returns {Array[string]} list of the background names
   */
  getBackgroundNames() {
    return this.backgroundNames;
  }


  /* --- FUNCTION FOR RECTANGLE SELECTION --- */

  initRectangleSelectionLayer() {
    this.map.addLayer(this.drawnItems);
  }

  /**
   * Enables the rectangle selection
   */
  enableRectangleSelection() {
    return new Promise((resolve, reject) => {
      this.removeRectangleSelection();
      new L.Draw.Rectangle(this.map, { showArea: false }).enable(); // enabled selection

      let self = this;
      this.map.on(L.Draw.Event.CREATED, function (e) {  // when the user draws on the map

        var type = e.layerType
        var layer = e.layer;

        self.drawnItems.addLayer(layer);  //displaying rectangle
        resolve({ coordsRec: self.getDrawnItemsBbox() });
      });
    })
  }

  /**
   * Remove the display from the rectangle selection
   */
  removeRectangleSelection() { //remove rectangle from display
    return new Promise((resolve, reject) => {
      this.drawnItems.clearLayers()

      resolve({ status: "done" })
    })
  }

  /**
   * Get the bbox of the drawn items
   * @returns {Number[]} [ lngMin, latMin, lngMax, latMax ]
   */
  public getDrawnItemsBbox() {
    let bounds = this.drawnItems.getBounds();
    let NE = bounds._northEast;
    let SO = bounds._southWest;
    let result;
    if (NE && SO) {
      result = [SO.lng, SO.lat, NE.lng, NE.lat];
    }
    return result;
  }

  /**
  * allow to change underscror into space
  * @param str name of collection
  */
  public changeCaract(str) {
    return str.replace(/_/g, ' ');
  }

}
