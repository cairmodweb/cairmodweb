import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  // Event for catching change on collectionList
  @Output() collectionListChanged: EventEmitter<any> = new EventEmitter();

  constructor() { }

  /**
   * Emit an event when called
   * @param collectionList The collectionList that changed
   */
  public emitCollectionListChanged(collectionList) {
    this.collectionListChanged.emit(collectionList);
  }
  /**
   * Listener of changes on collectionList
   * catch with subscribe
   */
  public getCollectionListChanged() {
    return this.collectionListChanged;
  }
}
