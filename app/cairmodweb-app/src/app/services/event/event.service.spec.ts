import { TestBed } from '@angular/core/testing';

import { EventService } from './event.service';

describe('EventService', () => {
  let service: EventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should emit collection',()=>{
   spyOn(service.collectionListChanged, 'emit');
   service.emitCollectionListChanged(['couche','couche2']);
   expect(service.collectionListChanged.emit).toHaveBeenCalledWith(['couche','couche2']);

  })
  
  it('should emit collection',()=>{
    expect(service.getCollectionListChanged()).toEqual(service.collectionListChanged);
   })
});
