import { TestBed } from '@angular/core/testing';

import { ExportService } from './export.service';
import * as L from 'leaflet';
import { browser } from 'protractor';
import { fstat } from 'fs';

describe('ExportService', () => {
  let service: ExportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open CSV download window', () => {
    service.downloadCSV("layerTest")
    expect(service.layerToExport).toEqual("layerTest");
    expect(service.exportCsvAlertOpened).toBe(true);
  })

  it('should close CSV download window', () => {
    service.exportCsvAlertOpened = true;
    service.closeExportcsv("no");
    expect(service.exportCsvAlertOpened).toBe(false);
    service.exportCsvAlertOpened = true;
    service.closeExportcsv("yes");
    expect(service.exportCsvAlertOpened).toBe(false);
  })

  it('should close PDF export window', () => {
    service.exportWindowOpened = true;
    service.exportBackground = true;
    service.closeExportpdf("no");
    expect(service.exportWindowOpened).toBe(false);
    expect(service.exportBackground).toBe(false);
  })

  it('should open export window', () => {
    service.open();
    expect(service.exportWindowOpened).toBe(true);
    expect(service.mapTitle).toEqual('');
    expect(service.exportType).toEqual('p');
  })

  it('should set the export type', () => {
    service.choseType('p');
    expect(service.exportType).toEqual('p');
    service.choseType('pouet');
    expect(service.exportType).toEqual('pouet');
  })

  it('should chose the Background', () => {
    service.exportBackground = true
    service.choseBackground();
    expect(service.exportBackground).toBe(false);
    service.choseBackground();
    expect(service.exportBackground).toBe(true);
  })


  it('should download the csv file', (done) => { 
    const layer = {
      "bbox": [31.23625, 30.03211, 31.26015, 30.08097],
      "features": [{
        "geometry": {
          "coordinates": [465, 350],
          "type": "Point"},
      "properties": {
        "alt_Label_ala": "test",
        "date": 1965,"uri": "https://hdl.handle.net/20.500.11942/crtgq97fmsitj"
      },
      "type": "Feature"
    }],
      "type": "FeatureCollection"
    }
    const layerCSV = "geometry.type;geometry.coordinates;properties.alt_Label_ala;properties.date;properties.uri;type\n"
      + "Point;[465,350];test;1965;https://hdl.handle.net/20.500.11942/crtgq97fmsitj;Feature"

    var geojson = L.geoJson(layer);

    service.getCSV(geojson)
    .then((csv) => {
      expect(csv).toBe(layerCSV);
      done();
    });
  })

  it('should return visible layers',()=>{
    let marker1 = L.marker([30.2, 31.2]),
    marker2    = L.marker([30.3, 31.3]),
    marker3    = L.marker([30.4, 31.4]),
    marker4   = L.marker([30.5, 31.5]);
    let layer1 = L.layerGroup([marker1,marker2,marker3,marker4]);
    let layer2 =L.layerGroup([marker4]);
    layer1.visible = false;
    layer2.visible = false;
    service.mapService.queryLayers = [layer1,layer2];
    expect(service.getVisibleQueryLayers()).toEqual([]);
    let layer3 =L.layerGroup([marker3,marker2]);
    layer3.visible = true;
    service.mapService.queryLayers = [layer1,layer2,layer3];
    expect(service.getVisibleQueryLayers()).toEqual([layer3]);
  })


});
