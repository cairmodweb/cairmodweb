import { Injectable } from '@angular/core';
import { json2csvAsync } from 'json-2-csv';
import { saveAs } from 'file-saver';
import * as L from 'leaflet';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { MapService } from "src/app/services/map/map.service";

@Injectable({
  providedIn: 'root'
})
export class ExportService {
  public csv_delimiter = ";";
  public exportType;
  public mapTitle = '';
  public exportWindowOpened = false;
  public exportCsvAlertOpened = false;
  public layerToExport;
  public loading = false;
  public webSiteName = "CairmodWeb";
  public exportBackground = false;




  constructor(public mapService: MapService) {
  }


  //CSV EXPORT
  /**
  * Open CSV download window
  * @param layer Layer to transform in csv to download
  */
  public downloadCSV(layer) {
    this.layerToExport = layer;
    this.exportCsvAlertOpened = true;
  }


  /**
   * Open CSV Download window
   */
  public generateCSVDownloadLink() {
    var csv = "";
    this.getCSV(this.layerToExport).then((el) => {
      let blob = new Blob([el], { type: 'text/csv' });
      saveAs(blob, "export.csv");
    })

  }

  /**
   * Transform layer to download in csv in csv
   * @param layerToExport Layer to Download in csv
   */
  public async getCSV(layerToExport) {
    let layerGeoJson = layerToExport.toGeoJSON();
    if (layerGeoJson.type == "FeatureCollection") {
      let csv = await json2csvAsync(layerGeoJson.features,
        {
          delimiter: {
            field: this.csv_delimiter, // Comma field delimiter
          }
        }
      ).then(el => { return el })
      return (csv);
    }

  }

  /**
   * Closing export window
   * @param status "yes" or "no" depending on the answer
   */
  public closeExportcsv(status) {
    this.exportCsvAlertOpened = false;
    if (status == "yes") {
      this.generateCSVDownloadLink();
    }
  0}

  //PDF EXPORT

  /**
   * Get visible layers from the map
   * @returns an array of visible layers
   */
  getVisibleQueryLayers(){
    var queryLayers = this.mapService.queryLayers;
    var visibleQueryLayers = [];
    queryLayers.forEach(queryLayer => {
      if(queryLayer.visible){
        visibleQueryLayers.push(queryLayer);
      }
    });
    return visibleQueryLayers;
  }


  /**
   * Create download link to export map in pdf
   * @param data map html element
   */
  exportAsPDF(data) {
    html2canvas(data, {
      allowTaint: true,
      useCORS: true
    }).then(canvas => {
      var imgWidth = 198;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf(this.exportType, 'mm', 'a4'); // A4 size page of PDF
      var pdfWidth = pdf.internal.pageSize.getWidth();
      var pdfHeight = pdf.internal.pageSize.getHeight();


      // Writing title
      var yTitle = 20;
      var splitTitle = pdf.splitTextToSize(this.mapTitle, 180);
      pdf.setTextColor(148, 41, 0);
      pdf.setFontType('bold');
      for (var i = 0; i < splitTitle.length; i++) {
        // Managing Multiline title
        var textWidth = pdf.getStringUnitWidth(splitTitle[i]) * pdf.internal.getFontSize() / pdf.internal.scaleFactor;
        var textOffset = (pdf.internal.pageSize.width - textWidth) / 2;
        pdf.text(textOffset, yTitle, splitTitle[i]);
        yTitle += 7;
      }
      pdf.setTextColor(0, 0, 0);
      pdf.setFontType('normal');

      var ySources, heightContainerLegend, topContainerLegend, bottomContainerLegend;
      var mapTop = yTitle + 5;
      var bottomMap = mapTop + imgHeight;

      // If the map includes results, setting parameters of the legend
      var visibleQueryLayers = this.getVisibleQueryLayers();
      if (visibleQueryLayers.length > 0) {
        heightContainerLegend = 20 + visibleQueryLayers.length * 5;

        // Setting initial ySources according to the exportType
        if (this.exportType == "p") {
          topContainerLegend = bottomMap + 5;
          bottomContainerLegend = topContainerLegend + heightContainerLegend;
          ySources = bottomContainerLegend + 10;
        } else {
          topContainerLegend = bottomMap - heightContainerLegend;
          bottomContainerLegend = bottomMap;
          ySources = bottomMap + 10;
        }
      } else {
        // If there's no legend to display
        ySources = bottomMap + 10;
      }

      // Setting parameters according to the exportType
      var yMaxSources, maxImgWidth, leftContainerLegend;
      if (this.exportType == "p") {
        yMaxSources = 272;
        maxImgWidth = 180;
        leftContainerLegend = 10;
      } else {
        yMaxSources = 185;
        maxImgWidth = 220;
        leftContainerLegend = 240;
      }

      // If the map is too large or tall (sources must be included in the document), reducing it
      while (ySources > yMaxSources || imgWidth > maxImgWidth) {
        imgHeight *= 0.9;
        imgWidth *= 0.9;
        bottomMap = mapTop + imgHeight;

        if (heightContainerLegend) {
          // Si il y a une légende
          bottomMap = mapTop + imgHeight;

          if (this.exportType == "p") {
            // portrait exportType
            topContainerLegend = bottomMap + 5;
            bottomContainerLegend = topContainerLegend + heightContainerLegend;
            ySources = bottomContainerLegend + 10;
          } else {
            // landscape exportType
            topContainerLegend = bottomMap - heightContainerLegend;
            bottomContainerLegend = topContainerLegend + heightContainerLegend;
            ySources = bottomMap + 10;
          }
        } else {
          // If there's no legend
          ySources = bottomMap + 10;
        }
      }

      //  ----------------------
      // Ajouter le centrage de la carte
      //  ----------------------
      // Drawing the map
      var mapLeft;
      if (this.exportType == "p" || !heightContainerLegend) {
        // center the map
        mapLeft = (pdf.internal.pageSize.width - imgWidth) / 2;
      } else {
        // center the map
        mapLeft = (((leftContainerLegend - 20) - imgWidth) / 2) + 10;
      }

      pdf.addImage(contentDataURL, 'PNG', mapLeft, mapTop, imgWidth, imgHeight);

      // Only if we display a legend
      if (visibleQueryLayers.length > 0) {

        // Writing legend container
        var widthContainerLegend = 45;
        pdf.setFillColor(10, 10, 10, 0);
        pdf.rect(leftContainerLegend, topContainerLegend, widthContainerLegend, heightContainerLegend, "F");

        // Writing legend title
        var yTitleLegend = topContainerLegend + 8;
        pdf.setFontSize(13);
        pdf.text(leftContainerLegend + 5, yTitleLegend, "Légende");
        pdf.setFontSize(10);

        // Writing legend content
        for (let i = 0; i < visibleQueryLayers.length; i++) {
          // Color item result 
          var leftLegendItemCircle = leftContainerLegend + 10;
          var yLegendItemCircle = yTitleLegend + 8 + i * 5
          pdf.setFillColor(visibleQueryLayers[i].style.fillColor);
          pdf.circle(leftContainerLegend + 10, yLegendItemCircle, 1, 'FD');

          // Text item result 
          var xLegendItemText = leftLegendItemCircle + 5;
          var yLegendItemText = yLegendItemCircle + 1;
          pdf.setTextColor("#000000");
          pdf.text(xLegendItemText, yLegendItemText, visibleQueryLayers[i].name);
        }
      }

      // Writing sources
      pdf.setFontSize(10);
      pdf.text(15, ySources, 'Source : InVisu');
      pdf.text(15, ySources + 5,
        "Cette carte a été générée par le site " + this.webSiteName + ", site développé par des élèves de l'ENSG.");

      // Generated PDF 
      pdf.save(this.mapTitle.split(' ').join('_') + '.pdf');
      this.exportWindowOpened = false;
      this.loading = false;

      //Control buttons back on
      this.mapService.map._controlCorners.topleft.firstChild.hidden = false;
      this.mapService.map._controlCorners.topleft.lastChild.hidden = false;
      this.mapService.map._controlCorners.bottomright.firstChild.hidden = true; //Affichage Nord
    });
  }

  /**
     * Closing export window
     * @param status "yes" or "no" depending on the answer
     */
  public closeExportpdf(status) {
    if (status == "yes") {// Export Map
      this.loading = true;
      this.mapService.map._controlCorners.topleft.firstChild.hidden = true;
      this.mapService.map._controlCorners.topleft.lastChild.hidden = true;
      this.mapService.map._controlCorners.bottomright.firstChild.hidden = false;//Affichage Nord
      if (this.exportBackground) {
        //Remove Control Buttons
        this.initLayersToExport();
        setTimeout(() => {
          var data = document.getElementById('printable');
          this.exportAsPDF(data);
          this.resetLayers();
        }, 2000);
      } else {
        var data = document.getElementById('printable');
        this.exportAsPDF(data);
      }
    }
    else { //Cancel
      this.exportWindowOpened = false;
    }
    this.exportBackground = false;

  }

  /**
   * Opening export window
   */
  public open() {
    //Open ExportWindow
    this.exportWindowOpened = true;
    this.mapTitle = '';
    this.exportType = 'p';
  }

  /**
   * Set export type to landscape or portrait
   * @param value 'l' or 'p'
   */
  choseType(value) {
    this.exportType = value;
  }

  /**
   * Update export Parameters for background
   */
  choseBackground() {
    this.exportBackground = !this.exportBackground;
  }



  /**
   * Creates new map to export
   */
  initLayersToExport() {
    this.mapService.map.eachLayer((layer) => {
      //this.isQueryLayer(layer);
      if (layer instanceof L.TileLayer) {
        this.mapService.map.removeLayer(layer);
      }
    });
    let backgroundLayersList = [];
    this.mapService.backgroundLayerGroup.eachLayer((layer) => {
      backgroundLayersList.push(layer);
    })
    let n = backgroundLayersList.length;
    for (let i = 0; i < n; i++) {
      backgroundLayersList[n - i - 1].addTo(this.mapService.map);
    }

    this.mapService.initOSMBackground();
  }

  /**
   * Reset Map
   */
  resetLayers() {
    this.mapService.map.eachLayer((layer) => {
      if (layer instanceof L.TileLayer) {
        this.mapService.map.removeLayer(layer);
      }
    });
    this.mapService.initOSMBackground();
    this.mapService.backgroundLayerGroup.eachLayer((layer) => layer.addTo(this.mapService.map));
    this.mapService.backgroundLayerGroup.addTo(this.mapService.map);
  }


  //Control Buttons
  /**
   * Initialise Export to Pdf button
   */
  initialiseDownloadButton() {
    var customControl;
    customControl = L.Control.extend({
      options: {
        position: 'topleft'
      },

      onAdd: (map) => {
        var container = L.DomUtil.create('input', 'leaflet-bar leaflet-control leaflet-control-custom');
        container.type = "button";
        container.style.backgroundColor = 'white';
        container.style.backgroundImage = "url(assets/exportLogo.jpg)";
        container.style.backgroundSize = "27px 27px";
        container.style.width = '30px';
        container.style.height = '30px';

        container.onclick = () => {
          this.open();
        }
        return container;
      }
    });
    this.mapService.map.addControl(new customControl());

  }

  /**
   * Initialise Export to Pdf button
   */
  initialiseNorth() {
    var customControl;
    customControl = L.Control.extend({
      options: {
        position: 'bottomright'
      },

      onAdd: (map) => {
        var container = L.DomUtil.create('img', '');
        container.src = "assets/northArrow.jpg";
        container.style.width = '40px';
        container.style.height = '40px'

        return container;
      }
    });
    this.mapService.map.addControl(new customControl());

  }
}