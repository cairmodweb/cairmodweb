import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '../environments/environment';

// Components Import
import { HomeComponent } from './components/home/home.component';
import { VisualisationComponent } from './components/visualisation/visualisation.component';
import { InfoComponent } from './components/info/info.component';
import { ComparisonComponent } from './components/comparison/comparison.component';


let urls = environment.internalUrls;

export let appRoutes: Routes = [
  { path: urls.root.noSlash, redirectTo: urls.home.slash, pathMatch: 'full' },
  { path: urls.home.noSlash, component: HomeComponent },
  { path: urls.visu.noSlash, component: VisualisationComponent },
  { path: urls.info.noSlash, component: InfoComponent },
  { path: urls.comparison.noSlash, component: ComparisonComponent},
  { path: urls.all.noSlash, redirectTo: urls.home.slash }

]

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // True for debug purpose
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
