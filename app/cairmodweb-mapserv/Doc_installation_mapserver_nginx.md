# MapServer and Nginx

## Installation
### Install Mapserver

The following package must be installed to install mapserver with this command :
```bash
sudo apt-get install cgi-mapserver mapserver-bin mapserver-doc libmapscript-perl python-mapscript ruby-mapscript
```

### Nginx Configuration

#### Installation
* Nginx:
    * version: recommended 1.14.2
    * documentation: https://docs.nginx.com/nginx/
    * download link : https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/


#### Set up Nginx configuration (if not done)

1. You need to modify the default configuration of Nginx by placing or adding, in the default file from the nginx folder of the system at `/etc/nginx/sites-enabled/default`, the following lines with:
    ```
    # MapServer
    location ~ ^/mapws/([^/]+)/(.*)?$ {
            # Allow All origin
            add_header Access-Control-Allow-Origin *;

            # Fast CGI config
            fastcgi_pass                     127.0.0.1:9999;
            include                          fastcgi_params;
            fastcgi_param SCRIPT_FILENAME    /usr/bin/mapserv;
            fastcgi_param SCRIPT_NAME        /usr/bin/mapserv;
            fastcgi_param MS_MAPFILE         /var/www/maps/$1.map;
    }
    ```
    An example of a full nginx configuration is available in the folder `cairmodweb-deploy`: [cairmodweb-nginx.conf](../cairmodweb-deploy/cairmodweb-nginx.conf).
1. Verify the configuration file:
    ```bash
    sudo nginx -t
    ```
    If ok, then continue, else report the bug in the git projet.
1. Reload the nginx configuration:
    ```bash
    sudo /etc/init.d/nginx reload
    ```


#### Usefull Service commands
* To verify nginx status
    ```bash
    sudo systemctl status nginx
    ```
* To stop the nginx service :
    ```bash
    sudo systemctl stop nginx
    ```
* To start/to restart nginx service : 
Before running the following command, make sure that the port :9999 is free before starting the service, it will otherwise fail.
    ```bash
    sudo systemctl start nginx
    ```
To free the port :
* Get the id of the program on the port: 
    ```bash
    sudo netstat -lpn |grep :9999
    ```
* Kill the program : 
    ```
    sudo kill <id>
    ```

### Fast-CGI installation

1. Install spawn-fcgi if needed:
    ```bash
    sudo apt-get install spawn-fcgi
    ````
1. Place the file `mapserv` of `cairmodweb-mapser` in the folder `etc/init.d`:
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-mapserv/mapserv /etc/init.d/mapserv
    ```
1. Give persmission to all in the `/etc/init.d/mapserv` folder.
    ```bash
    sudo chmod +x /etc/init.d/mapserv
    ```

### Start MapServer
1. Start the MapServer service
    ```bash
    sudo /etc/init.d/mapserv start
    ````

 
### GDAL installation

GDAL is needed to tile up the images to provide to the MapServer. To install GDAL, use the following commands :

    sudo apt-get install gdal-bin
    sudo apt-get install libgdal-dev
    sudo apt-get install python-gdal
    
### Other installation
With pip, install pillow
```
pip install pillow
```

### Automatisation Raster mapserver

1. To automate the addition of pictures in mapserver you need to create several folders with these orders: 
    ```bash
    sudo mkdir /home/cmweb/data_admin
    sudo mkdir /home/cmweb/data_admin/img_mapserver
    ```
1. Then you need to place the mapfile `template_mapfile.map` of `cairmodweb-mapserv` in the folder `/home/cmweb/data_admin/`.
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-mapserv/template_mapfile.map /home/cmweb/data_admin/template_mapfile.map
    ```
1. You have to place the Python script `automatisation_raster.py` de `cairmodweb-mapserv` dans le dossier du serveur `/home/cmweb/data_admin/` therewith:
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-mapserv/automatisation_raster.py /home/cmweb/data_admin/automatisation_raster.py
    ```
1. Place the file `modif-folder-img.sh` of `cairmodweb-mapserv` at the root of your user folder:
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-mapserv/modif-folder-img.sh /home/cmweb/modif-folder-img.sh
    ```
1. Write un crontab in order to set up the automatic execution of the update script every 5 minutes: 
    ```bash
    sudo crontab -e
    ```
    The terminal editor opens, add the line:
    ```bash
    */5 * * * * /home/cmweb/modif-folder-img.sh 1 >> /home/cmweb/log-cron/log.txt 2 >> / home / cmweb / log-cron / error.txt
    ```



## How to use

It is now possible to query the WMS flux. The following link provides the available layers with the GetCapabilities: 
[http://217.160.173.154/mapws/background_map/?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities](http://217.160.173.154/mapws/background_map/?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities)