
import csv
import os 
import mapscript
from PIL import Image
Image.MAX_IMAGE_PIXELS = None

list_img_mapserv = [name for name in os.listdir('/var/www/maps/img/') if '.tif' in name]

def testimg(name):
    try:
        im = Image.open(name)
        im.verify()
        im.close()
        im = Image.open(name)	
        im.crop((1,2,2,3))
        im.close()
        return True
    except:
        return False
        

with open('/home/cmweb/data_admin/20200408_coll_cartes_web.csv', 'rb') as csv_file:
    csv_reader = csv.reader(csv_file,delimiter=',')
    line_count = 0
    mapfile = mapscript.mapObj('/home/cmweb/data_admin/template_mapfile.map')
    
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            filename = row[1] + '.tif'
            if os.path.exists('/home/cmweb/data_admin/img_mapserver/'+filename) & testimg('/home/cmweb/data_admin/img_mapserver/'+filename):
                if os.path.exists('/var/www/maps/img/'+filename):
                    if filename in list_img_mapserv:
                        list_img_mapserv.remove(filename)
                else:
                    os.system("cp /home/cmweb/data_admin/img_mapserver/"+filename+' '+ "/var/www/maps/img/"+ filename)
                    os.mkdir("/var/www/maps/img/"+ row[1])
                    os.system('gdal_retile.py -levels 3 -ps 512 512 -co "TILED=YES" -targetDir /var/www/maps/img/'+row[1] + ' ' + '/home/cmweb/data_admin/img_mapserver/'+filename)
                    os.system('gdaltindex /var/www/maps/img/'+row[1]+'_0.shp  /var/www/maps/img/'+row[1]+'*.tif')
                    os.system('gdaltindex /var/www/maps/img/'+row[1]+'_1.shp  /var/www/maps/img/'+row[1]+'/1/*.tif')
                    os.system('gdaltindex /var/www/maps/img/'+row[1]+'_2.shp  /var/www/maps/img/'+row[1]+'/2/*.tif')
                    os.system('gdaltindex /var/www/maps/img/'+row[1]+'_3.shp  /var/www/maps/img/'+row[1]+'/3/*.tif')
                    
                layer = mapscript.fromstring("""LAYER NAME """+'"'+row[1]+'"'+""" STATUS ON GROUP """+'"'+row[0]+'"'+""" SCALETOKEN NAME "%num%" VALUES "0" "3" "1000" "2" "5000" "1" "25000" "0" END END TILEINDEX """+'"'+row[1]+"""_%num%.shp" TYPE RASTER END""")
                mapfile.insertLayer(layer)
                mapfile.save('tempo_map.map')
                os.system("sudo rm -rf /home/cmweb/data_admin/img_mapserver/"+filename)
            else:
                if os.path.exists('/var/www/maps/img/'+filename):
                    if filename in list_img_mapserv:
                        list_img_mapserv.remove(filename)
                    layer = mapscript.fromstring("""LAYER NAME """+'"'+row[1]+'"'+""" STATUS ON GROUP """+'"'+row[0]+'"'+""" SCALETOKEN NAME "%num%" VALUES "0" "3" "1000" "2" "5000" "1" "25000" "0" END END TILEINDEX """+'"'+row[1]+"""_%num%.shp" TYPE RASTER END""")
                    mapfile.insertLayer(layer)
                    mapfile.save('tempo_map.map')



os.system("cp /home/cmweb/data_admin/tempo_map.map /var/www/maps/background_map.map")


for name_img in list_img_mapserv:
    value = name_img.split('.')
    os.system("find /var/www/maps/img/ -name '"+value[0]+ "*' -delete")
    os.system("sudo rm -rf /var/www/maps/img/"+value[0]+"/")