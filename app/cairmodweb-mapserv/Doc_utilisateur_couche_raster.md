# Add and remove raster in Mapserver

### General use

The MapServer is host on the server. It is autmatically updated when a change has been detected in the folder `/home/cmweb/data_admin/`(scanned every 5 minutes). This folder contains :
- a csv who lists the collections and their images: `20200408_coll_cartes_web.csv`. 
- a folder with all the images: `img_mapserver`


### Listing file

The file `20200408_coll_cartes_web.csv` must respect some rules to be valid:
- One line for each images
- The format for a line is: `collection_name,image_name`

**Be carefull:** accents are not allowed in collection or image name.


### Update the server

The csv file can be modified directly or a new one can be pasted to replace it, notably with the command:
```bash
sudo scp <path to your file>20200408_coll_cartes_web.csv cmweb@217.160.173.154:/home/cmweb/data_admin/20200408_coll_cartes_web.csv
```

The images in the folder `img_mapserver` has to be kept updated.


Add a image:
```bash
sudo scp <path to your files>*.tif cmweb@217.160.173.154:/home/cmweb/data_admin/img_mapserver/
```

Remove a image:
```bash
cd /home/cmweb/data_admin/img_mapserver
rm img_to_remove
```
 
