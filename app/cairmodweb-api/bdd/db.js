/* Import of MongoDB module and client */
var MongoClient = require('mongodb').MongoClient;

/* Definition of our object and thus its constructor */
/**
 * Constructor of DB object
 */
function DB() {
    this.client = null;
    this.db = null;
}

/* Definition of DB prototype functions */

/**
 * Create the connection to the mongoDB server
 * @param {string} uri Url of the mongoDB server
 * @param {string} db Name of the actual database to connect to. Default is "admin".
 * @returns {Promise} 
 */
DB.prototype.connect = function (uri, db = 'admin') {
    let _this = this;

    return new Promise((resolve, reject) => {

        if (_this.client) {
            resolve({ message: "Already connected" });
        }
        else {
            const client = new MongoClient(uri, { useUnifiedTopology: true })
            client.connect()
                .then((database) => {
                    _this.client = database;
                    _this.db = database.db(db);

                    console.log("Connected succesfully to MongoDB database " + db + "!")

                    resolve({Success: "Successfully Connected"});
                })
                .catch(error => {
                    // console.log("Error connecting: " + error.message);

                    reject({
                        Exception: {
                          code: 500,
                          locator: "Database - connect()",
                          message: error
                        }
                      });
                })
        }
    })
}

/**
 * Close the connection to the mongoDB server
 */
DB.prototype.close = function () {
    if (this.client) {
        this.client.close()
            .then(() => {
                console.log("Successfully closed database")
            })
            .catch((error) => {
                console.log("Failed to close the database: " + error.message)
            })
    }
}

/**
 * Get all documents of the given collection
 * @param {string} coll Name of the collection
 * @returns {Promise}
 */
DB.prototype.findAll = function (coll) {
    let _this = this;

    return new Promise((resolve, reject) => {
        _this.db.collection(coll, (error, collection) => {

            if (error) {
                console.log("Could not access collection " + coll + ": " + error.message);
                reject({
                    Exception: {
                      code: 500,
                      locator: "Database - findAll()",
                      message: error
                    }
                  });
            }
            else {
                // Query to database
                collection.find()
                    .toArray((err, result) => {
                        if (err) {
                            throw err;
                        }
                        // console.log(result);
                        resolve(result);
                    });
            }
        })

    })
}

/**
 * Get all documents of the collection with date between start_date and stop_date
 * @param {string} coll Name of the collection
 * @param {number} start_date 
 * @param {number} stop_date 
 * @returns {Promise}
 */
DB.prototype.findBetween = function (coll, start_date, stop_date) {
    let _this = this;

    return new Promise((resolve, reject) => {
        _this.db.collection(coll, (error, collection) => {

            if (error) {
                console.log("Could not access collection " + coll + ": " + error.message);
                reject({
                    Exception: {
                      code: 500,
                      locator: "Database - findBetween()",
                      message: error
                    }
                  });
            }
            else {
                /* Query writting
                    We want :  
                        start_date < date < stop_date
                        || start_date < date_fourchette_debut < stop_date
                        || start_date < date_fourchette_fin < stop_date
                 */
                let query = {
                    $or: [
                        { $and: [{ date: { $gte: start_date } }, { date: { $lte: stop_date } }] },
                        { $and: [{ date_fourchette_debut: { $gte: start_date } }, { date_fourchette_debut: { $lte: stop_date } }] },
                        { $and: [{ date_fourchette_fin: { $gte: start_date } }, { date_fourchette_fin: { $lte: stop_date } }] }
                    ]
                };

                // Query to database
                collection.find(query).toArray((err, result) => {

                    if (err) {
                        throw err;
                    }

                    // console.log(result);
                    resolve(result);
                });
            }
        })
    })
}



// Make the module available for use in other files
module.exports = DB;