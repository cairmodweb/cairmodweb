// Import the dependencies for testing
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../server');// Configure chai

chai.use(chaiHttp);
chai.should();

describe("ServeurRoutes", () => {
    describe("GET /ping", () => {
        it("should return pong", (done) => {
             chai.request(app)
                 .get('/ping')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     res.body["ping"].should.equal("pong");
                     done();
                  });
         });    
    });

    describe("GET /",() => {
        it("should return the banner",(done) => {
            chai.request(app)
                .get('/')
                .end((err,res)=>{
                    res.body["banner"].should.equal("CairmodWeb API - surcouche de la VM cairmod");
                    done();
                })

        })
    })

    describe("Error",() => {
        it("should return an error",(done) => {
            chai.request(app)
                .get('/errortest')
                .end((err,res)=>{
                    res.body["exception"].should.equal("bad getaway");
                    done();
                })

        })
    })
});

