let chai = require('chai');
let chaiHttp = require('chai-http');

let app = require('../server');
let rewire = require('rewire');
let api = rewire('../routes/api.js');
let fetch = require('node-fetch')

const sinon = require('sinon');
const SinonChai = require('sinon-chai');
chai.use(SinonChai);
chai.use(chaiHttp);
chai.should()


//Remove function console.log
TESTING = true;
old_console_log = console.log;
console.log = function () {
    if (!TESTING) {
        old_console_log.apply(this, arguments);
    }
}

//Import API functions
APIcomputeCircleRadius = api.__get__('computeCircleRadius');
APIcomputeCircleCenter = api.__get__('computeCircleCenter');
APIparseCoordinates = api.__get__('parseCoordinates');
APIrectangleToCircleQuery = api.__get__('rectangleToCircleQuery');
APIcleanJsonGeometrySelection = api.__get__('cleanJsonGeometrySelection');
APIqueryKeywords = api.__get__('queryKeywords');
APIcomputeQueryURL = api.__get__('computeQueryURL');
APIqueryCoordinates = api.__get__('queryCoordinates');
APIqueryKeywordCoordinates = api.__get__('queryKeywordCoordinates');
APImakeQuery = api.__get__('makeQuery');
APIaddDateField = api.__get__('addDateField');
APIisInDateResult = api.__get__('isInDateResult');
APIcomputeBbox = api.__get__('computeBbox');
APIaddPerseeIdField = api.__get__(' addPerseeIdField');
APIURLencoding = api.__get__('URLencoding');

describe("API", () => {

    var data = {
        "bbox": [31.28455,30.07662,31.32932,30.09871],
        "features": [
            {
                "geometry": {
                    "coordinates": [31.28455, 30.07662],
                    "type": "Point"
                }
            },
            {
                "geometry": {
                    "coordinates": [31.32932,30.09871],
                    "type": "Point"
                }
            },
            {
                "geometry": {
                    "coordinates": [31.285, 30.08],
                    "type": "Point"
                }
            }
        ]
    };

    describe("APIqueryKeywords", () => {
        it("the result of is a queryKeywords json", (done) => {
            sinon.stub(fetch, "Promise").onFirstCall().returns(Promise.resolve({ "status": '200', json: () => { return data } }))

            APIqueryKeywords("http://217.160.75.212/app/search/42?", "palais").then(function (data) {
                data.should.be.a('object');
                done();
            }); 
        });

        afterEach(() => {
            sinon.restore();
        });
    });



    describe("APIqueryCoordinate", () => {
        it("the result of queryCoordinate is a json", (done) => {
            sinon.stub(fetch, "Promise").onFirstCall().returns(Promise.resolve({ "status": '200', json: () => { return data } }))

            APIqueryCoordinates("http://217.160.75.212/app/search/42?",[31.284, 30.076, 31.286, 30.09]).then(function (data) {
                data.should.be.a('object');
                done();
            }); 
        });

        afterEach(() => {
            sinon.restore();
        });
    });

    describe("APIqueryKeywordCoordinates", () => {
        it("the result of queryKeywordCoordinates is a json", (done) => {
            sinon.stub(fetch, "Promise").onFirstCall().returns(Promise.resolve({ "status": '200', json: () => { return data } }))

            APIqueryKeywordCoordinates("http://217.160.75.212/app/search/42?","palais",[31.284, 30.076, 31.286, 30.09]).then(function (data) {
                data.should.be.a('object');
                done();
            }); 
        });

        afterEach(() => {
            sinon.restore();
        });
    });
    
    describe("computeCircleRadius", () => {
        it("should return the correct radius", (done) => {
            APIcomputeCircleRadius(0, 5, 0, 17).should.be.closeTo(90 * 8.86, 0.01);
            done();
        });
    });

    describe("computeCircleCenter", () => {
        it("should return the correct center", (done) => {
            APIcomputeCircleCenter(0, 5, 0, 17)[0].should.be.closeTo(5 - (5 - 0) / 2, 0.01);
            APIcomputeCircleCenter(0, 5, 0, 17)[1].should.be.closeTo(17 - (17 - 0) / 2, 0.01);
            done();
        });
    });
    
    describe("computeQueryURL", () => {
        it("should return a good format of request", (done) => {
            APIcomputeQueryURL("request?", ["palais"], [5, 0, 17, 0]).should.be.eql("request?keyword=palais&" + APIrectangleToCircleQuery([5, 0, 17, 0]));
            done();
        });
    });

    describe("parseCoordinates", () => {
        it("should return the correct array from the parse", (done) => {
            APIparseCoordinates("[0,5,0,17]").should.be.eql(["0", "5", "0", "17"]);
            done();
        });
    });

    describe("rectangleToCircleQuery", () => {
        it("should return the correct string for request", (done) => {
            APIrectangleToCircleQuery([5, 0, 17, 0]).should.be.equal("lon=" + APIcomputeCircleCenter(0, 0, 5, 17)[1] + "&lat=" + APIcomputeCircleCenter(0, 0, 5, 17)[0] + "&radius=" + APIcomputeCircleRadius(0, 0, 5, 17));
            done();
        });
    });

    describe("is in DateResult",()=>{
        it("should return if the feature is in dateresult", (done) => {
            let feature = {
                "geometry": {
                    "coordinates": [31.28455, 30.07662],
                    "type": "Point"
                },
                "properties": {
                    "id" : 145
                }
            };
            let dateResult = [{'id_opentheso':2},{'id_opentheso':145}];
            let dateResult2 = [{'id_opentheso':2},{'id_opentheso':147}];
            APIisInDateResult(feature,dateResult).isIn.should.be.equal(true);
            APIisInDateResult(feature,dateResult2).isIn.should.be.equal(false);
            done();
        
        });
    });

    describe("computeBbox",()=>{
        it("should return the bbox", (done) => {
            let feature1 = {
                "geometry": {
                    "coordinates": [31.28455, 30.07662],
                    "type": "Point"
                }
            };
            let feature2 = {
                "geometry": {
                    "coordinates": [31.3, 30.1],
                    "type": "Point"
                }
            };
            let feature3 = {
                "geometry": {
                    "coordinates": [30.5, 30.0],
                    "type": "Point"
                }
            }
            let features = [feature1,feature2,feature3];
            let bbox = APIcomputeBbox(features);
            bbox[0].should.be.equal(30.5);
            bbox[1].should.be.equal(30.0);
            bbox[2].should.be.equal(31.3);
            bbox[3].should.be.equal(30.1);
            done();
    })

    })

    describe("URLEncoding", () => {
        it("should change url encoding", (done) => {
            APIURLencoding('testé').should.be.eql('test%C3%A9');
            APIURLencoding('testè').should.be.eql('test%C3%A8');
            APIURLencoding('testê').should.be.eql('test%C3%AA');
            APIURLencoding('testë').should.be.eql('test%C3%AB');
            APIURLencoding('testà').should.be.eql('test%C3%A0');
            APIURLencoding('testâ').should.be.eql('test%C3%A2');
            APIURLencoding('testï').should.be.eql('test%C3%AF');
            APIURLencoding('testî').should.be.eql('test%C3%AE');
            APIURLencoding('testù').should.be.eql('test%C3%B9');
            APIURLencoding('testû').should.be.eql('test%C3%BB');
            APIURLencoding('testç').should.be.eql('test%C3%A7');
            APIURLencoding('testô').should.be.eql('test%C3%B4');
            APIURLencoding('téstô').should.be.eql('t%C3%A9st%C3%B4');
            done();
        });
    });

    describe("addDateField",()=>{
        let dateInfo = {};
        dateInfo.date = 1850;
        dateInfo.note_date = "Construction";

        let feature = {
            "geometry": {
                "coordinates": [31.28455, 30.07662],
                "type": "Point"
            },
            "properties": {
                "test" : "test"
            }
        }

        APIaddDateField(feature,dateInfo);
        feature.properties.date[0].date.should.be.equal(1850);
        feature.properties.date[0].note_date.should.be.equal("Construction");

        let dateInfo2 = {};
        dateInfo2.date = 1852;
        dateInfo2.note_date = "Destruction";

        APIaddDateField(feature,dateInfo2);
        feature.properties.date[1].date.should.be.equal(1852);
        feature.properties.date[1].note_date.should.be.equal("Destruction");
        
        let dateInfoFourchette = {};
        dateInfoFourchette.date_fourchette_debut = 1840;
        dateInfoFourchette.date_fourchette_fin = 1890;
        dateInfoFourchette.note_date = "Construction";
        APIaddDateField(feature,dateInfoFourchette);
        feature.properties.date[2].date_fourchette_debut.should.be.equal(1840);
        feature.properties.date[2].date_fourchette_fin.should.be.equal(1890);
        feature.properties.date[2].note_date.should.be.equal("Construction");

    });

    describe("cleanJsonGeometrySelection", () => {
        it("should return a json with only object in the selection", (done) => {
            let cleanData = {
                "bbox": [31.28455,30.07662,31.32932,30.09871],
                "features": [
                    {
                        "geometry": {
                            "coordinates": [31.28455, 30.07662],
                            "type": "Point"
                        }
                    },
                    {
                        "geometry": {
                            "coordinates": [31.285, 30.08],
                            "type": "Point"
                        }
                    }
                ]
            }
            APIcleanJsonGeometrySelection(data, [31.284, 30.076, 31.286, 30.09]).should.be.eql(cleanData);
            done();
        });
    });

    describe("APImakeQuery", () => {
        it("should call the good query in case of parameters given", (done) => {
            sinon.stub(fetch, "Promise").returns(Promise.resolve({ "status": '200', json: () => { return data } }))

            // When only request in param
            APImakeQuery("http://217.160.75.212/app/search/42?").then(function (data) {
                data.should.be.a('object');
            }); 

            // When only request and keyword in param
            APImakeQuery("http://217.160.75.212/app/search/42?", "palais").then(function (data){
                data.should.be.a('object');
                APIqueryKeywords("http://217.160.75.212/app/search/42?", "palais").then((data2) => {
                    data2.should.be.equal(data);
                });
            });

            // When only request and coordsRec in param
            APImakeQuery("http://217.160.75.212/app/search/42?", null, "[31.284,30.076,31.286,30.09]").then(function (data){
                data.should.be.a('object');
                APIqueryCoordinates("http://217.160.75.212/app/search/42?", "[31.284,30.076,31.286,30.09]").then((data2) => {
                    data2.should.be.equal(data);
                });
            });

            // When request, keyword and coordsRec in param
            APImakeQuery("http://217.160.75.212/app/search/42?", "palais", "[31.284,30.076,31.286,30.09]").then(function (data){
                data.should.be.a('object');
                APIqueryKeywords("http://217.160.75.212/app/search/42?", "palais", APIparseCoordinates("[31.284,30.076,31.286,30.09]")).then((data2) => {
                    data2.should.be.equal(data); 
                });
            });

            // When only startdate and enddate are set
            APImakeQuery("http://217.160.75.212/app/search/42?", null, null, "18500312", "18600412")
            .catch(err => { 
                err.Exception.code.should.be.equal(500);
                err.Exception.locator.should.be.equal("makeQuery");
                err.Exception.message.should.be.equal("Query not implemented");
            });
            

            // When only enddate is set
            APImakeQuery("http://217.160.75.212/app/search/42?", null, null, null, "18600412")
            .catch(err => { 
                err.Exception.code.should.be.equal(500);
                err.Exception.locator.should.be.equal("makeQuery");
                err.Exception.message.should.be.equal("Query not implemented");
            });
            

            // When only startdate is set
            APImakeQuery("http://217.160.75.212/app/search/42?", null, null, "18600412", null)
            .catch(err => { 
                err.Exception.code.should.be.equal(500);
                err.Exception.locator.should.be.equal("makeQuery");
                err.Exception.message.should.be.equal("Query not implemented");
            });

            done();     
        });

        afterEach(() => {
            sinon.restore();
        });
    });
})




describe("APIRoutes", () => {
    describe("GET /search keyword", () => {
        it("should return an object", (done) => {
            chai.request(app)
                .get('/api/search/42?keyword=rectorat')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    })
});
