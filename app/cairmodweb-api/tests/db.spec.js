const DB = require('../bdd/db');
var MongoMock = require('mongomock');

let chai = require('chai');
chai.should()

//initial mock data
var allMonuments = [{"_id":{"$oid":"5e86e6e4afc6a55b0df07860"},"id_opentheso":3243,"nom":"Hôtel Continental","date":1869},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07861"},"id_opentheso":3243,"nom":"Continental Hotel","date":1857,"note_date":"publication de la photographie"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07862"},"id_opentheso":3243,"nom":"Hôtel continental","date":1868,"note_date":"publication de la photographie"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07863"},"id_opentheso":3243,"nom":"Hôtel continental","date":1870,"note_date":"publication de la photographie"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07864"},"id_opentheso":3243,"nom":"Hôtel continental","date":1870,"note_date":"publication de la photographie"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07868"},"id_opentheso":3243,"nom":"Hôtel continental","date_fourchette_debut":1850,"date_fourchette_fin":1890,"note_date":"construction"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07867"},"id_opentheso":3243,"nom":"Hôtel continental","date_fourchette_debut":1820,"date_fourchette_fin":1890,"note_date":"construction"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07869"},"id_opentheso":3243,"nom":"Hôtel continental","date_fourchette_debut":1820,"date_fourchette_fin":1850,"note_date":"construction"},
{"_id":{"$oid":"5e86e6e4afc6a55b0df07865"},"id_opentheso":3243,"nom":"Hôtel continental / place de l'opéra","date":1912,"note_date":"publication de la photographie"}
]
var db = {
    monuments: allMonuments}

var mongo = new MongoMock(db);

database = new DB;
database.db = mongo;


describe("Database", () => {
    it("should return all monuments", (done) => {
        database.findAll('monuments').then(data => {
            data.length.should.be.equal(allMonuments.length);
            for (let i=0; i<allMonuments.length;i++){
                data[i].should.be.equal(allMonuments[i]);
            }
            done(); 
        });              
    });

    it("should return all monuments within two dates",(done)=>{
        let dataWithinDates = [{"_id":{"$oid":"5e86e6e4afc6a55b0df07860"},"id_opentheso":3243,"nom":"Hôtel Continental","date":1869},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07861"},"id_opentheso":3243,"nom":"Continental Hotel","date":1857,"note_date":"publication de la photographie"},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07862"},"id_opentheso":3243,"nom":"Hôtel continental","date":1868,"note_date":"publication de la photographie"},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07863"},"id_opentheso":3243,"nom":"Hôtel continental","date":1870,"note_date":"publication de la photographie"},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07864"},"id_opentheso":3243,"nom":"Hôtel continental","date":1870,"note_date":"publication de la photographie"},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07868"},"id_opentheso":3243,"nom":"Hôtel continental","date_fourchette_debut":1850,"date_fourchette_fin":1890,"note_date":"construction"},
        {"_id":{"$oid":"5e86e6e4afc6a55b0df07869"},"id_opentheso":3243,"nom":"Hôtel continental","date_fourchette_debut":1820,"date_fourchette_fin":1850,"note_date":"construction"},
        ]
        // If there is data
        database.findBetween('monuments',1830,1875).then(data => {
            data.length.should.be.equal(dataWithinDates.length);
            for (let i=0; i<dataWithinDates.length;i++){
                data[i]._id.$oid.should.be.equal(dataWithinDates[i]._id.$oid);
            }

            // If there is no data
            database.findBetween('monuments',1710,1750).then(data=> {
                data.length.should.be.equal(0)
                done();});
        })

    })

});

