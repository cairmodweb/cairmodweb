// Get dependencies
const express = require('express');
const http = require('http');
const swaggerUi = require('swagger-ui-express');
var path = require('path');



// Get our API routes
const api = require('./routes/api');
// Get our openAPISpecs json
const apiDocs = require('./public/openapi.json');
// Get our dbtest routes
const dbtest = require('./routes/dbtest');

// Definition of the Express app
const app = express();

let bannerText = "CairmodWeb API - surcouche de la VM cairmod";

/**  
 * API routes 
 */
// Set our PING routes
app.get('/ping', (req, res) => {
  res.json({
    ping: "pong"
  });
})

// Set our api routes
app.use('/api', api);


// Set our doc routes
app.use('/doc',express.static(__dirname + '/routes/api-doc/site'));

app.get('/doc', function (req, res){
    res.sendFile(__dirname + '/routes/api-doc/site/index.html');
});

// Set our openAPI description routes
app.use('/apispecs', swaggerUi.serve, swaggerUi.setup(apiDocs));

// Test db
app.use('/dbtest', dbtest);

// Catch all other routes and return the index file
app.get('/', (req, res) => {
  res.json({
    banner: bannerText
  });
});

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.json({
    exception: "bad getaway"
  });
});


/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);


/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));

module.exports = app;
