// This is the general config of the API.

let config = {
    urls: {
        cairmodVM: 'http://217.160.75.212',
        uriDB: 'mongodb://root:root@localhost:27017',
    },
    database: {
        dbName: 'cairmodweb',
        collectionName: 'monuments',
    },
    degkmconversion: 90
}

module.exports = config;