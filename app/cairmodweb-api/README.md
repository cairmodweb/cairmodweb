# The API of the Cairmodweb project

## Prerequisites

* NodeJS:
    * version: recommended 12.16
    * documentation: https://nodejs.org/en/docs/
    * download: https://nodejs.org/en/download/
* Npm:
    * version: recommended 6.13
    * documentation: https://docs.npmjs.com
    * download: comes with NodeJS
* MkDocs:
    * version: version 1.0.4 or higher
    * documentation : https://www.mkdocs.org/
    * download : 'sudo apt-get install -y mkdocs'


## Start the API
```bash
npm install
npm start
```

You api is running on http://localhost:3000/.


## Configure the API

The API has a [config file](./config.js) in which are defined some global parameters such as:
* the cairmodVM address
* the mongo database adress
* the database parameters (name, collection, etc.)
* the parameter to convert degrees to kilometers for cartographic transformations

The parameters defined are those for our development, do not forget to adapt them to yours.


## Test the API

Pour tester l'API, il suffit de lancer la commande :
```bash
npm install
npm test
```
Les résultats sont accessibles directement dans le terminal, avec un récapitulatif de la couverture de code.