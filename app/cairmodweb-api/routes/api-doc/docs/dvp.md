# Développement
Pour lancer le service en local, il faut lancer à la fois la base de données et le serveur Node.

## Prérequis
### Installations nécessaires
Les installations suivantes sont nécessaires sur le serveur avant de déployer l'API. 

Pour l'API en elle-même :

- NodeJS:
    - version: recommandée 12.16
    - documentation: [https://nodejs.org/en/docs/](https://nodejs.org/en/docs/)
    - téléchargement : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- Npm:
    - version: recommandée 6.13
    - documentation: [https://docs.npmjs.com](https://docs.npmjs.com)
    - téléchargement: comes with NodeJS
- MkDocs:
    - version: version 1.0.4 ou plus
    - documentation : [https://www.mkdocs.org/](https://www.mkdocs.org/)
    - téléchargement : `sudo apt-get install -y mkdocs`

Pour la base de données :

- Docker:
    - version: recommandée 19.03
    - documentation: [https://www.docker.com/](https://www.docker.com/)
- Docker-compose:
    - version: recommandée 1.25
    - documentation: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
    - téléchargement : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Lancer l'API en local
### Base de données
L'image Docker de MongoDB est utilisée. Après s'être assuré que la base de données n'est pas déjà présente, la commande[^1] suivante permet de la mettre en place.
```
cd ./app/cairmodweb-db
docker-compose up
```

### Serveur NodeJs
Le serveur NodeJS peut se lancer avec la commande suivante :
```
cd ./app/cairmodweb-api
npm install
npm start
```

Le service sera alors disponible à l'adresse [http://localhost:3000](http://localhost:3000). 

### Modifier les données de la base de données
Pour mettre à jour la base de données, il suffit de modifier les données du fichier ```/app/cairmodweb-db/mongo-init/init_csv.csv``` avant de lancer les commandes suivantes.
```
cd ./app/cairmodweb-db
docker-compose down
docker-compose up
```
Le service sera alors interrompu pendant quelques instants. 

## Accéder à la base de données
Une fois la base de données mise en place avec docker-compose, il est possible de consulter manuellement les données qui y sont enregistrées. 

- Il existe un interface administrateur où il est possible de consulter les données : [http://localhost:8081](http://localhost:8081)
- Il est également possible d'y accéder par le conteneur docker. Les commandes suivantes sont à lancer :
    ```
    docker container exec -it cairmodweb-db_mongo_1 bash
    ```
    Puis dans le bash du conteneur : 
    ```
    mongo -u root -p root
    ```
    Il est alors possible de requêter la base de données. Les commandes possibles sont alors détaillées [ici](https://docs.mongodb.com/manual/reference/method/).

[^1]: Les chemins exprimés dans toutes les commandes de cette page sont relatifs par rapport à la racine du projet git.


## Mise à jour de l'API

### Modifier l'API

Toutes modifications du code de l'API seront effectives lors de son prochain lancement.


### Configurer l'API et la base de données

#### Configurer l'API

La configuration de l'API peut être mise à jour dans le fichier `config.js` de `cairmodweb-api`. 

Il définit des paramètres globaux de l'API tels que :

- Les urls externes : `urls`
    - l'adresse de la VM CAIRMOD : `cairmodVM`
    - l'adresse de la base de données Mongo : `uriDB`
- Les paramètres de la base de données : `database`
    - le nom de la base de données : `dbName`
    - le nom de la collection : `monuments`
- Le paramètre de conversion de coordonnées entre degrés et kilomètres : `degkmconversion`

**Attention :** Les paramètres de la base de données sont présents dans le fichier de configuration mais ne sont pas encore pris en compte en tant que variable globale dans le code. Il faut donc mettre à jour ces paramètres dans le code en dur du dossier `routes`:

- Dans le fichier `api.js` : dans la fonction `getIdFromDBWithDate()`(lignes 546-548).
- Dans le fichier `dbtest.js` : à chaque appel de la fonction `database.connect()` (lignes 17-31-55) puis dans le `.then()`qui la suit.

#### Configurer la base de données

La base de données est gérée par le biais d'un docker-compose dont l'ensemble des fichiers de paramètres se trouvent dans `cairmodweb-db`. Ainsi, les paramètres modifiables sont :

- Le nom de la base de données : 
    1. dans le fichier `docker-compose.yml`, modifier la variable `MONGO_INITDB_DATABASE`
    1. dans le fichier `00_init-mongo.js` du dossier `mongo-init`, modifier à la ligne 24 la valeur de la variable `db`.
    1. dans le fichier `01_import-csv-mongo.sh` du dossier `mongo-init`, modifier à la ligne 8 la variable juste après le flag `-d`.
    1. dans le fichier `02_create-index-mongo.js` du dossier `mongo-init`, modifier à la ligne 2 la variable prise par la fonction `db.getSiblingDB()`.
    1. **NB :** le nom de la base de données est à mettre à jour aussi dans l'API.
- Le nom de la collection :
    1. dans le fichier `00_init-mongo.js` du dossier `mongo-init`, modifier à la ligne 33 la variable prise par la fonction `db.createCollection()`.
    1. dans le fichier `01_import-csv-mongo.sh` du dossier `mongo-init`, modifier à la ligne 8 la variable juste après le flag `-c`.
    1. **NB :** le nom de la collection est à mettre à jour aussi dans l'API.


### Mettre à jour la documentation

La documentation que vous êtes en train de lire est générée en utilisant [MkDocs](https://github.com/mkdocs/mkdocs/). 

Elle est rédigée en Markdown. Vous pouvez la modifier dans le dossier `routes/api-doc` de `cairmodweb-api`. Sa modification sera effective au prochain démarage de l'API localement (cf [partie Déploiement](../deploiement) ou en déploiement (cf [Lancer l'API en local](./#lancer-lapi-en-local)).

Vous pouvez visualiser en temps réel les changements réalisés sur la documentation avec la commande suivante lancée depuis la racine de `cairmodweb-api` :
```bash
cd ./routes/api-doc/
mkdocs serve
```
Le site est visible à l'adresse [http://localhost:8000](http://localhost:8000)


## Tester l'API

Pour tester l'API, il suffit de lancer la commande depuis `cairmodweb-api` :
```bash
npm install
npm test
```
Les résultats sont accessibles directement dans le terminal, avec un récapitulatif de la couverture de code.