# Déploiement

L'API peut être déployé sur un serveur. La procédure décrite ici reprend le déploiement du projet CairmodWeb entier (API, base de données et web-application).

## Prérequis
### Installations nécessaires
Les installations suivantes sont nécessaires sur le serveur avant de déployer l'API. 

Pour l'API en elle-même :

- NodeJS:
    - version: recommandée 12.16
    - documentation: [https://nodejs.org/en/docs/](https://nodejs.org/en/docs/)
    - téléchargement : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- Npm:
    - version: recommandée 6.13
    - documentation: [https://docs.npmjs.com](https://docs.npmjs.com)
    - téléchargement: comes with NodeJS
- MkDocs:
    - version: version 1.0.4 ou plus
    - documentation : [https://www.mkdocs.org/](https://www.mkdocs.org/)
    - téléchargement : `sudo apt-get install -y mkdocs`

Pour la base de données :

- Docker:
    - version: recommandée 19.03
    - documentation: [https://www.docker.com/](https://www.docker.com/)
- Docker-compose:
    - version: recommandée 1.25
    - documentation: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
    - téléchargement : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

### Autres dépendances
Avant de lancer les services, il faut :

- placer le projet git dans `/home/cmweb/`
- ajouter un fichier `date.csv` dans `/home/cmweb/data_admin/data_csv/` respectant la structure définie [ici](maj.md#fichier-de-mise-à-jour).

## Services
Les services suivants ont été créés :

- `cairmodweb-api.service ` : Il démarre et maintient le serveur Node utilisé pour l'API.
- `cairmodweb-database.service` : Il permet de lancer au démarrage la base de données.
- `cairmodweb-db-update.service` : Il démarre et maintient le serveur Node permettant la mise à jour automatique de la base de données.

Ces trois services doivent être placés dans le dossier `/etc/systemd/system` du serveur et doivent être lancés : 
```bash
sudo systemctl start cairmodweb-database.service
sudo systemctl start cairmodweb-api.service
sudo systemctl start cairmodweb-db-update.service
```
```bash
sudo systemctl enable cairmodweb-database.service
sudo systemctl enable cairmodweb-api.service
sudo systemctl enable cairmodweb-db-update.service
```

Leur status peut être vérifié avec la commande suivante :
```bash
sudo systemctl status service_name.service
```

