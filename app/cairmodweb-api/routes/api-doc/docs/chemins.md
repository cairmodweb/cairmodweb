# Points d'accès
Ces différents chemins ont été définis dans le cadre de l'API. Ces chemins ont été définis de telle sorte à ressembler le plus possible à ceux de la VM Cairmod. 

## Chemins de base
### /api
Le chemin `/api` permet d'accèder aux spécifications des points d'accès de cette application conformément à [OpenAPI](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md).

La réponse est un JSON mais un éditeur est utilisé pour le rendre plus lisible.

### /
Le chemin de base `/` renvoit la bannière de l'explication
```
{"banner":"CairmodWeb API - surcouche de la VM cairmod"}
```


### /ping
Le chemin `/ping` permet de vérifier la connexion à l'API. Il renvoie à chaque instance la réponse suivante :
```
{"ping":"pong"}
```

### /doc
Le chemin `/doc` (le chemin actif) contient toute la documentation liée au site. Il renvoie un site statique de documentation.

## Requêter des objets
### /api/search/{thid}?[directory={value[,value]}]&[keyword={value[,value]}]&[coordsRec=[value,value,value,value]]&[stop=value]&[start=value]

Ce chemin correspond à la recherche des objets géo-localisés qui répondent au·x critère·s de recherche :

- `directory`: la valeur passée est recherchée dans `skos:prefLabel` des `skos:Concept` qui n'ont pas de géométrie. La valeur particulière root récupère tous les objets géo-localisés et les sauvegarde en cache sous `cache/{idg}_{th}.geojson`. La valeur peut être une liste de chaîne de caractères séparée par une `,`. **Attention :** si la valeur `root` est donnée pour ce paramètre, les autres paramètres ne seront pas pris en compte.
- `keyword` : la valeur passée est recherchée dans `skos:prefLabel` et `skos:altLabel` des `skos:Concept` qui ont une géométrie.
- `start` : la valeur de date à partir de la laquelle les données sont requêtées.
- `stop` : la valeur de date avant laquelle les données sont requêtées.
- `coordsRec` : coordonnées définissant un rectangle de recherche géographique.

Le format de la réponse est défini [ci dessous](#réponse-des-chemins-/search).

### /api/search/{thid}?directory=root
Ce chemin renvoit l'intégralité des objets requêtables. 

Le format de la réponse est défini [ci dessous](#réponse-des-chemins-/search).


## Réponse des chemins /search
La réponse est un GeoJSON : 
```json
{ 
    "type": "FeatureCollection",
    "bbox": [,,,],
    "features": [
        ...
    ]
}
```
L'emprise des données (`bbox`) est calculée à partir des données trouvées. Les objets géo-localisés sont dans la liste `features`. Chaque objet est alors retourné ainsi :
```json
{"geometry":
    {
    "coordinates": [,],
    "type":"Point"
    }
    ,
"properties":
    {"altLabel_ala":"",
    "altLabel_ar":"",
    "altLabel_en":"",
    "altLabel_fr":"",
    "altLabel_iso":"",
    "altLabel_mul":"",
    "broadMatch":"",
    "created":"",
    "description_ala":"",
    "description_ar":"",
    "description_en":"",
    "description_fr":"",
    "description_iso":"",
    "description_mul":"",
    "editorialNote_ala":"",
    "editorialNote_ar":"",
    "editorialNote_en":"",
    "editorialNote_fr":"",
    "editorialNote_iso":"",
    "editorialNote_mul":"",
    "exactMatch":"",
    "historyNote_ala":"",
    "historyNote_ar":"",
    "historyNote_en":"",
    "historyNote_fr":"",
    "historyNote_iso":"",
    "historyNote_mul":"",
    "id":"",
    "isA":"",
    "label_ala":"",
    "label_ar":"",
    "label_en":"",
    "label_fr":"",
    "label_iso":"",
    "label_mul":"",
    "modified":"",
    "note_ala":"",
    "note_ar":"",
    "note_en":"",
    "note_fr":"",
    "note_iso":"",
    "note_mul":"",
    "related":"",
    "relatedMatch":"",
    "scopeNote_ala":"",
    "scopeNote_ar":"",
    "scopeNote_en":"",
    "scopeNote_fr":"",
    "scopeNote_iso":"",
    "scopeNote_mul":"",
    "uri":"",
    "perseeid":""},
"type":"Feature"}
```
Plus d'informations sur ce retour sont accessibles par le chemin /api.