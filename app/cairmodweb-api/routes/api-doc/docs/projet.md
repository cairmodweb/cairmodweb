# Le projet

## Contexte du projet CairmodWeb
Cette API a été développée dans le cadre du projet CAIRMOD de mars à avril 2020. 

Le projet CairmodWeb prend place au sein du projet CAIRMOD piloté par [InVisu](https://invisu.cnrs.fr/), unité de service et de recherche de l'Institut National de l'Histoire de l'Art (INHA), et du Centre National de la Recherche Scientifique (CNRS). 

Le projet est porté en partenariat avec l'UMS Persée, qui développe et met en œuvre des corpus numériques de recherche, ainsi que l'Institut Français d'Archéologie Orientale (IFAO), et l'École Nationale des Sciences Géographiques (ENSG), qui propose des projets en lien avec CAIRMOD aux élèves de différentes promotions.

## Précisions sur le projet CAIRMOD [^1]
Le projet global, nommé CAIRMOD (le Caire Moderne), est lauréat de la première vague d’appel à projet CollEx Persée. Lancé en novembre 2018, il s’intéresse à la documentation des albums iconographiques (photos, coupures de presse,
photocopies, etc.) actuellement consultables sur Gallica, la plateforme de la Bibliothèque nationale de France (BnF), après avoir été intégralement numérisés. Ces albums rendent compte de l’architecture moderne de la ville du Caire en
Égypte et sont mis à disposition sur la plateforme Athar sous la forme d’un corpus iconographique de cinq albums :

- 1 album de photographies sur les transformations urbaines du Caire entre 1849 et 1946
- 3 albums d’impressions photomécaniques découpées dans des livres ou périodiques
- 1 album de photographies de bâtiments de la ville du Caire

Ainsi, le principal enjeu du projet CAIRMOD est de pouvoir rendre compte de la mobilité du tissu urbain cairote aux XIXe et XXe siècles en géo-visualisant et en spatialisant l’ensemble des images et leurs métadonnées.

## Auteurs
L'API a été développée par quatre élèves de TSI (deux provenant de l’option C-Programmeur et du cursus Ingénieur de l’ENSG et deux provenant de l’option G-Architecte et de la formation Géomètre puis M1 Informatique de l’ENSG et de l’UGE) sous l'encadrement technique et pédagogique de Didier Richard (IGN/ENSG). 

Auteurs: 

* Alexis Coatsaliou
* Julie Grosmaire
* Antoine Laurendon
* Fanny Vignolles

## Commanditaires

Les commanditaires du projet sont :

- Julie Erismann, chargée de projet CAIRMOD pour InVisu
- Bulle Leonetti, cheffe de projet CAIRMOD pour InVisu
- Pierre Monier, développeur/informaticien pour InVisu
- Hélène Bégnis - Coordination du projet pour Persée - Chargée des partenariats recherche

[^1]: Pour plus d'informations : [site de InVisu](https://invisu.cnrs.fr/project/geo-visualisation-de-contenus-de-la-perseide-athar-le-cas-du-caire-moderne/)

![](img/cnrs.svg){: style="width:90px"} ![](img/invisu.png){: style="width:100px"}![](img/collex-persee.png){: style="width:150px"} ![](img/persee.png){: style="width:100px"} ![](img/cairmod.png){: style="width:100px"} ![](img/ensg.png){: style="width:90px"}