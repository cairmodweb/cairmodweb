# Mise à jour des données
Les données utilisées sont issues des sources suivantes :
- le thésaurus en ligne sous [OpenTheso des Humanités numériques](https://opentheso.huma-num.fr/opentheso/) via une Machine Virtuelle 
- une base de données spécifiques à l'API répertoriant les dates correspondantes aux objets du thésaurus.

## Objets
Les objets sont directement requêtés sur le Thésaurus et la mise à jour est donc faîte lors de la mise à jour du Thésaurus.

## Dates
### Fonctionnement général
La base de données est hébergée sur le serveur courant et est automatiquement mise à jour à partir de ce csv. Ce csv est présent dans le dossier `/home/cmweb/data_admin/data_csv/` et porte le nom de `date.csv`. C'est ce csv qu'il faut modifier pour déclencher une mise à jour.

### Fichier de mise à jour
Le fichier .csv doit répondre aux critères suivants pour entraîner une mise à jour :

- Le délimiteur utilisé est la virgule.
- L'entête comprend les champs suivants dans cet ordre : `id_opentheso`, `nom`, `date`, `date_fourchette_debut`, `date_fourchette_fin`, `note_date`.
- Chaque ligne correspond à une date ou à un intervalle de date (date fourchette). Ainsi l'`id_opentheso` n'est pas forcément unique. Un objet peut être présent autant de fois qu'il présente une date d'intérêt.
- Si les champs de `date_fourchette_debut` et `date_fourchette_fin` fonctionnent ensemble. Si l'un est renseigné l'autre doit l'être aussi.
- Le champs `date` et l'ensemble `date_fourchette_debut`-`date_fourchette_fin` ne peuvent pas être simultanément remplis. Sinon, seule la première date sera prise en compte.


### Mettre à jour sur le serveur
Le fichier csv peut être mis à jour directement ou un nouveau csv peut être copié pour le remplacer, notamment grâce à la commande suivante :
```bash
scp <pathtofile>file.csv user@server-ip:/home/cmweb/data_admin/date_csv/date.csv
```

La mise à jour des données est automatiquement déclenchée au changement du csv et peut entraîner une coupure de service de l'ordre de la minute. 