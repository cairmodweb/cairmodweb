# Documentation API surcouche CairmodWeb

## Introduction
Cette API permet de requêter le thésaurus en ligne sous [OpenTheso des Humanités numériques](https://opentheso.huma-num.fr/opentheso/) via une VM. Elle permet notamment de requêter les objets en fonction de mots-clés, de zones géographiques ou de dates.

Cette application a été développée comme une surcouche de la VM Cairmod afin d'y ajouter de nouvelles fonctionnalités.

## Structure
L'API est composée de deux éléments :

- une API surcouche permettant de faire les calculs et de requêter avec un serveur Nodejs
- une base de données NoSQL MongoDB 

![](img/structure.png)

### Base de données : Structure et fonctionnement
La base de données est une base de données SQL qui enregistre toutes les informations de dates liées aux différents objets du Thesaurus. Cette base de données est utilisée en raison de l'absence d'information de date exploitable dans le Thesaurus. 

La base de données contient donc : 

- une note décrivant l'événement
- une date ou un intervalle de temps correspondant à l'événement. 
- l'objet associé (id_opentheso et nom) à cet événement

Toutes ces informations étant récupérées dans un csv  [^3].

La base de données fonctionne grâce aux éléments suivants (présents dans /app/cairmodweb-db) : 

- le `fichier docker-compose.yml` qui utilise deux images docker (la base de données mongo [^4] et le client de mongo-db pour le web mongo-express [^5]). Le lancement de ce fichier permet de mettre en place les différents conteneurs nécessaires directement.
- le script `00_init-mongo.js` qui permet de créer l'utilisateur, la base de données en elle-même et la collection contenant les informations nécessaires
- le script `01_import-csv-mongo.sh` qui importe le csv.
- le script `02_create-index-mongo.js` permettant de créer des index afin d'accélérer la recherche dans la base de données.

### API : Structure et fonctionnement
L'API utilise ExpressJS qui définit différents chemins. Ces chemins sont définis [ici](./chemins.md).

[^1]: Il est nécessaire d'avoir téléchargé mkdocs avant de lancer cette commande
[^2]: Les chemins exprimés sont relatifs par rapport à la racine du projet git
[^3]: Plus d'informations peuvent être obtenues dans [l'explication du csv associé](./maj.md#fichier-de-mise-à-jour). 
[^4]: mongoDB : https://hub.docker.com/_/mongo
[^5]: mongo-express : https://hub.docker.com/_/mongo-express