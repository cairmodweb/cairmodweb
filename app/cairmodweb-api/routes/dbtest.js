const express = require('express');
const router = express.Router();

// Import of our db class
const DB = require('../bdd/db');

// Test if routes works
router.get('/', (req, res) => {
    res.send('dbtest works');
});

// Test connect
router.get('/connect', (req, res) => {
    console.log("\n/connect");

    var database = new DB;
    database.connect('mongodb://root:root@localhost:27017', 'cairmodweb')
        .then((result) => {
            console.log(result);
            database.close();
            res.json(result);
        })
        .catch(error => res.json(error));
})

// Test findAll
router.get('/findAll', (req, res) => {
    console.log("\n/findAll");

    var database = new DB;
    database.connect('mongodb://root:root@localhost:27017', 'cairmodweb')
        .then(() => {
            return database.findAll('monuments')
        })
        .then((result) => {
            console.log(result);
            database.close();
            res.json(result);
        })
        .catch(error => res.json(error))
})

// Test findBetween
router.get('/findBetween/:start_date/:stop_date', (req, res) => {
    console.log("\n/findBetween/:start_date/:stop_date");

    console.log('params:', req.params)
    // Params needs to be cast as integers
    let start_date = parseInt(req.params.start_date);
    let stop_date = parseInt(req.params.stop_date);
    console.log("start_date, stop_date:", start_date, stop_date);


    var database = new DB;
    database.connect('mongodb://root:root@localhost:27017', 'cairmodweb')
        .then(() => {
            return database.findBetween('monuments', start_date, stop_date)
        })
        .then((result) => {
            // console.log(Object.keys(result).length);
            database.close();
            res.json(result);
        })
        .catch(error => res.json(error))
})


module.exports = router;