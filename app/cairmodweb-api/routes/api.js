const express = require('express');
const router = express.Router();
fetch = require('node-fetch');
// Import of the config file
const config = require('../config.js');
// Import of our db class
const DB = require('../bdd/db');


// Definition of the Cairmod.ensg.eu VM address
const cairmodVM = config.urls.cairmodVM;
const uriDB = config.urls.uriDB;
const dbName = config.database.dbName;
const collectionName = config.database.collectionName;
const degkmconversion = config.degkmconversion;

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});


/* GET api listing. */
router.get('/search/:thid', (req, res) => {
  console.log("\n /search/:thid")
  console.log('params:', req.params)
  console.log('query:', req.query)
  // Setting headers to allow requests from the website
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');


  // Get params
  let params = req.params;
  let query = req.query;
  let thid = params.thid;
  let directory = query.directory;
  let keywords = query.keyword;
  let coordsRec = query.coordsRec;
  let start = parseInt(query.start);
  let stop = parseInt(query.stop);
  console.log("queryParams:", directory, keywords, coordsRec, start, stop)

  // Create base url to request cairmodVM
  let request = cairmodVM + "/app/search/" + thid + "?directory=" + directory + "&";

  // Do the query.ies to cairmodVM and manage their results
  makeQuery(thid, request, keywords, coordsRec, start, stop)
    .then(result => {

      // We add the perseeid to the result
      addPerseeIdField(result, thid)
        .then(perseeResult => {
          res.json(perseeResult)
        })
    })
    .catch(error => {
      res.json(error)
    });


});



/* --- QUERY FUNCTIONS --- */

/**
 * Filter the queries to balance them in their own functions
 * @param {String} thid The thesaurus id
 * @param {String} request base URL whithout query params
 * @param {Array[String]} keywords 
 * @param {String} coordsRec 
 * @param {String} start 
 * @param {String} stop 
 * @returns {Promise} the JSON resulting from the search or an JSON error
 */
function makeQuery(thid, request, keywords, coordsRec, start, stop) {
  console.log("\nmakeQuery function");

  // If none of the params are defined, returns the basic request with directory
  if (!keywords && !coordsRec && !start && !stop) {
    console.log("request all:", request);
    return fetch(request).then(result => result.json())
  }
  else {
    console.log("request: ", request);
    let promises = [];

    // Make the good query in function of given params

    if (start && stop && !coordsRec && !keywords) {
      return queryDate(thid, start, stop);
    }
    else if (start && stop && keywords && !coordsRec) {
      return queryDateCombineTo(start, stop, queryKeywords(request, keywords));
    }
    else if (start && stop && coordsRec && !keywords) {
      return queryDateCombineTo(start, stop, queryCoordinates(request, parseCoordinates(coordsRec)));
    }
    else if (start && stop && coordsRec && keywords) {
      return queryDateCombineTo(start, stop, queryKeywordCoordinates(request, keywords, parseCoordinates(coordsRec)));
    }
    else if (keywords && !coordsRec) {
      return queryKeywords(request, keywords);
    }
    else if (coordsRec && !keywords) {
      return queryCoordinates(request, parseCoordinates(coordsRec));
    }
    else if (coordsRec && keywords) {
      return queryKeywordCoordinates(request, keywords, parseCoordinates(coordsRec))
    }
    else {
      return new Promise((resolve, reject) => {
        reject({
          Exception: {
            code: 500,
            locator: "makeQuery",
            message: "Query not implemented"
          }
        })
      })
    }
  }
}

/**
 * Compute the URL of the request
 * @TODO date !
 * @param {String} request base URL whithout query params
 * @param {Array[String]} keywords 
 * @param {Array[float]} coordsRec 
 * @param {String} start 
 * @param {String} stop 
 * @return {String} full URL to query
 */
function computeQueryURL(request, keywords, coordsRecArray, start, stop) {
  if (keywords) {
    console.log(keywords);
    request += "keyword=" + keywords;
    console.log(request);
    if (coordsRecArray) {
      request += "&" + rectangleToCircleQuery(coordsRecArray);
    }
  }
  else {
    if (coordsRecArray) {
      request += rectangleToCircleQuery(coordsRecArray);
    }
  }

  return (URLencoding(request));

}


/**
 * The Query for keyword(s)
 * @todo Mulptiple keywords
 * @param {String} request base URL base URL whithout query params
 * @param {Array[String]} keywords 
 * @returns {Promise} JSON whith the result of the query
 */
function queryKeywords(request, keywords) {
  console.log("queryKeywords function");

  // Construction of the url request for cairmod VM
  request = computeQueryURL(request, keywords, undefined, undefined, undefined);

  // Request
  return fetch(request)
    .then(result => result.json())

}

/**
 * The query for coordinates
 * @param {String} request base URL base URL whithout query params
 * @param {Array[float]} coordsRecArray 
 * @return {Json} correct json
 */
function queryCoordinates(request, coordsRecArray) {
  console.log("queryCoordinates function");

  request = computeQueryURL(request, undefined, coordsRecArray, undefined, undefined);

  //Request
  return fetch(request)
    .then(result => result.json()).then(function (data) {
      return (cleanJsonGeometrySelection(data, coordsRecArray));
    })
}

/**
 * The query for coordinates and keywords
 * @param {String} request base URL base URL whithout query params
 * @param {Array[String]} keywords 
 * @param {Array[float]} coordsRecArray 
 * @return {Json} correct json
 */
function queryKeywordCoordinates(request, keywords, coordsRecArray) {
  console.log("queryKeywords+Coordinates function");

  // Construction of the url request for cairmod VM
  request = computeQueryURL(request, keywords, coordsRecArray, undefined, undefined);

  //Fetch data
  return fetch(request)
    .then(result => result.json()).then(function (data) {
      return (cleanJsonGeometrySelection(data, coordsRecArray));
    })
}


/**
 * Query for date
 * @param {number} start 
 * @param {number} stop 
 * @todo NOT VERIFIED, MAY BE NOT WORKING
 */
function queryDate(thid, start, stop) {
  console.log("queryDate function");

  // Preparing promise queries for keyword and id with this date range
  let datePromise = getIdFromDBWithDate(start, stop);
  let request = cairmodVM + "/app/feature/" + thid + "/";

  return new Promise((resolve, reject) => {
    datePromise.then(dateResult => {
      // Initialisation of variables
      let objectPromises = [];
      let resultFeatures = [];
      let idSeen = [];  // Follow the ids that have already been seen

      // Make all request to cairmodVM for the features needed in result
      dateResult.forEach(el => {
        // The request is made only once
        if (!idSeen.includes(el.id_opentheso)) {
          let objectRequest = request + el.id_opentheso;
          objectPromises.push(
            fetch(objectRequest)
              .then(result => result.json())
          );
          idSeen.push(el.id_opentheso);
        }
      })


      // Resolving all promises of fetch object at once
      Promise.all(objectPromises)
        .then(resultPromises => {
          // Initialisation counter for comportement managing
          // DON'T REMOVE - USEFULL FOR DEBUG
          let counter_promises = 0;
          let counter_success = 0;

          // For each object, 
          // Verify is in dateResult
          // Update its dateInfo field
          // Add it to the result
          resultPromises.forEach(resultPromise => {
            if (!resultPromise.Exception) {

              let isInResult = isInDateResult(resultPromise, dateResult);
              if (isInResult.isIn) {
                let updateFeature = isInResult.feature;
                resultFeatures.push(updateFeature);
                counter_success++;
              }
            } else {
              // Indicator of comportement
              // DON'T REMOVE - USEFULL FOR DEBUG
              // console.log("error position:", counter_promises)
              // console.log("error on id_opentheso:", idSeen[counter_promises])
            }
            counter_promises++
          })

          // Indicator of comportement
          // DON'T REMOVE - USEFULL FOR DEBUG
          // console.log("dateInfo count:", dateResult.length)
          // console.log("countResult: " + counter_success + "/" + counter_promises);
          // console.log("nbError:", counter_promises - counter_success);
          // console.log("idSeen: ", idSeen);
          // console.log("idSeen sorted", idSeen.sort((a,b)=>a-b));

          // Create our result
          let requestResult = {
            bbox: computeBbox(resultFeatures),
            features: resultFeatures,
            type: "FeatureCollection"
          }

          resolve(requestResult);
        })
        .catch(error => reject(error));
    })
      .catch(error => {
        reject(error);
      })
  });
}

/**
 * Query for date combine to another query
 * @param {number} start 
 * @param {number} stop 
 * @param {Promesse} queryPromise 
 */
function queryDateCombineTo(start, stop, queryPromise) {
  console.log("queryDateCombineTo otherQuery function");

  // Preparing promise queries for keyword and id with this date range
  let datePromise = getIdFromDBWithDate(start, stop);
  // let keywordsPromise = queryKeywords(request, keywords);

  let queryPromises = [datePromise, queryPromise];

  return new Promise((resolve, reject) => {
    // Resolving all promises at once
    Promise.all(queryPromises)
      .then((resultPromises) => {
        // Get Result from promises
        let dateResult = resultPromises[0];
        let otherQueriesResult = resultPromises[1];
        let otherQueriesFeatures = otherQueriesResult.features;

        let validFeatures = [];

        // Sort in function of date
        otherQueriesFeatures.forEach(feature => {
          let isInResult = isInDateResult(feature, dateResult);
          if (isInResult.isIn) {
            let updateFeature = isInResult.feature;
            validFeatures.push(updateFeature);
          }
        })

        // Update our result
        let requestResult = {
          bbox: computeBbox(validFeatures),
          features: validFeatures,
          type: "FeatureCollection"
        }

        resolve(requestResult);
      })
      .catch(error => {
        reject(error);
      })
  })
}


/* --- OTHER FUNCTIONS --- */


/**
 * Add the field perseeid to properties of all features in the result
 * @param {JSON} result 
 * @param {Integer} thid 
 * @returns {Promise} the JSON with the new fiel perseeid for all features possible
 */
function addPerseeIdField(result, thid) {
  console.log("addPerseeIDField function")

  // Initialisation of promises array
  let promises = []

  // One promise for each feature to retrieve its perseeid
  result.features.forEach(feature => {
    let request = cairmodVM + "/app/perseeids/" + thid + "/" + feature.properties.id;

    let promise = fetch(request)
      .then(res => {
        return res.json()
      })
      .then(res => {
        resFeature = res;
        return { perseeid: resFeature[0].identifier };
      })
      .catch(error => {
        errorFeature = error;
        // If error, then perseeid is undefined
        return { perseeid: undefined };
      })

    // Add a specific feature promise to the promises array
    promises.push(promise)
  });


  return new Promise((resolve, reject) => {
    // Resolving all promises at once
    Promise.all(promises)
      .then(promises => {
        // For each feature, add the perseeid as a field in its properties
        for (i = 0; i < result.features.length; i++) {
          result.features[i].properties.perseeid = promises[i].perseeid;
        }

        // Resolve the Promise
        resolve(result);
      })
      .catch(errors => {
        // For each feature, add the perseeid as a field in its properties
        for (i = 0; i < result.features.length; i++) {
          result.features[i].properties.perseeid = promises[i].perseeid;
        }

        // Resolve the promise even if errors because undefined is accepted
        resolve(result);
      })
  })
}



/* --- CoordinatesQuery related functions --- */

/**
 * Clean json data to make sure each object is within the selection
 * @param {json} data 
 * @param {Array[float]} coordsRecArray coordinates of the geometry selection
 */
function cleanJsonGeometrySelection(data, coordsRecArray) {
  console.log("\ncleanJsonGeometrySelection function");
  let selectedFeatures = [];
  for (let i = 0; i < data.features.length; i++) {
    let lat = data.features[i].geometry.coordinates[0];
    let long = data.features[i].geometry.coordinates[1];
    if (coordsRecArray[0] <= lat && coordsRecArray[2] >= lat && coordsRecArray[1] <= long && coordsRecArray[3] >= long) {
      selectedFeatures.push(data.features[i]);
    }
  }

  data.features = selectedFeatures;
  return data;
}

/**
 * Compute center and radius of the circumscribing circle and teturns the standardized parameters for coordinates query
 * @param {Array[float]} coordsRecArray coordinates of the rectangle
 */
function rectangleToCircleQuery(coordsRecArray) {
  let center = computeCircleCenter(coordsRecArray[1], coordsRecArray[3], coordsRecArray[0], coordsRecArray[2]);
  let radius = computeCircleRadius(coordsRecArray[1], coordsRecArray[3], coordsRecArray[0], coordsRecArray[2]);
  return ("lon=" + center[1] + "&lat=" + center[0] + "&radius=" + radius);
}


/**
 * Transforms string coordsRec parameters send into an array
 * @param {String} coordsRec coordinates to parse
 */
function parseCoordinates(coordsRec) {
  return (coordsRec.slice(1, -1).split(","));
}

/**
 * Returns the radius of the circle
 * @param {float} xmin minimum abscissa
 * @param {float} xmax maximum abscissa
 * @param {float} ymin minimum ordinate
 * @param {float} ymax maximum ordinate
 */
function computeCircleRadius(xmin, xmax, ymin, ymax) {
  let Lx = xmax - xmin;
  let Ly = ymax - ymin;
  let diag = Math.pow(Math.pow(Lx, 2) + Math.pow(Ly, 2), 0.5);
  return (degkmconversion * diag / 2);
}

/**
 * Returns the center of the circle
 * @param {float} xmin minimum abscissa
 * @param {float} xmax maximum abscissa
 * @param {float} ymin minimum ordinate
 * @param {float} ymax maximum ordinate
 */
function computeCircleCenter(xmin, xmax, ymin, ymax) {
  return [xmax - (xmax - xmin) / 2, ymax - (ymax - ymin) / 2];
}





/* --- DateQuery Related functions --- */


/**
 * Verify if feature id is matching the id of a dateResult element
 * @param {Object} feature 
 * @param {Object[]} dateResult 
 * @returns {Object} { isIn: boolean, feature: Object }
 */
function isInDateResult(feature, dateResult) {
  // console.log(feature)
  let isIn = false;

  dateResult.forEach(el => {
    if (feature.properties.id == el.id_opentheso) {
      isIn = true;
      feature = addDateField(feature, el);
    }
  })

  return { isIn: isIn, feature: feature };
}

/**
 * Add the field date to the given feature with dateInfo
 * @param {Object} feature 
 * @param {Object} dateInfo 
 * @returns {Object} The updated feature
 */
function addDateField(feature, dateInfo) {
  if (!feature.properties.date) {
    feature.properties.date = []
  }

  if (dateInfo.date) {
    feature.properties.date.push({
      note_date: dateInfo.note_date,
      date: dateInfo.date
    })
  } else {
    feature.properties.date.push({
      note_date: dateInfo.note_date,
      date_fourchette_debut: dateInfo.date_fourchette_debut,
      date_fourchette_fin: dateInfo.date_fourchette_fin
    })
  }

  return feature;
}

/**
 * Get Ids of object whith date between start and stop
 * @param {number} start 
 * @param {number} stop 
 */
function getIdFromDBWithDate(start, stop) {
  var database = new DB;
  return new Promise((resolve, reject) => {
    database.connect('mongodb://root:root@localhost:27017', 'cairmodweb')
      .then(() => {
        return database.findBetween('monuments', start, stop)
      })
      .then((result) => {
        database.close();
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      })
  })
}

/**
 * Compute Bbox of the given array of features
 * @param {Object[]} features 
 * @returns {float[]}
 */
function computeBbox(features) {
  // Initialisation
  let lngMin = features[0].geometry.coordinates[0];
  let latMin = features[0].geometry.coordinates[1];
  let lngMax = features[0].geometry.coordinates[0];
  let latMax = features[0].geometry.coordinates[1];

  // Look for min and max
  features.forEach(feature => {
    lngMin = Math.min(lngMin, feature.geometry.coordinates[0]);
    latMin = Math.min(latMin, feature.geometry.coordinates[1]);

    lngMax = Math.max(lngMax, feature.geometry.coordinates[0]);
    latMax = Math.max(latMax, feature.geometry.coordinates[1]);
  });

  return [lngMin, latMin, lngMax, latMax];
}

/**
 * Apply URL encoding to the query
 * @param {String} texte text to encode
 */
function URLencoding(texte){
  let URLencoding = "";
  URLencoding = texte.split('é').join('%C3%A9'); //é
  URLencoding = URLencoding.split('è').join('%C3%A8'); //è
  URLencoding = URLencoding.split('ê').join('%C3%AA'); //ê
  URLencoding = URLencoding.split('ë').join('%C3%AB'); //ë
  URLencoding = URLencoding.split('à').join('%C3%A0'); //à
  URLencoding = URLencoding.split('â').join('%C3%A2'); //â
  URLencoding = URLencoding.split('ï').join('%C3%AF'); //ï
  URLencoding = URLencoding.split('î').join('%C3%AE'); //î
  URLencoding = URLencoding.split('ù').join('%C3%B9'); //ù
  URLencoding = URLencoding.split('û').join('%C3%BB'); //û
  URLencoding = URLencoding.split('ç').join('%C3%A7'); //ç
  URLencoding = URLencoding.split('ô').join('%C3%B4'); //ô
  return URLencoding;
}


module.exports = {
  computeCircleRadius,
  computeCircleCenter,
  parseCoordinates,
  rectangleToCircleQuery,
  cleanJsonGeometrySelection,
  queryKeywords,

};
module.exports = router;
