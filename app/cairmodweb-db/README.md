# The Database for the Cairmodweb project


## 1 - How to install
The database is install through the service. 

## 2 - Update the database
To update the database, change the .csv in the `/home/cmweb/data_admin/date_csv` folder. 

```
scp <path_to_file>/date.csv cmweb@217.160.173.154:/home/cmweb/data_admin/date_csv/date.csv
```

## 3 - Composition

### 3.1 - Docker-compose file : [docker-compose.yml](./docker-compose.yml)
Two images:
1. `mongo`: the mongoDB database https://hub.docker.com/_/mongo
1. `mongo-express`: mongoDB client for web https://hub.docker.com/_/mongo-express

### 3.2 - Initialisation scripts : [mongo-init folder](./mongo-init/)

The initialisation scripts are placed in the docker-entrypoint-initdb.d folder when the docker volume of mongo is mounted.
Thoose files are evaluated by mongo at the initialisation of the mongoDB server. Only the *.js and *.sh files are treated and in alphabetic order.

#### 3.2.1 - [00_init-mongo.js](./mongo-init/00_init-mongo.js)

**Goal :** 
* Create a _CairmodWeb_ user, owner of the _cairmodweb_ database
* Create the _cairmodweb_ database
* Create the _monuments_ collection in _cairmodweb_ database


#### 3.2.2 - [01_import-csv-mongo.sh](./mongo-init/01_import-csv-mongo.sh)

**Goal:** 
* Import the csv file _init_csv.csv_ to fill the _monuments_ collection.

**Precision:** The module [mongodb-import](https://docs.mongodb.com/manual/reference/program/mongoimport/) is used with parameters:
* `--type csv`: type of the file to import
* `-d cairmodweb`: name of the target database 
* `-c monuments`: name of the target collection
* `--drop`: ask to drop the database before importing
* `--ignoreBlanks`: ignores fields with a null value in csv imports 
* `--headerline`: using with csv, specifies that the first line has to be used as field names
* `--columnsHaveTypes`: instructs that the type of each field is specified in the csv headerline
* `--parseGrace=skipRow`:  specifies to skip rows containing a value whose type does not match the expected type
* `./docker-entrypoint-initdb.d/init_csv.csv`: file to import


#### 3.2.3 - [02_create-index-mongo.js](./mongo-init/02_create-index-mongo.js)

**Goal:** 
* Create indexes on fields: `date`, `date_fourchette_debut`, `date_fourchette_fin`


## 4 - Commands for the database


### 4.1 - Access the database
* Your mongoDB is listening on port 27017 and can be access by a db driver with : mongodb://localhost:27017.
* The mongo-express server gives you an admin plateform to manage your database on : http://localhost:8081.
* You can access mongo container while running, with the following command in a new terminal:
    ```bash
    docker container exec -it cairmodweb-db_mongo_1 bash
    ```
    And the mongo shell with the command `mongo -u root -p root`. See commands in the following section 2.3.


### 4.2 - MongoDB shell commands

After starting mongoBD shell with : `mongo -u root -p root`. You can:
* List databases: `show dbs`.
* Select database: `use cairmodweb`.
* List collections: `show collections`.
* Query on a collection: `db.myCollection.find(myQuery)` with
    * `myCollection`: name of the collection to query.
    * `myQuery`: the rules the documents have to respect to be returned. Exemple:
        * all documents: nothing instead of `myQuery` => `db.myCollection.find()`.
        * with a specific value for a field name _date_ => `db.myCollection.find({ date: 1930 })`.
        * For more information, see [Collection methods for mongo Shell](https://docs.mongodb.com/manual/reference/method/db.collection.find/#db.collection.find)
    * You can add `.pretty()` to make more readable the result.
    * You can add `.count()` to get the number of documents in the result.

For more information, see [mongo Shell Methods Manual](https://docs.mongodb.com/manual/reference/method).

