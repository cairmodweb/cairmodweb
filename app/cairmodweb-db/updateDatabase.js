const fs = require('fs');
const updateBDDutils = require("./utils/updateBDDfunction.js");
let csvPath = '../../../data_admin/date_csv/date.csv';
let lastupdate = new Date();

//Update Data Base
fs.watchFile(csvPath, () => {
    try {
      if (fs.existsSync(csvPath)) {
        console.log("file was changed");
        lastupdate = new Date();
        let currentStartTime = lastupdate;
        readFile(csvPath).then(data => 
          { 
            updateBDDutils.computeCSV(data);
            let composedUp = true;

            var intr = setInterval(async () => {
                var proc = updateBDDutils.dockerComposeUp();
                proc.on('error', function (err) {
                  //handle the error
                  composedUp = false;
                });
              if (composedUp || lastupdate!=currentStartTime) {
                console.log("Updated");
                clearInterval(intr);
              }
            }, 10000);


          });
      }
    } catch(err) {
        console.log("no file");
    }
  });

function readFile(path) {
    var fileContent;
  
    return new Promise(function(resolve) {
        fileContent = fs.readFileSync(path, {encoding: 'utf8'});
        resolve(fileContent);
    });
}