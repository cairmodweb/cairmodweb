/**
 * When entering this file, 
 *   mongoDB database is set to the variable MONGO_INTIDB_DATABASE
 *   with user = MONGO_INTIDB_ROOT_USERNAME
 *    and pwd = MONGO_INITDB_ROOT_PASSWORD
 */


/**
 *  Create a CairmodWeb user (not used yet)
 *  Create the cairmodweb database
 */ 
db.createUser(
  {
    user : "CairmodWeb",
    pwd : "cairmodweb",
    roles : [ 
      {
        role : "readWrite", 
        db : "admin" 
      },
      {
        role : "dbOwner", 
        db : "cairmodweb" 
      }
    ]
  }
)

/**
 * Create a "monuments" collection in the cairmodweb database
 */
db.createCollection("monuments");