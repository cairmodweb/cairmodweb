#!/bin/bash

echo ""
echo "Hello World!"
echo "I am import-csv-mongo."
echo "Starting import of init-csv.csv."
echo ""
mongoimport --type csv -d cairmodweb -c monuments \
    --drop --ignoreBlanks \
    --headerline --columnsHaveTypes \
    --parseGrace=skipRow \
    ./docker-entrypoint-initdb.d/init_csv.csv

echo ""
echo "Finishing import of init-csv.csv."
echo ""