/* Move to the right database */
db = db.getSiblingDB('cairmodweb');

/* Create indexes : 1 for ASC, -1 for DESC */
db.monuments.createIndex({'date': 1}, { name: 'date_index'});  // Ascending index on field date
db.monuments.createIndex({'date_fourchette_debut': 1}, { name: 'date_fourchette_debut_index'});  // Ascending index on field date_fourchette_debut
db.monuments.createIndex({'date_fourchette_fin': 1}, { name: 'date_fourchette_fin_index'});  // Ascending index on field date_fourchette_fin