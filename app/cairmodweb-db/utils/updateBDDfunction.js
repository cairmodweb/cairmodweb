var exec = require('child_process').exec;
const fs = require('fs');

module.exports = {
    computeCSV : function(data){
        console.log("Compute CSV");
        let separator = getNewLineSeparator(data);
          if (separator != "error"){
            let dataSplit = data.split(separator);
            if (correctHeader(dataSplit[0])){
              console.log("Update Data Base");
              let newData = addTypeHeader(dataSplit);
              
              //Update DB csv
              fs.writeFileSync('./mongo-init/init_csv.csv', newData.join(separator));

              //Restart Docker
              dockerComposeDown();
              
  
            }else 
            {
              console.log("Wrong header")
            }

          }
          else {
            console.log("Erreur. No new line")
          }
    },
   dockerComposeUp : function(){
    return(exec('sudo systemctl start cairmodweb-db.service'));
        
    }


}

function dockerComposeDown(){
    exec('sudo docker-compose down -v ',
        function (error) {
            if (error !== null) {
                 console.log('exec error: ' + error);
            }
        });
}



function correctHeader(header){
    let correctHeaderText = "id_opentheso,nom,date,date_fourchette_debut,date_fourchette_fin,note_date"
    return (header.replace(/\s/g, '') == correctHeaderText);
}

function getNewLineSeparator(data){
    if (data.includes('\r\n')){
        return ('\r\n');
    }
    else {
        if (data.includes('\n\r')){
            return ('\n\r')
        }
        else {
            if (data.includes('\n')){
                return ('\n')
            }
            else {
                if (data.includes('\r')){
                    return ('\r');
                }
                else {
                    return("error");
                }
            }
        }
    }
}

function addTypeHeader(dataSplit){
    dataSplit[0] = "id_opentheso.double(),nom.string(),date.double(),date_fourchette_debut.double(),date_fourchette_fin.double(),note_date.string()";
    return (dataSplit);
}
