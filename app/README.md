# WebApplication with API and Database

## Prerequisites

### For the database
* Docker:
    * version: recommended 19.03
    * documentation: https://www.docker.com/
    * download: https://www.docker.com/products/docker-desktop
* Docker-compose:
    * version: recommended 1.25
    * documentation: https://docs.docker.com/compose/
    * download: https://docs.docker.com/compose/install/

### For the API
* NodeJS:
    * version: recommended 12.16
    * documentation: https://nodejs.org/en/docs/
    * download: https://nodejs.org/en/download/
* Npm:
    * version: recommended 6.13
    * documentation: https://docs.npmjs.com
    * download: comes with NodeJS
* MkDocs:
    * version: version 1.0.4 or higher
    * documentation : https://www.mkdocs.org/
    * download : https://github.com/mkdocs/mkdocs 

### For the WebApplication
* NodeJs
* Npm
* MkDocs
* Angular-cli:
    * version: 9.0.5 or higher
    * documentation: https://angular.io/docs
    * download: `npm install -g @angular/cli@9.0.5`


### A running MapServer
To learn more about this, see [our mapserver documentation](./cairmodweb-mapserv/Doc_installation_mapserver_nginx.md).


## Run the application locally
To let the application works, you need to:
1. Start the API
    ```bach
    cd cairmodweb-api
    npm install
    npm start
    ```
    The API runs at http://localhost:3000/.
1. Start the WebApplication
    ```bach
    cd cairmodweb-app
    npm install
    npm run-script build
    npm start
    ```
    The API runs at http://localhost:4200/.

**Warning:** To stop the applications, you need to:
1. Stop the API: stop the shell where the API is running.
1. Stop the WebApplication: stop the shell where the WebApp is running.




## Deploy the application on a server

To deploy the application on a server, you need to follow some steps:
1. Upload the project on the server.
1. Configure the Nginx Proxy.
1. Set up the mapserver.
1. Build the web-app with the deployment parameters.
1. Activate the systemd services of the database, the api, the web-app.

### 1 - Upload the project on the server

The first step is to pull the git project on your server from a release:
```bash
cd ~
git clone -b <tag_release> https://gitlab.com/cairmodweb/cairmodweb.git
```

A folder named `cairmodweb` is created at your user root. It contains all you need to deploy the app.

For the next steps, you will have to adapt the name of your user server and the path of the project. We will consider here that:
* the user is `cmweb`.
* the deploy server address is `217.160.173.154`
* the path of the project is `/home/cmweb/cairmodweb/`.
* your current working directory is `/home/cmweb/`.


### 2 - Configure the Nginx Proxy

Nginx is used here as a reverse proxy for our htttp port (:80). It is reponsible for redirecting the requests to the right local server:
* `/cairmodweb/`: redirects to the web-app server on http://localhost:4200/.
* `/api/`: reidrects to the api on http://localhost:3000/.
* `/mapsw/`: redirects to the mapserver on http://localhost:9999/.

#### Install Nginx
* Nginx:
    * version: recommended 1.14.2
    * documentation: https://docs.nginx.com/nginx/
    * download: https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/

#### Set up the configuration

1. Place the nginx configuration in the nginx folder of the system:
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-deploy/cairmodweb-nginx.conf /etc/nginx/sites-enabled/default
    ```
1. Add the custom page
    ```bash
    sudo cp /cairmodweb/app/cairmodweb-deploy/custom-pages/custom_404.html /usr/share/nginx/html
    ```
1. Verify the configuration file:
    ```bash
    sudo nginx -t
    ```
    If ok, then continue, else report the bug in the git projet.
1. Reload the nginx configuration:
    ```bash
    sudo /etc/init.d/nginx reload
    ```

### 3 - Set up the MapServer

You can follow the steps described in our [MapServer documentation](./cairmodweb-mapserv/Doc_installation_mapserver_nginx.md) (skip the Nginx configuration step).


### 4 - Build the WebApp

The webApp needs to be built to be deployed. The build process is not the same as the one for the local run of the app. 
The commands are:
```bash
cd /cairmodweb/app/cairmodweb-app/
npm install
npm run-script build-deploy
```

It might be possible that you app does not build because of a heap memory of JavaScript. In this case you need to:
1. Build the application on an other devices (for example your computer) but still with de previous `build-deploy` command.
2. Upload the build folder on the server:
    ```bash
    scp -r <my_project_folder>/cairdmoweb/app/cairmodweb-app/dist cmweb@217.160.173.154:/home/cmweb/cairmodweb/app/cairmodweb-app/
    ```
1. Upload the build documentation on the server:
    ```bash
    scp -r <my_project_folder>/cairdmoweb/app/cairmodweb-app/server/routes/app-doc/site cmweb@217.160.173.154:/home/cmweb/cairmodweb/app/cairmodweb-app/server/routes/app-doc/
    ```


### 5 - Activate the services

The activation of the services can be done automatically or manually


#### 5.1 - Automatically

The services can be activated with the following command:
```bash
sudo sh cairmodweb-deploy/deploy.sh
```

The `deploy.sh` script from `cairmodweb-deploy` does automatically the activation steps.

#### 5.2 - Manually

1. You need to place the four services into the systemd folder:
    ```bash
    sudo cp cairmodweb/app/cairmodweb-deploy/*.service /etc/systemd/system/
    ```
1. Reload the sytemd services:
    ```bash
    sudo systemctl reload
    ```
1. Start them:
    ```bash
    sudo systemctl start cairmodweb-database.service
    sudo systemctl start cairmodweb-api.service
    sudo systemctl start cairmodweb-app.service
    sudo systemctl start cairmodweb-db-update.service
    ````
2. Enable them in order to be launched at each restart of the server:
    ```bash
    sudo systemctl enable cairmodweb-database.service
    sudo systemctl enable cairmodweb-api.service
    sudo systemctl enable cairmodweb-app.service
    sudo systemctl enable cairmodweb-db-update.service
    ````

#### 5.4 - Monitor services
You can monitor the state of each service with:
```bash
sudo systemctl status <the_service>
````